﻿create table tb_Unidad_Organizativa
(
tb_Unidad_Organizativa_id serial not null,
tb_Unidad_Organizativa_nombre character varying(50) not null,
tb_Unidad_Organizativa_abreviatura character varying(30) not null,
tb_Unidad_Organizativa_estado character(1) not null,
constraint pk_Unidad_Organizativa primary key(tb_Unidad_Organizativa_id),
constraint chk_Unidad_Organizativa_id check(tb_Unidad_Organizativa_id >0),
constraint chk_Unidad_Organizativa_estado check(tb_Unidad_Organizativa_estado in('A','I'))
);

create table tb_Tipo_Documento
(
tb_Tipo_Documento_id serial not null,
tb_Tipo_Documento_nombre character varying(50) not null,
tb_Tipo_Documento_estado character(1) not null,
constraint pk_Tipo_Documento primary key(tb_Tipo_Documento_id),
constraint chk_Tipo_Documento_id check(tb_Tipo_Documento_id >0),
constraint chk_Tipo_Documento_estado check(tb_Tipo_Documento_estado in('A','I'))
);

create table tb_Unidad_Tramite
(
tb_Unidad_Tramite_id serial not null,
tb_Unidad_Tramite_nombre character varying(50) not null,
tb_Unidad_Tramite_abreviatura character varying(30) not null,
tb_Unidad_Tramite_responsable character varying(50) not null,
tb_Unidad_Tramite_estado character(1) not null,
tb_Unidad_Organizativa_id integer not null,
constraint pk_Unidad_Tramite primary key(tb_Unidad_Tramite_id),
constraint fk_Unidad_Organizativa_Unidad_Tramite foreign key(tb_Unidad_Organizativa_id) references tb_Unidad_Organizativa(tb_Unidad_Organizativa_id),
constraint chk_Unidad_Tramite_id check(tb_Unidad_Tramite_id >0),
constraint chk_Unidad_Tramite_estado check(tb_Unidad_Tramite_estado in('A','I'))
);

create table tb_Documento_Recibido
(
tb_Documento_Recibido_id serial not null,
tb_Documento_Recibido_añoexpediente date not null,
tb_Documento_Recibido_numexpediente character(50) not null,
tb_Documento_Recibido_numero character varying(10),
tb_Documento_Recibido_de character varying(50) not null,
tb_Documento_Recibido_asunto character varying(50) not null,
tb_Documento_Recibido_observacion character varying(100) not null,
tb_Documento_Recibido_tipo character(1) not null,
tb_Documento_Recibido_estado character(1) not null,
tb_Tipo_Documento_id integer,
constraint pk_Documento_Recibido primary key(tb_Documento_Recibido_id),
constraint fk_Tipo_Documento_Documento_Recibido foreign key(tb_Tipo_Documento_id) references tb_Tipo_Documento(tb_Tipo_Documento_id),
constraint chk_Documento_Recibido_id check(tb_Documento_Recibido_id >0),
constraint chk_Documento_Recibido_estado check(tb_Documento_Recibido_estado in('A','I'))
);

create table tb_Documento_Emitido
(
tb_Documento_Emitido_id serial not null,
tb_Documento_Emitido_año date not null,
tb_Documento_Emitido_numero character varying(50) not null,
tb_Documento_Emitido_fechemision date not null,
tb_Documento_Emitido_para character varying(50) not null,
tb_Documento_Emitido_asunto character varying(50) not null,
tb_Documento_Emitido_numexpediente character varying(50) not null,
tb_Documento_Emitido_fechrecepcion date not null,
tb_Documento_Emitido_tipo character varying(50) not null,
tb_Tipo_Documento_id integer not null,
tb_Unidad_Tramite_id integer not null,
constraint pk_Documento_Emitido primary key(tb_Documento_Emitido_id),
constraint fk_Tipo_Documento_Documento_Emitido foreign key(tb_Tipo_Documento_id) references tb_Tipo_Documento(tb_Tipo_Documento_id),
constraint fk_Unidad_Tramite_Documento_Emitido foreign key(tb_Unidad_Tramite_id) references tb_Unidad_Tramite(tb_Unidad_Tramite_id),
constraint chk_Documento_Emitido_id check(tb_Documento_Emitido_id >0),
constraint chk_Documento_Emitido check(tb_Documento_Emitido_tipo in('T','I'))
);
create table tb_Ruta
(
tb_Ruta_id serial not null,
tb_Ruta_fechhora timestamp not null,
tb_Ruta_tipoexpediente character varying(50) not null,
tb_Documento_Recibido_id integer not null,
tb_Unidad_Tramite_id integer not null,
constraint pk_Ruta primary key(tb_Ruta_id),
constraint fk_Documento_Recibido_Ruta foreign key(tb_Documento_Recibido_id) references tb_Documento_Recibido(tb_Documento_Recibido_id),
constraint fk_Unidad_Tramite_Ruta foreign key(tb_Unidad_Tramite_id) references tb_Unidad_Tramite(tb_Unidad_Tramite_id),
constraint chk_Ruta_id check(tb_Ruta_id >0)
);
create table tb_Tramite	
(
tb_Tramite_id serial not null,
tb_Tramite_descripcion character varying(50) not null,
tb_Tramite_tiptraslado character varying(50) not null,
tb_Tramite_tipentidad character varying(50) not null,
tb_Tramite_descripentidad character varying(50),
tb_Tramite_fecha_emision date not null,
tb_Documento_Emitido_id integer,
tb_Ruta_id integer,
constraint pk_Tramite primary key(tb_Tramite_id),
constraint fk_Documento_Emitido_Tramite foreign key(tb_Documento_Emitido_id) references tb_Documento_Emitido(tb_Documento_Emitido_id),
constraint fk_Ruta_Tramite foreign key(tb_Ruta_id) references tb_Ruta(tb_Ruta_id),
constraint chk_Tramite_id check(tb_Tramite_id >0)
);
create table tb_Tipo_Tramite
(
tb_Tipo_Tramite_id serial not null,
tb_Tipo_Tramite_nombre character varying(50) not null,
tb_Tipo_Tramite_tiempoestimado integer not null,
tb_Tipo_Tramite_estado character(1) not null,
tb_Unidad_Tramite_id integer not null,
constraint pk_Tipo_Tramite primary key(tb_Tipo_Tramite_id),
constraint fk_Unidad_Tramite_Tipo_Tramite foreign key(tb_Unidad_Tramite_id) references tb_Unidad_Tramite(tb_Unidad_Tramite_id),
constraint chk_Tipo_Tramite_id check(tb_Tipo_Tramite_id >0),
constraint chk_Tipo_Tramite_estado check(tb_Tipo_Tramite_estado in('A','I'))
);
create table tb_Proceso_Tramite
(
tb_Proceso_Tramite_id serial not null,
tb_Unidad_Tramite_id integer,
tb_Tramite_id integer,
constraint pk_Proceso_Tramite primary key(tb_Proceso_Tramite_id),
constraint fk_Unidad_Tramite_Proceso_Tramite foreign key(tb_Unidad_Tramite_id) references tb_Unidad_Tramite(tb_Unidad_Tramite_id),
constraint fk_Tramite_Proceso_Tramite foreign key(tb_Tramite_id) references tb_Tramite(tb_Tramite_id),
constraint chk_Proceso_Tramite_id check(tb_Proceso_Tramite_id >0)
);
create table tb_Requisitos
(
tb_Requisitos_id serial not null,
tb_Requisitos_descripcion character varying(50) not null,
tb_Requisitos_estado character(1) not null,
tb_Tipo_Tramite_id integer not null,
constraint pk_Requisitos primary key(tb_Requisitos_id),
constraint fk_Tipo_Tramite_Requisitos foreign key(tb_Tipo_Tramite_id) references tb_Tipo_Tramite(tb_Tipo_Tramite_id),
constraint chk_Requisitos_id check(tb_Requisitos_id >0),
constraint chk_Requisitos_estado check(tb_Requisitos_estado in('A','I'))
);
create table tb_Requisito_Concluidos
(
tb_Requisito_Concluidos_id serial not null,
tb_Requisitos_id integer,
tb_Documento_Recibido_id integer,
constraint pk_Requisito_Concluidos primary key(tb_Requisito_Concluidos_id),
constraint fk_Requisitos_Requisito_Concluidos foreign key(tb_Requisitos_id) references tb_Requisitos(tb_Requisitos_id),
constraint fk_Documento_Recibido_Requisito_Concluidos foreign key(tb_Documento_Recibido_id) references tb_Documento_Recibido(tb_Documento_Recibido_id),
constraint chk_Requisito_Concluidos_id check(tb_Requisito_Concluidos_id >0)
);
create table tb_Usuario
(
tb_Usuario_id serial not null,
tb_Usuario_apellipat character varying(50) not null,
tb_Usuario_apellimat character varying(50) not null,
tb_Usuario_nombre character varying(50) not null,
tb_Usuario_correo character varying(50) not null,
tb_Usuario_cuenta character varying(50) not null,
tb_Usuario_clave character varying(50) not null,
tb_Usuario_estado character(1) not null,
tb_Usuario_tipo character(1) not null,
constraint pk_Usuario primary key(tb_Usuario_id),
constraint unq_Usuario_cuenta unique(tb_Usuario_cuenta),
constraint chk_Usuario_id check(tb_Usuario_id >0),
constraint chk_Usuario_estado check(tb_Usuario_estado in('A','I')),
constraint chk_Usuario_tipo check(tb_Usuario_tipo in('O','T','G'))
);

create table tb_Permiso
(
tb_Permiso_id serial not null,
tb_Permiso_fechinicio date not null,
tb_Permiso_fechtermino date not null,
tb_Permiso_tipo character(1) not null,
tb_Usuario_id integer not null,
tb_Unidad_Tramite_id integer,
tb_Unidad_Organizativa_id integer,
constraint pk_Permiso primary key(tb_Permiso_id),
constraint fk_Unidad_Tramite_Permiso foreign key(tb_Unidad_Tramite_id) references tb_Unidad_Tramite(tb_Unidad_Tramite_id),
constraint fk_Usuario_Permiso foreign key(tb_Usuario_id) references tb_Usuario(tb_Usuario_id),
constraint fk_Unidad_Organizativa_Permiso foreign key(tb_Unidad_Organizativa_id) references tb_Unidad_Organizativa(tb_Unidad_Organizativa_id),
constraint chk_Permiso_id check(tb_Permiso_id >0),
constraint chk_Permiso_tipo check(tb_Permiso_tipo in('T','G'))
);

select * from tb_Documento_Recibido