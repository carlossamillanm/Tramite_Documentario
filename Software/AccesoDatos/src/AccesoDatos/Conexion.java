/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package AccesoDatos;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author Carlos Anthony
 */
public class Conexion {
    static String Datos[]=new String[5];
    
    public static Connection Abrir() throws Exception {
        Connection conexion = null;
        Leer();
        try {
            /*
            String servidor = "localhost";
            String puerto = "5432";
            String baseDatos = "Proyecto_Taller";
            String usuario = "postgres";
            String contrasena = "fuchan309";
            */
            
            String cadenaConexion = "jdbc:postgresql://" + Datos[0] + ":" +Datos[1] + "/" + Datos[2];

            conexion = DriverManager.getConnection(cadenaConexion,Datos[3],Datos[4]);            
            
            return conexion;
        } 
        catch (Exception e) {
            throw e;
        }
    }
    
    public static void Leer() {
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;

        try {
            archivo = new File("D:\\Trabajos\\Taller de Programacion\\Saavedra\\Proyecto\\Tercer Avanse\\Host\\Host.txt");
            fr = new FileReader(archivo);
            br = new BufferedReader(fr);

            String linea;
            int a = 0;

            while ((linea = br.readLine()) != null) {
                Datos[a] = linea;
                a++;
            }
            br.close();
            fr.close();

        } catch (Exception e) {
            System.out.println(e);
        }

    }
}
