package AccesoDatos;

import Entidades.DocumentoEmitido;
import Entidades.TipoDocumento;
import Entidades.UnidadOrganizativa;
import Entidades.UnidadTramite;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class DocumentoEmitidoAD {

    private Connection connection;

    public DocumentoEmitidoAD(Connection connection) {
        this.connection = connection;
    }

    public List<DocumentoEmitido> MostrarLista() throws Exception {
        try {
            String sql = "";

            sql = sql + " select";
            sql = sql + " tb_Documento_Emitido.tb_Documento_Emitido_id";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_año";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_numero";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_fechemision";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_para";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_asunto";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_numexpediente";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_fechrecepcion";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_tipo";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_id";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_nombre";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_estado";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_id";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_nombre";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_abreviatura";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_responsable";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_estado";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_estado";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_id";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_nombre";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_abreviatura";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_estado";
            sql = sql + " from";
            sql = sql + " tb_Documento_Emitido";
            sql = sql + " inner join tb_Unidad_Tramite on tb_Documento_Emitido.tb_Unidad_Tramite_id = tb_Unidad_Tramite.tb_Unidad_Tramite_id";
            sql = sql + " inner join tb_Tipo_Documento on tb_Documento_Emitido.tb_Tipo_Documento_id = tb_Tipo_Documento.tb_Tipo_Documento_id";
            sql = sql + " inner join tb_Unidad_Organizativa on tb_Unidad_Tramite.tb_Unidad_Organizativa_id = tb_Unidad_Organizativa.tb_Unidad_Organizativa_id";
            sql = sql + ";";

            List<DocumentoEmitido> lista = new ArrayList<DocumentoEmitido>();
            DocumentoEmitido objeto;

            PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = ps.executeQuery();

            rs.beforeFirst();

            while (rs.next()) {
                objeto = new DocumentoEmitido();

                objeto.setId(rs.getInt("tb_Documento_Emitido_id"));
                objeto.setAño(rs.getTimestamp("tb_Documento_Emitido_año"));
                objeto.setNum(rs.getString("tb_Documento_Emitido_numero"));
                objeto.setFechaEmision(rs.getTimestamp("tb_Documento_Emitido_fechemision"));
                objeto.setPara(rs.getString("tb_Documento_Emitido_para"));
                objeto.setAsunto(rs.getString("tb_Documento_Emitido_asunto"));
                objeto.setNumExpediente(rs.getString("tb_Documento_Emitido_numexpediente"));
                objeto.setFechaRecepcion(rs.getTimestamp("tb_Documento_Emitido_fechrecepcion"));
                objeto.setTipo(rs.getString("tb_Documento_Emitido_tipo"));
                objeto.setTipoDocumento(new TipoDocumento(rs.getInt("tb_Tipo_Documento_id"), rs.getString("tb_Tipo_Documento_nombre"), rs.getString("tb_Tipo_Documento_estado")));
                objeto.setUnidadTramite(new UnidadTramite(rs.getInt("tb_Unidad_Tramite_id"), rs.getString("tb_Unidad_Tramite_nombre"), rs.getString("tb_Unidad_Tramite_abreviatura"), rs.getString("tb_Unidad_Tramite_responsable"), rs.getString("tb_Unidad_Tramite_estado"), new UnidadOrganizativa(rs.getInt("tb_Unidad_Organizativa_id"))));
                lista.add(objeto);
            }

            return lista;
        } catch (Exception e) {
            throw e;
        }
    }

    public List<DocumentoEmitido> ListarFechas(String inicio, String fin) throws Exception {
        try {
            String sql = "";

            sql = sql + " select";
            sql = sql + " tb_Documento_Emitido.tb_Documento_Emitido_id";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_año";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_numero";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_fechemision";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_para";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_asunto";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_numexpediente";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_fechrecepcion";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_tipo";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_id";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_nombre";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_estado";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_id";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_nombre";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_abreviatura";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_responsable";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_estado";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_estado";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_id";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_nombre";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_abreviatura";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_estado";
            sql = sql + " from";
            sql = sql + " tb_Documento_Emitido";
            sql = sql + " inner join tb_Unidad_Tramite on tb_Documento_Emitido.tb_Unidad_Tramite_id = tb_Unidad_Tramite.tb_Unidad_Tramite_id";
            sql = sql + " inner join tb_Tipo_Documento on tb_Documento_Emitido.tb_Tipo_Documento_id = tb_Tipo_Documento.tb_Tipo_Documento_id";
            sql = sql + " inner join tb_Unidad_Organizativa on tb_Unidad_Tramite.tb_Unidad_Organizativa_id = tb_Unidad_Organizativa.tb_Unidad_Organizativa_id";
            sql = sql + " where";
            sql = sql + " cast(tb_Documento_Emitido.tb_Documento_Emitido_fechemision as date) between '" + inicio + "' and '" + fin + "'";
            sql = sql + "order by tb_Documento_Emitido_fechemision asc";
            sql = sql + ";";

            List<DocumentoEmitido> lista = new ArrayList<DocumentoEmitido>();
            DocumentoEmitido objeto;

            PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = ps.executeQuery();

            rs.beforeFirst();

            while (rs.next()) {
                objeto = new DocumentoEmitido();

                objeto.setId(rs.getInt("tb_Documento_Emitido_id"));
                objeto.setAño(rs.getTimestamp("tb_Documento_Emitido_año"));
                objeto.setNum(rs.getString("tb_Documento_Emitido_numero"));
                objeto.setFechaEmision(rs.getTimestamp("tb_Documento_Emitido_fechemision"));
                objeto.setPara(rs.getString("tb_Documento_Emitido_para"));
                objeto.setAsunto(rs.getString("tb_Documento_Emitido_asunto"));
                objeto.setNumExpediente(rs.getString("tb_Documento_Emitido_numexpediente"));
                objeto.setFechaRecepcion(rs.getTimestamp("tb_Documento_Emitido_fechrecepcion"));
                objeto.setTipo(rs.getString("tb_Documento_Emitido_tipo"));
                objeto.setTipoDocumento(new TipoDocumento(rs.getInt("tb_Tipo_Documento_id"), rs.getString("tb_Tipo_Documento_nombre"), rs.getString("tb_Tipo_Documento_estado")));
                objeto.setUnidadTramite(new UnidadTramite(rs.getInt("tb_Unidad_Tramite_id"), rs.getString("tb_Unidad_Tramite_nombre"), rs.getString("tb_Unidad_Tramite_abreviatura"), rs.getString("tb_Unidad_Tramite_responsable"), rs.getString("tb_Unidad_Tramite_estado"), new UnidadOrganizativa(rs.getInt("tb_Unidad_Organizativa_id"))));
                lista.add(objeto);
            }

            return lista;
        } catch (Exception e) {
            throw e;
        }
    }

    public DocumentoEmitido Ver(String id) throws Exception {
        try {
            String sql = "";

            sql = sql + " select";
            sql = sql + " tb_Documento_Emitido.tb_Documento_Emitido_id";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_año";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_numero";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_fechemision";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_para";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_asunto";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_numexpediente";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_fechrecepcion";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_tipo";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_id";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_nombre";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_estado";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_id";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_nombre";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_abreviatura";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_responsable";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_estado";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_estado";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_id";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_nombre";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_abreviatura";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_estado";
            sql = sql + " from";
            sql = sql + " tb_Documento_Emitido";
            sql = sql + " inner join tb_Unidad_Tramite on tb_Documento_Emitido.tb_Unidad_Tramite_id = tb_Unidad_Tramite.tb_Unidad_Tramite_id";
            sql = sql + " inner join tb_Tipo_Documento on tb_Documento_Emitido.tb_Tipo_Documento_id = tb_Tipo_Documento.tb_Tipo_Documento_id";
            sql = sql + " inner join tb_Unidad_Organizativa on tb_Unidad_Tramite.tb_Unidad_Organizativa_id = tb_Unidad_Organizativa.tb_Unidad_Organizativa_id";
            sql = sql + " where";
            sql = sql + " tb_Documento_Emitido.tb_Documento_Emitido_numexpediente like '" + id + "%'";
            sql = sql + ";";

            DocumentoEmitido objeto = null;

            PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = ps.executeQuery();

            rs.beforeFirst();

            if (rs.next()) {
                objeto = new DocumentoEmitido();

                objeto.setId(rs.getInt("tb_Documento_Emitido_id"));
                objeto.setAño(rs.getTimestamp("tb_Documento_Emitido_año"));
                objeto.setNum(rs.getString("tb_Documento_Emitido_numero"));
                objeto.setFechaEmision(rs.getTimestamp("tb_Documento_Emitido_fechemision"));
                objeto.setPara(rs.getString("tb_Documento_Emitido_para"));
                objeto.setAsunto(rs.getString("tb_Documento_Emitido_asunto"));
                objeto.setNumExpediente(rs.getString("tb_Documento_Emitido_numexpediente"));
                objeto.setFechaRecepcion(rs.getTimestamp("tb_Documento_Emitido_fechrecepcion"));
                objeto.setTipo(rs.getString("tb_Documento_Emitido_tipo"));
                objeto.setTipoDocumento(new TipoDocumento(rs.getInt("tb_Tipo_Documento_id"), rs.getString("tb_Tipo_Documento_nombre"), rs.getString("tb_Tipo_Documento_estado")));
                objeto.setUnidadTramite(new UnidadTramite(rs.getInt("tb_Unidad_Tramite_id"), rs.getString("tb_Unidad_Tramite_nombre"), rs.getString("tb_Unidad_Tramite_abreviatura"), rs.getString("tb_Unidad_Tramite_responsable"), rs.getString("tb_Unidad_Tramite_estado"), new UnidadOrganizativa(rs.getInt("tb_Unidad_Organizativa_id"))));

            }
            return objeto;
        } catch (Exception e) {
            throw e;
        }
    }

    public DocumentoEmitido VerTramite(String id) throws Exception {
        try {
            String sql = "";

            sql = sql + " select";
            sql = sql + " tb_Documento_Emitido.tb_Documento_Emitido_id";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_año";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_numero";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_fechemision";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_para";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_asunto";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_numexpediente";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_fechrecepcion";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_tipo";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_id";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_nombre";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_estado";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_id";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_nombre";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_abreviatura";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_responsable";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_estado";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_estado";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_id";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_nombre";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_abreviatura";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_estado";
            sql = sql + " from";
            sql = sql + " tb_Documento_Emitido";
            sql = sql + " inner join tb_Unidad_Tramite on tb_Documento_Emitido.tb_Unidad_Tramite_id = tb_Unidad_Tramite.tb_Unidad_Tramite_id";
            sql = sql + " inner join tb_Tipo_Documento on tb_Documento_Emitido.tb_Tipo_Documento_id = tb_Tipo_Documento.tb_Tipo_Documento_id";
            sql = sql + " inner join tb_Unidad_Organizativa on tb_Unidad_Tramite.tb_Unidad_Organizativa_id = tb_Unidad_Organizativa.tb_Unidad_Organizativa_id";
            sql = sql + " where";
            sql = sql + " tb_Documento_Emitido.tb_Documento_Emitido_numero like '" + id + "%'";
            sql = sql + ";";

            DocumentoEmitido objeto = null;

            PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = ps.executeQuery();

            rs.beforeFirst();

            if (rs.next()) {
                objeto = new DocumentoEmitido();

                objeto.setId(rs.getInt("tb_Documento_Emitido_id"));
                objeto.setAño(rs.getTimestamp("tb_Documento_Emitido_año"));
                objeto.setNum(rs.getString("tb_Documento_Emitido_numero"));
                objeto.setFechaEmision(rs.getTimestamp("tb_Documento_Emitido_fechemision"));
                objeto.setPara(rs.getString("tb_Documento_Emitido_para"));
                objeto.setAsunto(rs.getString("tb_Documento_Emitido_asunto"));
                objeto.setNumExpediente(rs.getString("tb_Documento_Emitido_numexpediente"));
                objeto.setFechaRecepcion(rs.getTimestamp("tb_Documento_Emitido_fechrecepcion"));
                objeto.setTipo(rs.getString("tb_Documento_Emitido_tipo"));
                objeto.setTipoDocumento(new TipoDocumento(rs.getInt("tb_Tipo_Documento_id"), rs.getString("tb_Tipo_Documento_nombre"), rs.getString("tb_Tipo_Documento_estado")));
                objeto.setUnidadTramite(new UnidadTramite(rs.getInt("tb_Unidad_Tramite_id"), rs.getString("tb_Unidad_Tramite_nombre"), rs.getString("tb_Unidad_Tramite_abreviatura"), rs.getString("tb_Unidad_Tramite_responsable"), rs.getString("tb_Unidad_Tramite_estado"), new UnidadOrganizativa(rs.getInt("tb_Unidad_Organizativa_id"))));

            }
            return objeto;
        } catch (Exception e) {
            throw e;
        }
    }

    public void Insertar(DocumentoEmitido objeto) throws Exception {
        try {
            String dml = "";

            dml = dml + " insert into tb_Documento_Emitido(";
            dml = dml + " tb_Documento_Emitido_año";
            dml = dml + ",tb_Documento_Emitido_numero";
            dml = dml + ",tb_Documento_Emitido_fechemision";
            dml = dml + ",tb_Documento_Emitido_para";
            dml = dml + ",tb_Documento_Emitido_asunto";
            dml = dml + ",tb_Documento_Emitido_numexpediente";
            dml = dml + ",tb_Documento_Emitido_fechrecepcion";
            dml = dml + ",tb_Documento_Emitido_tipo";
            dml = dml + ",tb_Tipo_Documento_id";
            dml = dml + ",tb_Unidad_Tramite_id";
            dml = dml + ") values (";
            dml = dml + " '" + objeto.getAño() + "'";
            dml = dml + ",'" + objeto.getNum() + "'";
            dml = dml + ",'" + objeto.getFechaEmision() + "'";
            dml = dml + ",'" + objeto.getPara() + "'";
            dml = dml + ",'" + objeto.getAsunto() + "'";
            dml = dml + ",'" + objeto.getNumExpediente() + "'";
            dml = dml + ",'" + objeto.getFechaRecepcion() + "'";
            dml = dml + ",'" + objeto.getTipo() + "'";
            dml = dml + ",'" + objeto.getTipoDocumento().getId().toString() + "'";
            dml = dml + ",'" + objeto.getUnidadTramite().getId().toString() + "'";
            dml = dml + " )";
            dml = dml + ";";

            PreparedStatement ps = connection.prepareStatement(dml);

            ps.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
    }

    public void Actualizar(DocumentoEmitido objeto) throws Exception {
        try {
            String dml = "";

            dml = dml + " update tb_Documento_Emitido set";
            dml = dml + " tb_Documento_Emitido_año = '" + objeto.getAño() + "'";
            dml = dml + ",tb_Documento_Emitido_numero = '" + objeto.getNum() + "'";
            dml = dml + ",tb_Documento_Emitido_fechemision = '" + objeto.getFechaEmision() + "'";
            dml = dml + ",tb_Documento_Emitido_para = '" + objeto.getPara() + "'";
            dml = dml + ",tb_Documento_Emitido_asunto = '" + objeto.getAsunto() + "'";
            dml = dml + ",tb_Documento_Emitido_numexpediente = '" + objeto.getNumExpediente() + "'";
            dml = dml + ",tb_Documento_Emitido_fechrecepcion = '" + objeto.getFechaRecepcion() + "'";
            dml = dml + ",tb_Documento_Emitido_tipo = '" + objeto.getTipo() + "'";
            dml = dml + ",tb_Tipo_Documento_id = '" + objeto.getTipoDocumento().getId().toString() + "'";
            dml = dml + ",tb_Unidad_Tramite_id = '" + objeto.getUnidadTramite().getId().toString() + "'";
            dml = dml + " where tb_Documento_Emitido_id = " + objeto.getId().toString();
            dml = dml + ";";

            PreparedStatement ps = connection.prepareStatement(dml);

            ps.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
    }

    public void Eliminar(DocumentoEmitido objeto) throws Exception {
        try {
            String dml = "";

            dml = dml + " delete from tb_Documento_Emitido";
            dml = dml + " where tb_Documento_Emitido_id = " + objeto.getId().toString();
            dml = dml + ";";

            PreparedStatement ps = connection.prepareStatement(dml);

            ps.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
    }
}
