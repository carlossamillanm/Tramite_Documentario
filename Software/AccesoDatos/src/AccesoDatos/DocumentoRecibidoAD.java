package AccesoDatos;

import Entidades.DocumentoRecibido;
import Entidades.TipoDocumento;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class DocumentoRecibidoAD {

    private Connection connection;

    public DocumentoRecibidoAD(Connection connection) {
        this.connection = connection;
    }

    public List<DocumentoRecibido> ListarFechas(String inicio, String fin) throws Exception {
        try {
            String sql = "";

            sql = sql + " select";
            sql = sql + " tb_Documento_Recibido.tb_Documento_Recibido_id";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_añoexpediente";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_numexpediente";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_numero";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_de";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_asunto";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_observacion";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_tipo";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_estado";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_id";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_nombre";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_estado";
            sql = sql + " from";
            sql = sql + " tb_Documento_Recibido";
            sql = sql + " inner join tb_Tipo_Documento on tb_Documento_Recibido.tb_Tipo_Documento_id = tb_Tipo_Documento.tb_Tipo_Documento_id";
            sql = sql + " where";
            sql = sql + " cast(tb_Documento_Recibido.tb_Documento_Recibido_añoexpediente as date) between '" + inicio + "' and '" + fin + "'";
            sql = sql + ";";
            
            List<DocumentoRecibido> lista = new ArrayList<DocumentoRecibido>();
            DocumentoRecibido objeto;

            PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = ps.executeQuery();

            rs.beforeFirst();

            while (rs.next()) {
                objeto = new DocumentoRecibido();

                objeto.setId(rs.getInt("tb_Documento_Recibido_id"));
                objeto.setAñoExpediente(rs.getTimestamp("tb_Documento_Recibido_añoexpediente"));
                objeto.setNumExpediente(rs.getString("tb_Documento_Recibido_numexpediente"));
                objeto.setNum(rs.getString("tb_Documento_Recibido_numero"));
                objeto.setDe(rs.getString("tb_Documento_Recibido_de"));
                objeto.setAsunto(rs.getString("tb_Documento_Recibido_asunto"));
                objeto.setObservacion(rs.getString("tb_Documento_Recibido_observacion"));
                objeto.setTipo(rs.getString("tb_Documento_Recibido_tipo"));
                objeto.setEstado(rs.getString("tb_Documento_Recibido_estado"));
                objeto.setTipoDocumento(new TipoDocumento(rs.getInt("tb_Tipo_Documento_id"), rs.getString("tb_Tipo_Documento_nombre"), rs.getString("tb_Tipo_Documento_estado")));
                lista.add(objeto);
            }

            return lista;
        } catch (Exception e) {
            throw e;
        }
    }

    public List<DocumentoRecibido> ListarTipos(String inicio, String fin, String Tipo) throws Exception {
        try {
            String sql = "";

            sql = sql + " select";
            sql = sql + " tb_Documento_Recibido.tb_Documento_Recibido_id";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_añoexpediente";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_numexpediente";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_numero";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_de";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_asunto";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_observacion";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_tipo";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_estado";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_id";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_nombre";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_estado";
            sql = sql + " from";
            sql = sql + " tb_Documento_Recibido";
            sql = sql + " inner join tb_Tipo_Documento on tb_Documento_Recibido.tb_Tipo_Documento_id = tb_Tipo_Documento.tb_Tipo_Documento_id";
            sql = sql + " where";
            sql = sql + " tb_Documento_Recibido.tb_Documento_Recibido_tipo like '" + Tipo + "%'";
            sql = sql + " and cast(tb_Documento_Recibido.tb_Documento_Recibido_añoexpediente as date) between '" + inicio + "' and '" + fin + "'";
            sql = sql + ";";

            List<DocumentoRecibido> lista = new ArrayList<DocumentoRecibido>();
            DocumentoRecibido objeto;

            PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = ps.executeQuery();

            rs.beforeFirst();

            while (rs.next()) {
                objeto = new DocumentoRecibido();

                objeto.setId(rs.getInt("tb_Documento_Recibido_id"));
                objeto.setAñoExpediente(rs.getTimestamp("tb_Documento_Recibido_añoexpediente"));
                objeto.setNumExpediente(rs.getString("tb_Documento_Recibido_numexpediente"));
                objeto.setNum(rs.getString("tb_Documento_Recibido_numero"));
                objeto.setDe(rs.getString("tb_Documento_Recibido_de"));
                objeto.setAsunto(rs.getString("tb_Documento_Recibido_asunto"));
                objeto.setObservacion(rs.getString("tb_Documento_Recibido_observacion"));
                objeto.setTipo(rs.getString("tb_Documento_Recibido_tipo"));
                objeto.setEstado(rs.getString("tb_Documento_Recibido_estado"));
                objeto.setTipoDocumento(new TipoDocumento(rs.getInt("tb_Tipo_Documento_id"), rs.getString("tb_Tipo_Documento_nombre"), rs.getString("tb_Tipo_Documento_estado")));
                lista.add(objeto);
            }

            return lista;
        } catch (Exception e) {
            throw e;
        }
    }

     public List<DocumentoRecibido> ListarTodo(String inicio, String fin, String Tipo, String Estado) throws Exception {
        try {
            String sql = "";

            sql = sql + " select";
            sql = sql + " tb_Documento_Recibido.tb_Documento_Recibido_id";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_añoexpediente";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_numexpediente";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_numero";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_de";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_asunto";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_observacion";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_tipo";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_estado";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_id";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_nombre";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_estado";
            sql = sql + " from";
            sql = sql + " tb_Documento_Recibido";
            sql = sql + " inner join tb_Tipo_Documento on tb_Documento_Recibido.tb_Tipo_Documento_id = tb_Tipo_Documento.tb_Tipo_Documento_id";
            sql = sql + " where";
            sql = sql + " tb_Documento_Recibido.tb_Documento_Recibido_tipo like '" + Tipo + "%'";
            sql = sql + " and tb_Documento_Recibido.tb_Documento_Recibido_estado like '" + Estado + "%'";
            sql = sql + " and cast(tb_Documento_Recibido.tb_Documento_Recibido_añoexpediente as date) between '" + inicio + "' and '" + fin + "'";
            sql = sql + ";";

            List<DocumentoRecibido> lista = new ArrayList<DocumentoRecibido>();
            DocumentoRecibido objeto;

            PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = ps.executeQuery();

            rs.beforeFirst();

            while (rs.next()) {
                objeto = new DocumentoRecibido();

                objeto.setId(rs.getInt("tb_Documento_Recibido_id"));
                objeto.setAñoExpediente(rs.getTimestamp("tb_Documento_Recibido_añoexpediente"));
                objeto.setNumExpediente(rs.getString("tb_Documento_Recibido_numexpediente"));
                objeto.setNum(rs.getString("tb_Documento_Recibido_numero"));
                objeto.setDe(rs.getString("tb_Documento_Recibido_de"));
                objeto.setAsunto(rs.getString("tb_Documento_Recibido_asunto"));
                objeto.setObservacion(rs.getString("tb_Documento_Recibido_observacion"));
                objeto.setTipo(rs.getString("tb_Documento_Recibido_tipo"));
                objeto.setEstado(rs.getString("tb_Documento_Recibido_estado"));
                objeto.setTipoDocumento(new TipoDocumento(rs.getInt("tb_Tipo_Documento_id"), rs.getString("tb_Tipo_Documento_nombre"), rs.getString("tb_Tipo_Documento_estado")));
                lista.add(objeto);
            }

            return lista;
        } catch (Exception e) {
            throw e;
        }
    }
    
    public List<DocumentoRecibido> ListarEstado(String inicio, String fin, String Estado) throws Exception {
        try {
            String sql = "";

            sql = sql + " select";
            sql = sql + " tb_Documento_Recibido.tb_Documento_Recibido_id";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_añoexpediente";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_numexpediente";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_numero";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_de";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_asunto";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_observacion";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_tipo";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_estado";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_id";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_nombre";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_estado";
            sql = sql + " from";
            sql = sql + " tb_Documento_Recibido";
            sql = sql + " inner join tb_Tipo_Documento on tb_Documento_Recibido.tb_Tipo_Documento_id = tb_Tipo_Documento.tb_Tipo_Documento_id";
            sql = sql + " where";
            sql = sql + " tb_Documento_Recibido.tb_Documento_Recibido_estado like '" + Estado + "%'";
            sql = sql + " and cast(tb_Documento_Recibido.tb_Documento_Recibido_añoexpediente as date) between '" + inicio + "' and '" + fin + "'";
            sql = sql + ";";

            List<DocumentoRecibido> lista = new ArrayList<DocumentoRecibido>();
            DocumentoRecibido objeto;

            PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = ps.executeQuery();

            rs.beforeFirst();

            while (rs.next()) {
                objeto = new DocumentoRecibido();

                objeto.setId(rs.getInt("tb_Documento_Recibido_id"));
                objeto.setAñoExpediente(rs.getTimestamp("tb_Documento_Recibido_añoexpediente"));
                objeto.setNumExpediente(rs.getString("tb_Documento_Recibido_numexpediente"));
                objeto.setNum(rs.getString("tb_Documento_Recibido_numero"));
                objeto.setDe(rs.getString("tb_Documento_Recibido_de"));
                objeto.setAsunto(rs.getString("tb_Documento_Recibido_asunto"));
                objeto.setObservacion(rs.getString("tb_Documento_Recibido_observacion"));
                objeto.setTipo(rs.getString("tb_Documento_Recibido_tipo"));
                objeto.setEstado(rs.getString("tb_Documento_Recibido_estado"));
                objeto.setTipoDocumento(new TipoDocumento(rs.getInt("tb_Tipo_Documento_id"), rs.getString("tb_Tipo_Documento_nombre"), rs.getString("tb_Tipo_Documento_estado")));
                lista.add(objeto);
            }

            return lista;
        } catch (Exception e) {
            throw e;
        }
    }
    
    public List<DocumentoRecibido> MostrarLista() throws Exception {
        try {
            String sql = "";

            sql = sql + " select";
            sql = sql + " tb_Documento_Recibido.tb_Documento_Recibido_id";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_añoexpediente";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_numexpediente";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_numero";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_de";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_asunto";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_observacion";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_tipo";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_estado";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_id";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_nombre";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_estado";
            sql = sql + " from";
            sql = sql + " tb_Documento_Recibido";
            sql = sql + " inner join tb_Tipo_Documento on tb_Documento_Recibido.tb_Tipo_Documento_id = tb_Tipo_Documento.tb_Tipo_Documento_id";
            sql = sql + ";";

            List<DocumentoRecibido> lista = new ArrayList<DocumentoRecibido>();
            DocumentoRecibido objeto;

            PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = ps.executeQuery();

            rs.beforeFirst();

            while (rs.next()) {
                objeto = new DocumentoRecibido();

                objeto.setId(rs.getInt("tb_Documento_Recibido_id"));
                objeto.setAñoExpediente(rs.getTimestamp("tb_Documento_Recibido_añoexpediente"));
                objeto.setNumExpediente(rs.getString("tb_Documento_Recibido_numexpediente"));
                objeto.setNum(rs.getString("tb_Documento_Recibido_numero"));
                objeto.setDe(rs.getString("tb_Documento_Recibido_de"));
                objeto.setAsunto(rs.getString("tb_Documento_Recibido_asunto"));
                objeto.setObservacion(rs.getString("tb_Documento_Recibido_observacion"));
                objeto.setTipo(rs.getString("tb_Documento_Recibido_tipo"));
                objeto.setEstado(rs.getString("tb_Documento_Recibido_estado"));
                objeto.setTipoDocumento(new TipoDocumento(rs.getInt("tb_Tipo_Documento_id"), rs.getString("tb_Tipo_Documento_nombre"), rs.getString("tb_Tipo_Documento_estado")));
                lista.add(objeto);
            }

            return lista;
        } catch (Exception e) {
            throw e;
        }
    }

    public DocumentoRecibido Ver(String id) throws Exception {
        try {
            String sql = "";

            sql = sql + " select";
            sql = sql + " tb_Documento_Recibido.tb_Documento_Recibido_id";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_añoexpediente";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_numexpediente";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_numero";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_de";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_asunto";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_observacion";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_tipo";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_estado";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_id";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_nombre";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_estado";
            sql = sql + " from";
            sql = sql + " tb_Documento_Recibido";
            sql = sql + " inner join tb_Tipo_Documento on tb_Documento_Recibido.tb_Tipo_Documento_id = tb_Tipo_Documento.tb_Tipo_Documento_id";
            sql = sql + " where";
            sql = sql + " tb_Documento_Recibido.tb_Documento_Recibido_numexpediente like '" + id + "%'";
            sql = sql + ";";

            DocumentoRecibido objeto = null;

            PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = ps.executeQuery();

            rs.beforeFirst();

            if (rs.next()) {
                objeto = new DocumentoRecibido();

                objeto.setId(rs.getInt("tb_Documento_Recibido_id"));
                objeto.setAñoExpediente(rs.getTimestamp("tb_Documento_Recibido_añoexpediente"));
                objeto.setNumExpediente(rs.getString("tb_Documento_Recibido_numexpediente"));
                objeto.setNum(rs.getString("tb_Documento_Recibido_numero"));
                objeto.setDe(rs.getString("tb_Documento_Recibido_de"));
                objeto.setAsunto(rs.getString("tb_Documento_Recibido_asunto"));
                objeto.setObservacion(rs.getString("tb_Documento_Recibido_observacion"));
                objeto.setTipo(rs.getString("tb_Documento_Recibido_tipo"));
                objeto.setEstado(rs.getString("tb_Documento_Recibido_estado"));
                objeto.setTipoDocumento(new TipoDocumento(rs.getInt("tb_Tipo_Documento_id"), rs.getString("tb_Tipo_Documento_nombre"), rs.getString("tb_Tipo_Documento_estado")));

            }
            return objeto;
        } catch (Exception e) {
            throw e;
        }
    }

    public void Insertar(DocumentoRecibido objeto) throws Exception {
        try {
            String dml = "";

            dml = dml + " insert into tb_Documento_Recibido(";
            dml = dml + " tb_Documento_Recibido_añoexpediente";
            dml = dml + ",tb_Documento_Recibido_numexpediente";
            dml = dml + ",tb_Documento_Recibido_numero";
            dml = dml + ",tb_Documento_Recibido_de";
            dml = dml + ",tb_Documento_Recibido_asunto";
            dml = dml + ",tb_Documento_Recibido_observacion";
            dml = dml + ",tb_Documento_Recibido_tipo";
            dml = dml + ",tb_Documento_Recibido_estado";
            dml = dml + ",tb_Tipo_Documento_id";
            dml = dml + ") values (";
            dml = dml + " '" + objeto.getAñoExpediente() + "'";
            dml = dml + ",'" + objeto.getNumExpediente() + "'";
            dml = dml + ",'" + objeto.getNum() + "'";
            dml = dml + ",'" + objeto.getDe() + "'";
            dml = dml + ",'" + objeto.getAsunto() + "'";
            dml = dml + ",'" + objeto.getObservacion() + "'";
            dml = dml + ",'" + objeto.getTipo() + "'";
            dml = dml + ",'" + objeto.getEstado() + "'";
            dml = dml + ",'" + objeto.getTipoDocumento().getId().toString() + "'";
            dml = dml + " )";
            dml = dml + ";";

            PreparedStatement ps = connection.prepareStatement(dml);

            ps.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
    }

    public void Actualizar(DocumentoRecibido objeto) throws Exception {
        try {
            String dml = "";

            dml = dml + " update tb_Documento_Recibido set";
            dml = dml + " tb_Documento_Recibido_añoexpediente = '" + objeto.getAñoExpediente() + "'";
            dml = dml + ",tb_Documento_Recibido_numexpediente = '" + objeto.getNumExpediente() + "'";
            dml = dml + ",tb_Documento_Recibido_numero = '" + objeto.getNum() + "'";
            dml = dml + ",tb_Documento_Recibido_de = '" + objeto.getDe() + "'";
            dml = dml + ",tb_Documento_Recibido_asunto = '" + objeto.getAsunto() + "'";
            dml = dml + ",tb_Documento_Recibido_observacion = '" + objeto.getObservacion() + "'";
            dml = dml + ",tb_Documento_Recibido_tipo = '" + objeto.getTipo() + "'";
            dml = dml + ",tb_Documento_Recibido_estado = '" + objeto.getEstado() + "'";
            dml = dml + ",tb_Tipo_Documento_id = '" + objeto.getTipoDocumento().getId().toString() + "'";
            dml = dml + " where tb_Documento_Recibido_id = " + objeto.getId().toString();
            dml = dml + ";";

            PreparedStatement ps = connection.prepareStatement(dml);

            ps.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
    }

    public void Eliminar(DocumentoRecibido objeto) throws Exception {
        try {
            String dml = "";

            dml = dml + " delete from tb_Documento_Recibido";
            dml = dml + " where tb_Documento_Recibido_id = " + objeto.getId().toString();
            dml = dml + ";";

            PreparedStatement ps = connection.prepareStatement(dml);

            ps.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
    }
}
