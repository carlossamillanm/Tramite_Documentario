package AccesoDatos;

import Entidades.Permiso;
import Entidades.UnidadOrganizativa;
import Entidades.UnidadTramite;
import Entidades.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class PermisoAD {

    private Connection connection;

    public PermisoAD(Connection connection) {
        this.connection = connection;
    }

    public List<Permiso> MostrarLista() throws Exception {
        try {
            String sql = "";

            sql = sql + " select";
            sql = sql + " tb_Permiso.tb_Permiso_id";
            sql = sql + ",tb_Permiso.tb_Permiso_fechinicio";
            sql = sql + ",tb_Permiso.tb_Permiso_fechtermino";
            sql = sql + ",tb_Permiso.tb_Permiso_tipo";
            sql = sql + ",tb_Usuario.tb_Usuario_id";
            sql = sql + ",tb_Usuario.tb_Usuario_apellipat";
            sql = sql + ",tb_Usuario.tb_Usuario_apellimat";
            sql = sql + ",tb_Usuario.tb_Usuario_nombre";
            sql = sql + ",tb_Usuario.tb_Usuario_correo";
            sql = sql + ",tb_Usuario.tb_Usuario_cuenta";
            sql = sql + ",tb_Usuario.tb_Usuario_clave";
            sql = sql + ",tb_Usuario.tb_Usuario_estado";
            sql = sql + ",tb_Usuario.tb_Usuario_tipo";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_id";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_nombre";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_abreviatura";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_responsable";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_estado";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_id";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_nombre";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_abreviatura";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_estado";
            sql = sql + " from";
            sql = sql + " tb_Permiso";
            sql = sql + " inner join tb_Unidad_Tramite on tb_Permiso.tb_Unidad_Tramite_id = tb_Unidad_Tramite.tb_Unidad_Tramite_id";
            sql = sql + " inner join tb_Usuario on tb_Permiso.tb_Usuario_id = tb_Usuario.tb_Usuario_id";
            sql = sql + " inner join tb_Unidad_Organizativa on tb_Unidad_Tramite.tb_Unidad_Organizativa_id = tb_Unidad_Organizativa.tb_Unidad_Organizativa_id";
            sql = sql + ";";

            List<Permiso> lista = new ArrayList<Permiso>();
            Permiso objeto;

            PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = ps.executeQuery();

            rs.beforeFirst();

            while (rs.next()) {
                objeto = new Permiso();

                objeto.setId(rs.getInt("tb_Permiso_id"));
                objeto.setFechainicio(rs.getDate("tb_Permiso_fechinicio"));
                objeto.setFechafin(rs.getDate("tb_Permiso_fechtermino"));
                objeto.setTipo(rs.getString("tb_Permiso_tipo"));
                objeto.setUsuario(new Usuario(rs.getInt("tb_Usuario_id"), rs.getString("tb_Usuario_apellipat"), rs.getString("tb_Usuario_apellimat"), rs.getString("tb_Usuario_nombre"), rs.getString("tb_Usuario_correo"), rs.getString("tb_Usuario_cuenta"), rs.getString("tb_Usuario_clave"), rs.getString("tb_Usuario_estado"), rs.getString("tb_Usuario_tipo")));
                objeto.setUnidadTramite(new UnidadTramite(rs.getInt("tb_Unidad_Tramite_id"), rs.getString("tb_Unidad_Tramite_nombre"), rs.getString("tb_Unidad_Tramite_abreviatura"), rs.getString("tb_Unidad_Tramite_responsable"), rs.getString("tb_Unidad_Tramite_estado"), new UnidadOrganizativa(rs.getInt("tb_Unidad_Organizativa_id"))));
                objeto.setUnidadOrganizativa(new UnidadOrganizativa(rs.getInt("tb_Unidad_Organizativa_id"), rs.getString("tb_Unidad_Organizativa_nombre"), rs.getString("tb_Unidad_Organizativa_abreviatura"), rs.getString("tb_Unidad_Organizativa_estado")));

                lista.add(objeto);
            }

            return lista;
        } catch (Exception e) {
            throw e;
        }
    }

    public List<Permiso> ListarTipo(String tipo) throws Exception {
        try {
            String sql = "";

            sql = sql + " select";
            sql = sql + " tb_Permiso.tb_Permiso_id";
            sql = sql + ",tb_Permiso.tb_Permiso_fechinicio";
            sql = sql + ",tb_Permiso.tb_Permiso_fechtermino";
            sql = sql + ",tb_Permiso.tb_Permiso_tipo";
            sql = sql + ",tb_Usuario.tb_Usuario_id";
            sql = sql + ",tb_Usuario.tb_Usuario_apellipat";
            sql = sql + ",tb_Usuario.tb_Usuario_apellimat";
            sql = sql + ",tb_Usuario.tb_Usuario_nombre";
            sql = sql + ",tb_Usuario.tb_Usuario_correo";
            sql = sql + ",tb_Usuario.tb_Usuario_cuenta";
            sql = sql + ",tb_Usuario.tb_Usuario_clave";
            sql = sql + ",tb_Usuario.tb_Usuario_estado";
            sql = sql + ",tb_Usuario.tb_Usuario_tipo";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_id";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_nombre";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_abreviatura";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_responsable";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_estado";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_id";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_nombre";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_abreviatura";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_estado";
            sql = sql + " from";
            sql = sql + " tb_Permiso";
            sql = sql + " inner join tb_Unidad_Tramite on tb_Permiso.tb_Unidad_Tramite_id = tb_Unidad_Tramite.tb_Unidad_Tramite_id";
            sql = sql + " inner join tb_Usuario on tb_Permiso.tb_Usuario_id = tb_Usuario.tb_Usuario_id";
            sql = sql + " inner join tb_Unidad_Organizativa on tb_Unidad_Tramite.tb_Unidad_Organizativa_id = tb_Unidad_Organizativa.tb_Unidad_Organizativa_id";
            sql = sql + " where";
            sql = sql + " tb_Permiso.tb_Permiso_tipo like '" + tipo + "%'";
            sql = sql + ";";

            List<Permiso> lista = new ArrayList<Permiso>();
            Permiso objeto;

            PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = ps.executeQuery();

            rs.beforeFirst();

            while (rs.next()) {
                objeto = new Permiso();

                objeto.setId(rs.getInt("tb_Permiso_id"));
                objeto.setFechainicio(rs.getDate("tb_Permiso_fechinicio"));
                objeto.setFechafin(rs.getDate("tb_Permiso_fechtermino"));
                objeto.setTipo(rs.getString("tb_Permiso_tipo"));
                objeto.setUsuario(new Usuario(rs.getInt("tb_Usuario_id"), rs.getString("tb_Usuario_apellipat"), rs.getString("tb_Usuario_apellimat"), rs.getString("tb_Usuario_nombre"), rs.getString("tb_Usuario_correo"), rs.getString("tb_Usuario_cuenta"), rs.getString("tb_Usuario_clave"), rs.getString("tb_Usuario_estado"), rs.getString("tb_Usuario_tipo")));
                objeto.setUnidadTramite(new UnidadTramite(rs.getInt("tb_Unidad_Tramite_id"), rs.getString("tb_Unidad_Tramite_nombre"), rs.getString("tb_Unidad_Tramite_abreviatura"), rs.getString("tb_Unidad_Tramite_responsable"), rs.getString("tb_Unidad_Tramite_estado"), new UnidadOrganizativa(rs.getInt("tb_Unidad_Organizativa_id"))));
                objeto.setUnidadOrganizativa(new UnidadOrganizativa(rs.getInt("tb_Unidad_Organizativa_id"), rs.getString("tb_Unidad_Organizativa_nombre"), rs.getString("tb_Unidad_Organizativa_abreviatura"), rs.getString("tb_Unidad_Organizativa_estado")));

                lista.add(objeto);
            }

            return lista;
        } catch (Exception e) {
            throw e;
        }
    }

    public Permiso Ver(Integer id) throws Exception {
        try {
            String sql = "";

            sql = sql + " select";
            sql = sql + " tb_Permiso.tb_Permiso_id";
            sql = sql + ",tb_Permiso.tb_Permiso_fechinicio";
            sql = sql + ",tb_Permiso.tb_Permiso_fechtermino";
            sql = sql + ",tb_Permiso.tb_Permiso_tipo";
            sql = sql + ",tb_Usuario.tb_Usuario_id";
            sql = sql + ",tb_Usuario.tb_Usuario_apellipat";
            sql = sql + ",tb_Usuario.tb_Usuario_apellimat";
            sql = sql + ",tb_Usuario.tb_Usuario_nombre";
            sql = sql + ",tb_Usuario.tb_Usuario_correo";
            sql = sql + ",tb_Usuario.tb_Usuario_cuenta";
            sql = sql + ",tb_Usuario.tb_Usuario_clave";
            sql = sql + ",tb_Usuario.tb_Usuario_estado";
            sql = sql + ",tb_Usuario.tb_Usuario_tipo";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_id";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_nombre";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_abreviatura";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_responsable";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_estado";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_id";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_nombre";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_abreviatura";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_estado";
            sql = sql + " from";
            sql = sql + " tb_Permiso";
            sql = sql + " inner join tb_Unidad_Tramite on tb_Permiso.tb_Unidad_Tramite_id = tb_Unidad_Tramite.tb_Unidad_Tramite_id";
            sql = sql + " inner join tb_Usuario on tb_Permiso.tb_Usuario_id = tb_Usuario.tb_Usuario_id";
            sql = sql + " inner join tb_Unidad_Organizativa on tb_Unidad_Tramite.tb_Unidad_Organizativa_id = tb_Unidad_Organizativa.tb_Unidad_Organizativa_id";
            sql = sql + " where";
            sql = sql + " tb_Usuario.tb_Usuario_id = " + id.toString();
            sql = sql + ";";

            Permiso objeto = null;

            PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = ps.executeQuery();

            rs.beforeFirst();

            if (rs.next()) {
                objeto = new Permiso();

                objeto.setId(rs.getInt("tb_Permiso_id"));
                objeto.setFechainicio(rs.getDate("tb_Permiso_fechinicio"));
                objeto.setFechafin(rs.getDate("tb_Permiso_fechtermino"));
                objeto.setTipo(rs.getString("tb_Permiso_tipo"));
                objeto.setUsuario(new Usuario(rs.getInt("tb_Usuario_id"), rs.getString("tb_Usuario_apellipat"), rs.getString("tb_Usuario_apellimat"), rs.getString("tb_Usuario_nombre"), rs.getString("tb_Usuario_correo"), rs.getString("tb_Usuario_cuenta"), rs.getString("tb_Usuario_clave"), rs.getString("tb_Usuario_estado"), rs.getString("tb_Usuario_tipo")));
                objeto.setUnidadTramite(new UnidadTramite(rs.getInt("tb_Unidad_Tramite_id"), rs.getString("tb_Unidad_Tramite_nombre"), rs.getString("tb_Unidad_Tramite_abreviatura"), rs.getString("tb_Unidad_Tramite_responsable"), rs.getString("tb_Unidad_Tramite_estado"), new UnidadOrganizativa(rs.getInt("tb_Unidad_Organizativa_id"))));
                objeto.setUnidadOrganizativa(new UnidadOrganizativa(rs.getInt("tb_Unidad_Organizativa_id"), rs.getString("tb_Unidad_Organizativa_nombre"), rs.getString("tb_Unidad_Organizativa_abreviatura"), rs.getString("tb_Unidad_Organizativa_estado")));

            }
            return objeto;
        } catch (Exception e) {
            throw e;
        }
    }

    public void Insertar(Permiso objeto) throws Exception {
        try {
            String dml = "";

            dml = dml + " insert into tb_Permiso(";
            dml = dml + " tb_Permiso_fechinicio";
            dml = dml + ",tb_Permiso_fechtermino";
            dml = dml + ",tb_Permiso_tipo";
            dml = dml + ",tb_Usuario_id";
            dml = dml + ",tb_Unidad_Tramite_id";
            dml = dml + ",tb_Unidad_Organizativa_id";
            dml = dml + ") values (";
            dml = dml + " '" + objeto.getFechainicio() + "'";
            dml = dml + ",'" + objeto.getFechafin() + "'";
            dml = dml + ",'" + objeto.getTipo() + "'";
            dml = dml + ",'" + objeto.getUsuario().getId().toString() + "'";
            dml = dml + ",'" + objeto.getUnidadTramite().getId().toString() + "'";
            dml = dml + ",'" + objeto.getUnidadOrganizativa().getId().toString() + "'";
            dml = dml + " )";
            dml = dml + ";";

            PreparedStatement ps = connection.prepareStatement(dml);

            ps.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
    }

    public void Actualizar(Permiso objeto) throws Exception {
        try {
            String dml = "";

            dml = dml + " update tb_Permiso set";
            dml = dml + " tb_Permiso_fechinicio = '" + objeto.getFechainicio() + "'";
            dml = dml + ",tb_Permiso_fechtermino = '" + objeto.getFechafin() + "'";
            dml = dml + ",tb_Permiso_tipo = '" + objeto.getTipo() + "'";
            dml = dml + ",tb_Usuario_id = '" + objeto.getUsuario().getId().toString() + "'";
            dml = dml + ",tb_Unidad_Tramite_id = '" + objeto.getUnidadTramite().getId().toString() + "'";
            dml = dml + ",tb_Unidad_Organizativa_id = '" + objeto.getUnidadOrganizativa().getId().toString() + "'";
            dml = dml + " where tb_Permiso_id = " + objeto.getId().toString();
            dml = dml + ";";

            PreparedStatement ps = connection.prepareStatement(dml);

            ps.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
    }

    public void Eliminar(Permiso objeto) throws Exception {
        try {
            String dml = "";

            dml = dml + " delete from tb_Permiso";
            dml = dml + " where tb_Permiso_id = " + objeto.getId().toString();
            dml = dml + ";";

            PreparedStatement ps = connection.prepareStatement(dml);

            ps.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
    }
}
