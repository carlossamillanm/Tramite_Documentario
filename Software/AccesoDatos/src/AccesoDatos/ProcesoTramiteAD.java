package AccesoDatos;

import Entidades.DocumentoEmitido;
import Entidades.DocumentoRecibido;
import Entidades.ProcesoTramite;
import Entidades.Ruta;
import Entidades.TipoDocumento;
import Entidades.Tramite;
import Entidades.UnidadOrganizativa;
import Entidades.UnidadTramite;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class ProcesoTramiteAD {

    private Connection connection;

    public ProcesoTramiteAD(Connection connection) {
        this.connection = connection;
    }

    public List<ProcesoTramite> ListarTodo() throws Exception {
        try {
            String sql = "";

            sql = sql + " select";
          sql = sql + " tb_Proceso_Tramite.tb_Proceso_Tramite_id";
            sql = sql + ",tb_Tramite.tb_Tramite_id";
            sql = sql + ",tb_Tramite.tb_Tramite_descripcion";
            sql = sql + ",tb_Tramite.tb_Tramite_tiptraslado";
            sql = sql + ",tb_Tramite.tb_Tramite_tipentidad";
            sql = sql + ",tb_Tramite.tb_Tramite_descripentidad";
            sql = sql + ",tb_Tramite.tb_Tramite_fecha_emision";
            sql = sql + ",tb_Ruta.tb_Ruta_id";
            sql = sql + ",tb_Ruta.tb_Ruta_fechhora";
            sql = sql + ",tb_Ruta.tb_Ruta_tipoexpediente";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_id";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_año";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_numero";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_fechemision";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_para";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_asunto";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_numexpediente";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_fechrecepcion";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_tipo";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_id";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_nombre";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_estado";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_id";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_nombre";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_abreviatura";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_responsable";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_estado";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_id";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_nombre";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_abreviatura";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_estado";
            sql = sql + " from";
            sql = sql + " tb_Proceso_Tramite";
            sql = sql + " inner join tb_Ruta on tb_Tramite.tb_Ruta_id = tb_Ruta.tb_Ruta_id";
            sql = sql + " inner join tb_Tramite on tb_Proceso_Tramite.tb_Tramite_id = tb_Tramite.tb_Tramite_id";
            sql = sql + " inner join tb_Documento_Emitido on tb_Tramite.tb_Documento_Emitido_id = tb_Documento_Emitido.tb_Documento_Emitido_id";
            sql = sql + " inner join tb_Unidad_Tramite on tb_Proceso_Tramite.tb_Unidad_Tramite_id = tb_Unidad_Tramite.tb_Unidad_Tramite_id";
            sql = sql + " inner join tb_Tipo_Documento on tb_Documento_Emitido.tb_Tipo_Documento_id = tb_Tipo_Documento.tb_Tipo_Documento_id";
            sql = sql + " inner join tb_Unidad_Organizativa on tb_Unidad_Tramite.tb_Unidad_Organizativa_id = tb_Unidad_Organizativa.tb_Unidad_Organizativa_id";
            sql = sql + ";";

            List<ProcesoTramite> lista = new ArrayList<ProcesoTramite>();
            ProcesoTramite objeto;

            PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = ps.executeQuery();

            rs.beforeFirst();

            while (rs.next()) {
                objeto = new ProcesoTramite();

                objeto.setId(rs.getInt("tb_Proceso_Tramite_id"));
                objeto.setTramite(new Tramite(rs.getInt("tb_Tramite_id"), rs.getString("tb_Tramite_descripcion"), rs.getString("tb_Tramite_tiptraslado"), rs.getString("tb_Tramite_tipentidad"), rs.getString("tb_Tramite_descripentidad"), rs.getTimestamp("tb_Tramite_fecha_emision"), new DocumentoEmitido(rs.getInt("tb_Documento_Emitido_id"), rs.getTimestamp("tb_Documento_Emitido_año"), rs.getString("tb_Documento_Emitido_numero"), rs.getTimestamp("tb_Documento_Emitido_fechemision"), rs.getString("tb_Documento_Emitido_para"), rs.getString("tb_Documento_Emitido_asunto"), rs.getString("tb_Documento_Emitido_numexpediente"), rs.getTimestamp("tb_Documento_Emitido_fechrecepcion"), rs.getString("tb_Documento_Emitido_tipo"), new TipoDocumento(rs.getInt("tb_Tipo_Documento_id"), rs.getString("tb_Tipo_Documento_nombre"), rs.getString("tb_Tipo_Documento_estado")), new UnidadTramite(rs.getInt("tb_Unidad_Tramite_id"), rs.getString("tb_Unidad_Tramite_nombre"), rs.getString("tb_Unidad_Tramite_abreviatura"), rs.getString("tb_Unidad_Tramite_responsable"), rs.getString("tb_Unidad_Tramite_estado"), new UnidadOrganizativa(rs.getInt("tb_Unidad_Organizativa_id"), rs.getString("tb_Unidad_Organizativa_nombre"), rs.getString("tb_Unidad_Organizativa_abreviatura"), rs.getString("tb_Unidad_Organizativa_estado ")))),new Ruta(rs.getInt("tb_Ruta_id"), rs.getTimestamp("tb_Ruta_fechhora"),rs.getString("tb_Ruta_tipoexpediente"), new DocumentoRecibido(rs.getInt("tb_Documento_Recibido_id")), new UnidadTramite(rs.getInt("tb_Unidad_Tramite_id")))));
                objeto.setUnidadTramite(new UnidadTramite(rs.getInt("tb_Unidad_Tramite_id"), rs.getString("tb_Unidad_Tramite_nombre"), rs.getString("tb_Unidad_Tramite_abreviatura"), rs.getString("tb_Unidad_Tramite_responsable"), rs.getString("tb_Unidad_Tramite_estado"), new UnidadOrganizativa(rs.getInt("tb_Unidad_Organizativa_id"), rs.getString("tb_Unidad_Organizativa_nombre"), rs.getString("tb_Unidad_Organizativa_abreviatura"), rs.getString("tb_Unidad_Organizativa_estado"))));

                lista.add(objeto);
            }

            return lista;
        } catch (Exception e) {
            throw e;
        }
    }

    public ProcesoTramite Ver(Integer id) throws Exception {
        try {
            String sql = "";

            sql = sql + " select";
            sql = sql + " tb_Proceso_Tramite.tb_Proceso_Tramite_id";
            sql = sql + ",tb_Tramite.tb_Tramite_id";
            sql = sql + ",tb_Tramite.tb_Tramite_descripcion";
            sql = sql + ",tb_Tramite.tb_Tramite_tiptraslado";
            sql = sql + ",tb_Tramite.tb_Tramite_tipentidad";
            sql = sql + ",tb_Tramite.tb_Tramite_descripentidad";
            sql = sql + ",tb_Tramite.tb_Tramite_fecha_emision";
            sql = sql + ",tb_Ruta.tb_Ruta_id";
            sql = sql + ",tb_Ruta.tb_Ruta_fechhora";
            sql = sql + ",tb_Ruta.tb_Ruta_tipoexpediente";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_id";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_año";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_numero";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_fechemision";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_para";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_asunto";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_numexpediente";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_fechrecepcion";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_tipo";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_id";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_nombre";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_estado";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_id";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_nombre";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_abreviatura";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_responsable";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_estado";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_id";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_nombre";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_abreviatura";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_estado";
            sql = sql + " from";
            sql = sql + " tb_Proceso_Tramite";
            sql = sql + " inner join tb_Ruta on tb_Tramite.tb_Ruta_id = tb_Ruta.tb_Ruta_id";
            sql = sql + " inner join tb_Tramite on tb_Proceso_Tramite.tb_Tramite_id = tb_Tramite.tb_Tramite_id";
            sql = sql + " inner join tb_Documento_Emitido on tb_Tramite.tb_Documento_Emitido_id = tb_Documento_Emitido.tb_Documento_Emitido_id";
            sql = sql + " inner join tb_Unidad_Tramite on tb_Proceso_Tramite.tb_Unidad_Tramite_id = tb_Unidad_Tramite.tb_Unidad_Tramite_id";
            sql = sql + " inner join tb_Tipo_Documento on tb_Documento_Emitido.tb_Tipo_Documento_id = tb_Tipo_Documento.tb_Tipo_Documento_id";
            sql = sql + " inner join tb_Unidad_Organizativa on tb_Unidad_Tramite.tb_Unidad_Organizativa_id = tb_Unidad_Organizativa.tb_Unidad_Organizativa_id";
            sql = sql + " where";
            sql = sql + " tb_Proceso_Tramite.tb_Proceso_Tramite_id =" + id.toString();
            sql = sql + ";";

            ProcesoTramite objeto = null;

            PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = ps.executeQuery();

            rs.beforeFirst();

            if (rs.next()) {
                objeto = new ProcesoTramite();

                objeto.setId(rs.getInt("tb_Proceso_Tramite_id"));
                objeto.setTramite(new Tramite(rs.getInt("tb_Tramite_id"), rs.getString("tb_Tramite_descripcion"), rs.getString("tb_Tramite_tiptraslado"), rs.getString("tb_Tramite_tipentidad"), rs.getString("tb_Tramite_descripentidad"), rs.getTimestamp("tb_Tramite_fecha_emision"), new DocumentoEmitido(rs.getInt("tb_Documento_Emitido_id"), rs.getTimestamp("tb_Documento_Emitido_año"), rs.getString("tb_Documento_Emitido_numero"), rs.getTimestamp("tb_Documento_Emitido_fechemision"), rs.getString("tb_Documento_Emitido_para"), rs.getString("tb_Documento_Emitido_asunto"), rs.getString("tb_Documento_Emitido_numexpediente"), rs.getTimestamp("tb_Documento_Emitido_fechrecepcion"), rs.getString("tb_Documento_Emitido_tipo"), new TipoDocumento(rs.getInt("tb_Tipo_Documento_id"), rs.getString("tb_Tipo_Documento_nombre"), rs.getString("tb_Tipo_Documento_estado")), new UnidadTramite(rs.getInt("tb_Unidad_Tramite_id"), rs.getString("tb_Unidad_Tramite_nombre"), rs.getString("tb_Unidad_Tramite_abreviatura"), rs.getString("tb_Unidad_Tramite_responsable"), rs.getString("tb_Unidad_Tramite_estado"), new UnidadOrganizativa(rs.getInt("tb_Unidad_Organizativa_id"), rs.getString("tb_Unidad_Organizativa_nombre"), rs.getString("tb_Unidad_Organizativa_abreviatura"), rs.getString("tb_Unidad_Organizativa_estado ")))),new Ruta(rs.getInt("tb_Ruta_id"), rs.getTimestamp("tb_Ruta_fechhora"),rs.getString("tb_Ruta_tipoexpediente"), new DocumentoRecibido(rs.getInt("tb_Documento_Recibido_id")), new UnidadTramite(rs.getInt("tb_Unidad_Tramite_id")))));
                objeto.setUnidadTramite(new UnidadTramite(rs.getInt("tb_Unidad_Tramite_id"), rs.getString("tb_Unidad_Tramite_nombre"), rs.getString("tb_Unidad_Tramite_abreviatura"), rs.getString("tb_Unidad_Tramite_responsable"), rs.getString("tb_Unidad_Tramite_estado"), new UnidadOrganizativa(rs.getInt("tb_Unidad_Organizativa_id"), rs.getString("tb_Unidad_Organizativa_nombre"), rs.getString("tb_Unidad_Organizativa_abreviatura"), rs.getString("tb_Unidad_Organizativa_estado"))));

            }
            return objeto;
        } catch (Exception e) {
            throw e;
        }
    }

    public void Insertar(ProcesoTramite objeto) throws Exception {
        try {
            String dml = "";

            dml = dml + " insert into tb_Proceso_Tramite(";
            dml = dml + " tb_Tramite_id";
            dml = dml + ",tb_Unidad_Tramite_id";
            dml = dml + ") values (";
            dml = dml + " '" + objeto.getTramite().getId().toString() + "'";
            dml = dml + ",'" + objeto.getUnidadTramite().getId().toString() + "'";
            dml = dml + " )";
            dml = dml + ";";

            PreparedStatement ps = connection.prepareStatement(dml);

            ps.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
    }

    public void Actualizar(ProcesoTramite objeto) throws Exception {
        try {
            String dml = "";

            dml = dml + " update tb_Proceso_Tramite set";
            dml = dml + " tb_Tramite_id = '" + objeto.getTramite().getId().toString() + "'";
            dml = dml + ",tb_Unidad_Tramite_id = '" + objeto.getUnidadTramite().getId().toString() + "'";
            dml = dml + " where tb_Proceso_Tramite_id = " + objeto.getId().toString();
            dml = dml + ";";

            PreparedStatement ps = connection.prepareStatement(dml);

            ps.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
    }

    public void Eliminar(ProcesoTramite objeto) throws Exception {
        try {
            String dml = "";

            dml = dml + " delete from tb_Proceso_Tramite";
            dml = dml + " where tb_Proceso_Tramite_id = " + objeto.getId().toString();
            dml = dml + ";";

            PreparedStatement ps = connection.prepareStatement(dml);

            ps.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
    }
}
