package AccesoDatos;

import Entidades.DocumentoRecibido;
import Entidades.Requisitos;
import Entidades.TipoDocumento;
import Entidades.UnidadOrganizativa;
import Entidades.RequisitosConcluidos;
import Entidades.TipoTramite;
import Entidades.UnidadTramite;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class RequisitoConcluidosAD {

    private Connection connection;

    public RequisitoConcluidosAD(Connection connection) {
        this.connection = connection;
    }

    public List<RequisitosConcluidos> Listar(Integer id) throws Exception {
        try {
            String sql = "";

            sql = sql + " select";
            sql = sql + " tb_Requisito_Concluidos.tb_Requisito_Concluidos_id";
            sql = sql + ",tb_Requisitos.tb_Requisitos_id";
            sql = sql + ",tb_Requisitos.tb_Requisitos_descripcion";
            sql = sql + ",tb_Requisitos.tb_Requisitos_estado";
            sql = sql + ",tb_Tipo_Tramite.tb_Tipo_Tramite_id";
            sql = sql + ",tb_Tipo_Tramite.tb_Tipo_Tramite_nombre";
            sql = sql + ",tb_Tipo_Tramite.tb_Tipo_Tramite_tiempoestimado";
            sql = sql + ",tb_Tipo_Tramite.tb_Tipo_Tramite_estado";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_id";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_añoexpediente";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_numexpediente";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_numero";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_de";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_asunto";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_observacion";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_tipo";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_estado";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_id";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_nombre";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_estado";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_id";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_nombre";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_abreviatura";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_responsable";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_estado";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_id";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_nombre";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_abreviatura";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_estado";
            sql = sql + " from";
            sql = sql + " tb_Requisito_Concluidos";
            sql = sql + " inner join tb_Requisitos on tb_Requisito_Concluidos.tb_Requisitos_id = tb_Requisitos.tb_Requisitos_id";
            sql = sql + " inner join tb_Documento_Recibido on tb_Requisito_Concluidos.tb_Documento_Recibido_id = tb_Documento_Recibido.tb_Documento_Recibido_id";
            sql = sql + " inner join tb_Tipo_Tramite on tb_Requisitos.tb_Tipo_Tramite_id = tb_Tipo_Tramite.tb_Tipo_Tramite_id";
            sql = sql + " inner join tb_Unidad_Tramite on tb_Tipo_Tramite.tb_Unidad_Tramite_id = tb_Unidad_Tramite.tb_Unidad_Tramite_id";
            sql = sql + " inner join tb_Tipo_Documento on tb_Documento_Recibido.tb_Tipo_Documento_id = tb_Tipo_Documento.tb_Tipo_Documento_id";
            sql = sql + " inner join tb_Unidad_Organizativa on tb_Unidad_Tramite.tb_Unidad_Organizativa_id = tb_Unidad_Organizativa.tb_Unidad_Organizativa_id";
             sql = sql + " where";
            sql = sql + " tb_Documento_Recibido.tb_Documento_Recibido_id =" + id.toString();
            sql = sql + ";";

            List<RequisitosConcluidos> lista = new ArrayList<RequisitosConcluidos>();
            RequisitosConcluidos objeto;

            PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = ps.executeQuery();

            rs.beforeFirst();

            while (rs.next()) {
                objeto = new RequisitosConcluidos();

                objeto.setId(rs.getInt("tb_Requisito_Concluidos_id"));
                objeto.setDocumentoRecibido(new DocumentoRecibido(rs.getInt("tb_Documento_Recibido_id"), rs.getTimestamp("tb_Documento_Recibido_añoexpediente"), rs.getString("tb_Documento_Recibido_numexpediente"), rs.getString("tb_Documento_Recibido_numero"), rs.getString("tb_Documento_Recibido_de"), rs.getString("tb_Documento_Recibido_asunto"), rs.getString("tb_Documento_Recibido_observacion"),rs.getString("tb_Documento_Recibido_tipo"), rs.getString("tb_Documento_Recibido_estado"), new TipoDocumento(rs.getInt("tb_Tipo_Documento_id"), rs.getString("tb_Tipo_Documento_nombre"), rs.getString("tb_Tipo_Documento_estado"))));
                objeto.setRequisitos(new Requisitos(rs.getInt("tb_Requisitos_id"), rs.getString("tb_Requisitos_descripcion"), rs.getString("tb_Requisitos_estado"), new TipoTramite(rs.getInt("tb_Tipo_Tramite_id"), rs.getString("tb_Tipo_Tramite_nombre"), rs.getInt("tb_Tipo_Tramite_tiempoestimado"), rs.getString("tb_Tipo_Tramite_estado"), new UnidadTramite(rs.getInt("tb_Unidad_Tramite_id"), rs.getString("tb_Unidad_Tramite_nombre"), rs.getString("tb_Unidad_Tramite_abreviatura"), rs.getString("tb_Unidad_Tramite_responsable"), rs.getString("tb_Unidad_Tramite_estado"), new UnidadOrganizativa(rs.getInt("tb_Unidad_Organizativa_id"), rs.getString("tb_Unidad_Organizativa_nombre"), rs.getString("tb_Unidad_Organizativa_abreviatura"), rs.getString("tb_Unidad_Organizativa_estado"))))));

                lista.add(objeto);
            }

            return lista;
        } catch (Exception e) {
            throw e;
        }
    }

    public RequisitosConcluidos Ver(Integer id) throws Exception {
        try {
            String sql = "";

            sql = sql + " select";
            sql = sql + " tb_Requisito_Concluidos.tb_Requisito_Concluidos_id";
            sql = sql + ",tb_Requisitos.tb_Requisitos_id";
            sql = sql + ",tb_Requisitos.tb_Requisitos_descripcion";
            sql = sql + ",tb_Requisitos.tb_Requisitos_estado";
            sql = sql + ",tb_Tipo_Tramite.tb_Tipo_Tramite_id";
            sql = sql + ",tb_Tipo_Tramite.tb_Tipo_Tramite_nombre";
            sql = sql + ",tb_Tipo_Tramite.tb_Tipo_Tramite_tiempoestimado";
            sql = sql + ",tb_Tipo_Tramite.tb_Tipo_Tramite_estado";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_id";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_añoexpediente";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_numexpediente";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_numero";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_de";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_asunto";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_observacion";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_tipo";
            sql = sql + ",tb_Documento_Recibido.tb_Documento_Recibido_estado";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_id";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_nombre";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_estado";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_id";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_nombre";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_abreviatura";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_responsable";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_estado";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_id";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_nombre";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_abreviatura";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_estado";
            sql = sql + " from";
            sql = sql + " tb_Requisito_Concluidos";
            sql = sql + " inner join tb_Requisitos on tb_Requisito_Concluidos.tb_Requisitos_id = tb_Requisitos.tb_Requisitos_id";
            sql = sql + " inner join tb_Documento_Recibido on tb_Requisito_Concluidos.tb_Documento_Recibido_id = tb_Documento_Recibido.tb_Documento_Recibido_id";
            sql = sql + " inner join tb_Tipo_Tramite on tb_Requisitos.tb_Tipo_Tramite_id = tb_Tipo_Tramite.tb_Tipo_Tramite_id";
            sql = sql + " inner join tb_Unidad_Tramite on tb_Tipo_Tramite.tb_Unidad_Tramite_id = tb_Unidad_Tramite.tb_Unidad_Tramite_id";
            sql = sql + " inner join tb_Tipo_Documento on tb_Documento_Recibido.tb_Tipo_Documento_id = tb_Tipo_Documento.tb_Tipo_Documento_id";
            sql = sql + " inner join tb_Unidad_Organizativa on tb_Unidad_Tramite.tb_Unidad_Organizativa_id = tb_Unidad_Organizativa.tb_Unidad_Organizativa_id";
            sql = sql + " where";
            sql = sql + " tb_Documento_Recibido.tb_Documento_Recibido_id =" + id.toString();
            sql = sql + ";";

            RequisitosConcluidos objeto = null;

            PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = ps.executeQuery();

            rs.beforeFirst();

            if (rs.next()) {
                objeto = new RequisitosConcluidos();

                objeto.setId(rs.getInt("tb_Requisito_Concluidos_id"));
                objeto.setDocumentoRecibido(new DocumentoRecibido(rs.getInt("tb_Documento_Recibido_id"), rs.getTimestamp("tb_Documento_Recibido_añoexpediente"), rs.getString("tb_Documento_Recibido_numexpediente"), rs.getString("tb_Documento_Recibido_numero"), rs.getString("tb_Documento_Recibido_de"), rs.getString("tb_Documento_Recibido_asunto"), rs.getString("tb_Documento_Recibido_observacion"),rs.getString("tb_Documento_Recibido_tipo"), rs.getString("tb_Documento_Recibido_estado"), new TipoDocumento(rs.getInt("tb_Tipo_Documento_id"), rs.getString("tb_Tipo_Documento_nombre"), rs.getString("tb_Tipo_Documento_estado"))));
                objeto.setRequisitos(new Requisitos(rs.getInt("tb_Requisitos_id"), rs.getString("tb_Requisitos_descripcion"), rs.getString("tb_Requisitos_estado"), new TipoTramite(rs.getInt("tb_Tipo_Tramite_id"), rs.getString("tb_Tipo_Tramite_nombre"), rs.getInt("tb_Tipo_Tramite_tiempoestimado"), rs.getString("tb_Tipo_Tramite_estado"), new UnidadTramite(rs.getInt("tb_Unidad_Tramite_id"), rs.getString("tb_Unidad_Tramite_nombre"), rs.getString("tb_Unidad_Tramite_abreviatura"), rs.getString("tb_Unidad_Tramite_responsable"), rs.getString("tb_Unidad_Tramite_estado"), new UnidadOrganizativa(rs.getInt("tb_Unidad_Organizativa_id"), rs.getString("tb_Unidad_Organizativa_nombre"), rs.getString("tb_Unidad_Organizativa_abreviatura"), rs.getString("tb_Unidad_Organizativa_estado"))))));

            }
            return objeto;
        } catch (Exception e) {
            throw e;
        }
    }

    public void Insertar(RequisitosConcluidos objeto) throws Exception {
        try {
            String dml = "";

            dml = dml + " insert into tb_Requisito_Concluidos(";
            dml = dml + " tb_Requisitos_id";
            dml = dml + ",tb_Documento_Recibido_id";
            dml = dml + ") values (";
            dml = dml + " '" + objeto.getRequisitos().getId().toString() + "'";
            dml = dml + ",'" + objeto.getDocumentoRecibido().getId().toString() + "'";
            dml = dml + " )";
            dml = dml + ";";

            PreparedStatement ps = connection.prepareStatement(dml);

            ps.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
    }

    public void Actualizar(RequisitosConcluidos objeto) throws Exception {
        try {
            String dml = "";

            dml = dml + " update tb_Requisito_Concluidos set";
            dml = dml + " tb_Requisitos_id = '" + objeto.getRequisitos().getId().toString() + "'";
            dml = dml + ",tb_Documento_Recibido_id = '" + objeto.getDocumentoRecibido().getId().toString() + "'";
            dml = dml + " where tb_Requisito_Concluidos_id = " + objeto.getId().toString();
            dml = dml + ";";

            PreparedStatement ps = connection.prepareStatement(dml);

            ps.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
    }

    public void Eliminar(RequisitosConcluidos objeto) throws Exception {
        try {
            String dml = "";

            dml = dml + " delete from tb_Requisito_Concluidos";
            dml = dml + " where tb_Requisito_Concluidos_id = " + objeto.getId().toString();
            dml = dml + ";";

            PreparedStatement ps = connection.prepareStatement(dml);

            ps.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
    }
}
