package AccesoDatos;

import Entidades.UnidadOrganizativa;
import Entidades.Requisitos;
import Entidades.TipoTramite;
import Entidades.UnidadTramite;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class RequisitosAD {

    private Connection connection;

    public RequisitosAD(Connection connection) {
        this.connection = connection;
    }

    public List<Requisitos> ListarTodo() throws Exception {
        try {
            String sql = "";

            sql = sql + " select";
            sql = sql + " tb_Requisitos.tb_Requisitos_id";
            sql = sql + ",tb_Requisitos.tb_Requisitos_descripcion";
            sql = sql + ",tb_Requisitos.tb_Requisitos_estado";
            sql = sql + ",tb_Tipo_Tramite.tb_Tipo_Tramite_id";
            sql = sql + ",tb_Tipo_Tramite.tb_Tipo_Tramite_nombre";
            sql = sql + ",tb_Tipo_Tramite.tb_Tipo_Tramite_tiempoestimado";
            sql = sql + ",tb_Tipo_Tramite.tb_Tipo_Tramite_estado";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_id";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_nombre";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_abreviatura";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_responsable";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_estado";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_id";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_nombre";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_abreviatura";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_estado";
            sql = sql + " from";
            sql = sql + " tb_Requisitos";
            sql = sql + " inner join tb_Tipo_Tramite on tb_Requisitos.tb_Tipo_Tramite_id = tb_Tipo_Tramite.tb_Tipo_Tramite_id";
            sql = sql + " inner join tb_Unidad_Tramite on tb_Tipo_Tramite.tb_Unidad_Tramite_id = tb_Unidad_Tramite.tb_Unidad_Tramite_id";
            sql = sql + " inner join tb_Unidad_Organizativa on tb_Unidad_Tramite.tb_Unidad_Organizativa_id = tb_Unidad_Organizativa.tb_Unidad_Organizativa_id";
            sql = sql + ";";

            List<Requisitos> lista = new ArrayList<Requisitos>();
            Requisitos objeto;

            PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = ps.executeQuery();

            rs.beforeFirst();

            while (rs.next()) {
                objeto = new Requisitos();

                objeto.setId(rs.getInt("tb_Requisitos_id"));
                objeto.setDescripcion(rs.getString("tb_Requisitos_descripcion"));
                objeto.setEstado(rs.getString("tb_Requisitos_estado"));
                objeto.setTipoTramite(new TipoTramite(rs.getInt("tb_Tipo_Tramite_id"), rs.getString("tb_Tipo_Tramite_nombre"), rs.getInt("tb_Tipo_Tramite_tiempoestimado"), rs.getString("tb_Tipo_Tramite_estado"), new UnidadTramite(rs.getInt("tb_Unidad_Tramite_id"), rs.getString("tb_Unidad_Tramite_nombre"), rs.getString("tb_Unidad_Tramite_abreviatura"), rs.getString("tb_Unidad_Tramite_responsable"), rs.getString("tb_Unidad_Tramite_estado"), new UnidadOrganizativa(rs.getInt("tb_Unidad_Organizativa_id"), rs.getString("tb_Unidad_Organizativa_nombre"), rs.getString("tb_Unidad_Organizativa_abreviatura"), rs.getString("tb_Unidad_Organizativa_estado")))));
                lista.add(objeto);
            }

            return lista;
        } catch (Exception e) {
            throw e;
        }
    }

    public List<Requisitos> ListarTipo(Integer id) throws Exception {
        try {
            String sql = "";

            sql = sql + " select";
            sql = sql + " tb_Requisitos.tb_Requisitos_id";
            sql = sql + ",tb_Requisitos.tb_Requisitos_descripcion";
            sql = sql + ",tb_Requisitos.tb_Requisitos_estado";
            sql = sql + ",tb_Tipo_Tramite.tb_Tipo_Tramite_id";
            sql = sql + ",tb_Tipo_Tramite.tb_Tipo_Tramite_nombre";
            sql = sql + ",tb_Tipo_Tramite.tb_Tipo_Tramite_tiempoestimado";
            sql = sql + ",tb_Tipo_Tramite.tb_Tipo_Tramite_estado";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_id";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_nombre";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_abreviatura";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_responsable";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_estado";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_id";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_nombre";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_abreviatura";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_estado";
            sql = sql + " from";
            sql = sql + " tb_Requisitos";
            sql = sql + " inner join tb_Tipo_Tramite on tb_Requisitos.tb_Tipo_Tramite_id = tb_Tipo_Tramite.tb_Tipo_Tramite_id";
            sql = sql + " inner join tb_Unidad_Tramite on tb_Tipo_Tramite.tb_Unidad_Tramite_id = tb_Unidad_Tramite.tb_Unidad_Tramite_id";
            sql = sql + " inner join tb_Unidad_Organizativa on tb_Unidad_Tramite.tb_Unidad_Organizativa_id = tb_Unidad_Organizativa.tb_Unidad_Organizativa_id";
            sql = sql + " where";
            sql = sql + " tb_Tipo_Tramite.tb_Tipo_Tramite_id=" + id.toString();
            sql = sql + ";";

            List<Requisitos> lista = new ArrayList<Requisitos>();
            Requisitos objeto;

            PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = ps.executeQuery();

            rs.beforeFirst();

            while (rs.next()) {
                objeto = new Requisitos();

                objeto.setId(rs.getInt("tb_Requisitos_id"));
                objeto.setDescripcion(rs.getString("tb_Requisitos_descripcion"));
                objeto.setEstado(rs.getString("tb_Requisitos_estado"));
                objeto.setTipoTramite(new TipoTramite(rs.getInt("tb_Tipo_Tramite_id"), rs.getString("tb_Tipo_Tramite_nombre"), rs.getInt("tb_Tipo_Tramite_tiempoestimado"), rs.getString("tb_Tipo_Tramite_estado"), new UnidadTramite(rs.getInt("tb_Unidad_Tramite_id"), rs.getString("tb_Unidad_Tramite_nombre"), rs.getString("tb_Unidad_Tramite_abreviatura"), rs.getString("tb_Unidad_Tramite_responsable"), rs.getString("tb_Unidad_Tramite_estado"), new UnidadOrganizativa(rs.getInt("tb_Unidad_Organizativa_id"), rs.getString("tb_Unidad_Organizativa_nombre"), rs.getString("tb_Unidad_Organizativa_abreviatura"), rs.getString("tb_Unidad_Organizativa_estado")))));
                lista.add(objeto);
            }

            return lista;
        } catch (Exception e) {
            throw e;
        }
    }

    public Requisitos ver(Integer id) throws Exception {
        try {
            String sql = "";

            sql = sql + " select";
            sql = sql + " tb_Requisitos.tb_Requisitos_id";
            sql = sql + ",tb_Requisitos.tb_Requisitos_descripcion";
            sql = sql + ",tb_Requisitos.tb_Requisitos_estado";
            sql = sql + ",tb_Tipo_Tramite.tb_Tipo_Tramite_id";
            sql = sql + ",tb_Tipo_Tramite.tb_Tipo_Tramite_nombre";
            sql = sql + ",tb_Tipo_Tramite.tb_Tipo_Tramite_tiempoestimado";
            sql = sql + ",tb_Tipo_Tramite.tb_Tipo_Tramite_estado";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_id";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_nombre";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_abreviatura";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_responsable";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_estado";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_id";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_nombre";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_abreviatura";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_estado";
            sql = sql + " from";
            sql = sql + " tb_Requisitos";
            sql = sql + " inner join tb_Tipo_Tramite on tb_Requisitos.tb_Tipo_Tramite_id = tb_Tipo_Tramite.tb_Tipo_Tramite_id";
            sql = sql + " inner join tb_Unidad_Tramite on tb_Tipo_Tramite.tb_Unidad_Tramite_id = tb_Unidad_Tramite.tb_Unidad_Tramite_id";
            sql = sql + " inner join tb_Unidad_Organizativa on tb_Unidad_Tramite.tb_Unidad_Organizativa_id = tb_Unidad_Organizativa.tb_Unidad_Organizativa_id";
            sql = sql + " where";
            sql = sql + " tb_Requisitos.tb_Requisitos_id=" + id.toString();
            sql = sql + ";";

            Requisitos objeto = null;

            PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = ps.executeQuery();

            rs.beforeFirst();

            if (rs.next()) {
                objeto = new Requisitos();

                objeto.setId(rs.getInt("tb_Requisitos_id"));
                objeto.setDescripcion(rs.getString("tb_Requisitos_descripcion"));
                objeto.setEstado(rs.getString("tb_Requisitos_estado"));
                objeto.setTipoTramite(new TipoTramite(rs.getInt("tb_Tipo_Tramite_id"), rs.getString("tb_Tipo_Tramite_nombre"), rs.getInt("tb_Tipo_Tramite_tiempoestimado"), rs.getString("tb_Tipo_Tramite_estado"), new UnidadTramite(rs.getInt("tb_Unidad_Tramite_id"), rs.getString("tb_Unidad_Tramite_nombre"), rs.getString("tb_Unidad_Tramite_abreviatura"), rs.getString("tb_Unidad_Tramite_responsable"), rs.getString("tb_Unidad_Tramite_estado"), new UnidadOrganizativa(rs.getInt("tb_Unidad_Organizativa_id"), rs.getString("tb_Unidad_Organizativa_nombre"), rs.getString("tb_Unidad_Organizativa_abreviatura"), rs.getString("tb_Unidad_Organizativa_estado")))));
            }
            return objeto;
        } catch (Exception e) {
            throw e;
        }
    }

    public void Insertar(Requisitos objeto) throws Exception {
        try {
            String dml = "";

            dml = dml + " insert into tb_Requisitos(";
            dml = dml + " tb_Requisitos_descripcion";
            dml = dml + ",tb_Requisitos_estado";
            dml = dml + ",tb_Tipo_Tramite_id";
            dml = dml + ") values (";
            dml = dml + " '" + objeto.getDescripcion() + "'";
            dml = dml + ",'" + objeto.getEstado() + "'";
            dml = dml + ",'" + objeto.getTipoTramite().getId().toString() + "'";
            dml = dml + " )";
            dml = dml + ";";

            PreparedStatement ps = connection.prepareStatement(dml);

            ps.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
    }

    public void Actualizar(Requisitos objeto) throws Exception {
        try {
            String dml = "";

            dml = dml + " update tb_Requisitos set";
            dml = dml + " tb_Requisitos_descripcion = '" + objeto.getDescripcion() + "'";
            dml = dml + ",tb_Requisitos_estado = '" + objeto.getEstado() + "'";
            dml = dml + ",tb_Tipo_Tramite_id = '" + objeto.getTipoTramite().getId().toString() + "'";
            dml = dml + " where tb_Requisitos_id = " + objeto.getId().toString();
            dml = dml + ";";

            PreparedStatement ps = connection.prepareStatement(dml);

            ps.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
    }

    public void Eliminar(Requisitos objeto) throws Exception {
        try {
            String dml = "";

            dml = dml + " delete from tb_Requisitos";
            dml = dml + " where tb_Requisitos_id = " + objeto.getId().toString();
            dml = dml + ";";

            PreparedStatement ps = connection.prepareStatement(dml);

            ps.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
    }
}
