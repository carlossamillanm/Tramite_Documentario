package AccesoDatos;

import Entidades.TipoDocumento;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class TipoDocumentoAD {
    private Connection connection;

    public TipoDocumentoAD(Connection connection) {
        this.connection = connection;
    }

        public List<TipoDocumento> ListaTotal() throws Exception {
        try {
            String sql = "";

            sql = sql + " select";
            sql = sql + " tb_Tipo_Documento_id";
            sql = sql + ",tb_Tipo_Documento_nombre";
            sql = sql + ",tb_Tipo_Documento_estado";
            sql = sql + " from tb_Tipo_Documento";
            sql = sql + ";";

            List<TipoDocumento> lista = new ArrayList<TipoDocumento>();
            TipoDocumento objeto;

            PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = ps.executeQuery();

            rs.beforeFirst();

            while (rs.next()) {
                objeto = new TipoDocumento();

                objeto.setId(rs.getInt("tb_Tipo_Documento_id"));
                objeto.setNombre(rs.getString("tb_Tipo_Documento_nombre"));
                objeto.setEstado(rs.getString("tb_Tipo_Documento_estado"));

                lista.add(objeto);
            }

            return lista;
        } catch (Exception e) {
            throw e;
        }
    }
    
    public List<TipoDocumento> ListarNombre(String nombre) throws Exception {
        try {
            String sql = "";

            sql = sql + " select";
            sql = sql + " tb_Tipo_Documento_id";
            sql = sql + ",tb_Tipo_Documento_nombre";
            sql = sql + ",tb_Tipo_Documento_estado";
            sql = sql + " from tb_Tipo_Documento";
            sql = sql + " where tb_Tipo_Documento_nombre like '%" + nombre + "%'";
            sql = sql + ";";

            List<TipoDocumento> lista = new ArrayList<TipoDocumento>();
            TipoDocumento objeto;

            PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = ps.executeQuery();

            rs.beforeFirst();

            while (rs.next()) {
                objeto = new TipoDocumento();

                objeto.setId(rs.getInt("tb_Tipo_Documento_id"));
                objeto.setNombre(rs.getString("tb_Tipo_Documento_nombre"));
                objeto.setEstado(rs.getString("tb_Tipo_Documento_estado"));

                lista.add(objeto);
            }

            return lista;
        } catch (Exception e) {
            throw e;
        }
    }

    public TipoDocumento Ver(Integer id) throws Exception {
        try {
            String sql = "";

            sql = sql + " select";
            sql = sql + " tb_Tipo_Documento_id";
            sql = sql + ",tb_Tipo_Documento_nombre";
            sql = sql + ",tb_Tipo_Documento_estado";
            sql = sql + " from tb_Tipo_Documento";
            sql = sql + " where tb_Tipo_Documento_id = " + id.toString();
            sql = sql + ";";

            TipoDocumento objeto = null;

            PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = ps.executeQuery();

            rs.beforeFirst();

            if (rs.next()) {
                objeto = new TipoDocumento();

                objeto.setId(rs.getInt("tb_Tipo_Documento_id"));
                objeto.setNombre(rs.getString("tb_Tipo_Documento_nombre"));
                objeto.setEstado(rs.getString("tb_Tipo_Documento_estado"));
            }

            return objeto;
        } catch (Exception e) {
            throw e;
        }
    }

    public void Insertar(TipoDocumento objeto) throws Exception {
        try {
            String dml = "";

            dml = dml + " insert into tb_Tipo_Documento(";
            dml = dml + " tb_Tipo_Documento_nombre";
            dml = dml + ",tb_Tipo_Documento_estado";
            dml = dml + ") values (";
            dml = dml + " '" + objeto.getNombre() + "'";
            dml = dml + ",'" + objeto.getEstado() + "'";
            dml = dml + " )";
            dml = dml + ";";

            PreparedStatement ps = connection.prepareStatement(dml);

            ps.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
    }

    public void Actualizar(TipoDocumento objeto) throws Exception {
        try {
            String dml = "";

            dml = dml + " update tb_Tipo_Documento set";
            dml = dml + " tb_Tipo_Documento_nombre = '" + objeto.getNombre() + "'";
            dml = dml + ",tb_Tipo_Documento_estado = '" + objeto.getEstado() + "'";
            dml = dml + " where tb_Tipo_Documento_id = " + objeto.getId().toString();
            dml = dml + ";";

            PreparedStatement ps = connection.prepareStatement(dml);

            ps.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
    }

    public void Eliminar(TipoDocumento objeto) throws Exception {
        try {
            String dml = "";

            dml = dml + " delete from tb_Tipo_Documento";
            dml = dml + " where tb_Tipo_Documento_id = " + objeto.getId().toString();
            dml = dml + ";";

            PreparedStatement ps = connection.prepareStatement(dml);

            ps.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
    }
}

