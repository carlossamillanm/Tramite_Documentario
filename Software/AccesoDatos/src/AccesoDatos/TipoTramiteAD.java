package AccesoDatos;

import Entidades.UnidadOrganizativa;
import Entidades.TipoTramite;
import Entidades.UnidadTramite;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class TipoTramiteAD {

    private Connection connection;

    public TipoTramiteAD(Connection connection) {
        this.connection = connection;
    }

    public List<TipoTramite> ListarTodo() throws Exception {
        try {
            String sql = "";

            sql = sql + " select";
            sql = sql + " tb_Tipo_Tramite.tb_Tipo_Tramite_id";
            sql = sql + ",tb_Tipo_Tramite.tb_Tipo_Tramite_nombre";
            sql = sql + ",tb_Tipo_Tramite.tb_Tipo_Tramite_tiempoestimado";
            sql = sql + ",tb_Tipo_Tramite.tb_Tipo_Tramite_estado";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_id";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_nombre";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_abreviatura";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_responsable";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_estado";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_id";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_nombre";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_abreviatura";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_estado";
            sql = sql + " from";
            sql = sql + " tb_Tipo_Tramite";
            sql = sql + " inner join tb_Unidad_Tramite on tb_Tipo_Tramite.tb_Unidad_Tramite_id = tb_Unidad_Tramite.tb_Unidad_Tramite_id";
            sql = sql + " inner join tb_Unidad_Organizativa on tb_Unidad_Tramite.tb_Unidad_Organizativa_id = tb_Unidad_Organizativa.tb_Unidad_Organizativa_id";
            sql = sql + ";";

            List<TipoTramite> lista = new ArrayList<TipoTramite>();
            TipoTramite objeto;

            PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = ps.executeQuery();

            rs.beforeFirst();

            while (rs.next()) {
                objeto = new TipoTramite();

                objeto.setId(rs.getInt("tb_Tipo_Tramite_id"));
                objeto.setNombre(rs.getString("tb_Tipo_Tramite_nombre"));
                objeto.setTiempoEstimado(rs.getInt("tb_Tipo_Tramite_tiempoestimado"));
                objeto.setEstado(rs.getString("tb_Tipo_Tramite_estado"));
                objeto.setUnidadTramite(new UnidadTramite(rs.getInt("tb_Unidad_Tramite_id"), rs.getString("tb_Unidad_Tramite_nombre"), rs.getString("tb_Unidad_Tramite_abreviatura"), rs.getString("tb_Unidad_Tramite_responsable"), rs.getString("tb_Unidad_Tramite_estado"), new UnidadOrganizativa(rs.getInt("tb_Unidad_Organizativa_id"), rs.getString("tb_Unidad_Organizativa_nombre"), rs.getString("tb_Unidad_Organizativa_abreviatura"), rs.getString("tb_Unidad_Organizativa_estado"))));

                lista.add(objeto);
            }

            return lista;
        } catch (Exception e) {
            throw e;
        }
    }

    public List<TipoTramite> TipoTramite(Integer id) throws Exception {
        try {
            String sql = "";

            sql = sql + " select";
            sql = sql + " tb_Tipo_Tramite.tb_Tipo_Tramite_id";
            sql = sql + ",tb_Tipo_Tramite.tb_Tipo_Tramite_nombre";
            sql = sql + ",tb_Tipo_Tramite.tb_Tipo_Tramite_tiempoestimado";
            sql = sql + ",tb_Tipo_Tramite.tb_Tipo_Tramite_estado";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_id";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_nombre";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_abreviatura";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_responsable";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_estado";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_id";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_nombre";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_abreviatura";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_estado";
            sql = sql + " from";
            sql = sql + " tb_Tipo_Tramite";
            sql = sql + " inner join tb_Unidad_Tramite on tb_Tipo_Tramite.tb_Unidad_Tramite_id = tb_Unidad_Tramite.tb_Unidad_Tramite_id";
            sql = sql + " inner join tb_Unidad_Organizativa on tb_Unidad_Tramite.tb_Unidad_Organizativa_id = tb_Unidad_Organizativa.tb_Unidad_Organizativa_id";
            sql = sql + " where";
            sql = sql + " tb_Unidad_Tramite.tb_Unidad_Tramite_id =" + id.toString();
            sql = sql + ";";

            List<TipoTramite> lista = new ArrayList<TipoTramite>();
            TipoTramite objeto;

            PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = ps.executeQuery();

            rs.beforeFirst();

            while (rs.next()) {
                objeto = new TipoTramite();

                objeto.setId(rs.getInt("tb_Tipo_Tramite_id"));
                objeto.setNombre(rs.getString("tb_Tipo_Tramite_nombre"));
                objeto.setTiempoEstimado(rs.getInt("tb_Tipo_Tramite_tiempoestimado"));
                objeto.setEstado(rs.getString("tb_Tipo_Tramite_estado"));
                objeto.setUnidadTramite(new UnidadTramite(rs.getInt("tb_Unidad_Tramite_id"), rs.getString("tb_Unidad_Tramite_nombre"), rs.getString("tb_Unidad_Tramite_abreviatura"), rs.getString("tb_Unidad_Tramite_responsable"), rs.getString("tb_Unidad_Tramite_estado"), new UnidadOrganizativa(rs.getInt("tb_Unidad_Organizativa_id"), rs.getString("tb_Unidad_Organizativa_nombre"), rs.getString("tb_Unidad_Organizativa_abreviatura"), rs.getString("tb_Unidad_Organizativa_estado"))));

                lista.add(objeto);
            }

            return lista;
        } catch (Exception e) {
            throw e;
        }
    }

      public TipoTramite Ver(Integer id) throws Exception {
        try {
            String sql = "";

            sql = sql + " select";
            sql = sql + " tb_Tipo_Tramite.tb_Tipo_Tramite_id";
            sql = sql + ",tb_Tipo_Tramite.tb_Tipo_Tramite_nombre";
            sql = sql + ",tb_Tipo_Tramite.tb_Tipo_Tramite_tiempoestimado";
            sql = sql + ",tb_Tipo_Tramite.tb_Tipo_Tramite_estado";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_id";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_nombre";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_abreviatura";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_responsable";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_estado";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_id";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_nombre";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_abreviatura";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_estado";
            sql = sql + " from";
            sql = sql + " tb_Tipo_Tramite";
            sql = sql + " inner join tb_Unidad_Tramite on tb_Tipo_Tramite.tb_Unidad_Tramite_id = tb_Unidad_Tramite.tb_Unidad_Tramite_id";
            sql = sql + " inner join tb_Unidad_Organizativa on tb_Unidad_Tramite.tb_Unidad_Organizativa_id = tb_Unidad_Organizativa.tb_Unidad_Organizativa_id";
            sql = sql + " where";
            sql = sql + " tb_Tipo_Tramite.tb_Tipo_Tramite_id =" + id.toString();
            sql = sql + ";";

            TipoTramite objeto = null;

            PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = ps.executeQuery();

            rs.beforeFirst();

            if (rs.next()) {
                objeto = new TipoTramite();

                objeto.setId(rs.getInt("tb_Tipo_Tramite_id"));
                objeto.setNombre(rs.getString("tb_Tipo_Tramite_nombre"));
                objeto.setTiempoEstimado(rs.getInt("tb_Tipo_Tramite_tiempoestimado"));
                objeto.setEstado(rs.getString("tb_Tipo_Tramite_estado"));
                objeto.setUnidadTramite(new UnidadTramite(rs.getInt("tb_Unidad_Tramite_id"), rs.getString("tb_Unidad_Tramite_nombre"), rs.getString("tb_Unidad_Tramite_abreviatura"), rs.getString("tb_Unidad_Tramite_responsable"), rs.getString("tb_Unidad_Tramite_estado"), new UnidadOrganizativa(rs.getInt("tb_Unidad_Organizativa_id"), rs.getString("tb_Unidad_Organizativa_nombre"), rs.getString("tb_Unidad_Organizativa_abreviatura"), rs.getString("tb_Unidad_Organizativa_estado"))));

            }
            return objeto;
        } catch (Exception e) {
            throw e;
        }
    }
    
    public TipoTramite Vert(Integer id) throws Exception {
        try {
            String sql = "";

            sql = sql + " select";
            sql = sql + " tb_Tipo_Tramite.tb_Tipo_Tramite_id";
            sql = sql + ",tb_Tipo_Tramite.tb_Tipo_Tramite_nombre";
            sql = sql + ",tb_Tipo_Tramite.tb_Tipo_Tramite_tiempoestimado";
            sql = sql + ",tb_Tipo_Tramite.tb_Tipo_Tramite_estado";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_id";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_nombre";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_abreviatura";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_responsable";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_estado";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_id";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_nombre";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_abreviatura";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_estado";
            sql = sql + " from";
            sql = sql + " tb_Tipo_Tramite";
            sql = sql + " inner join tb_Unidad_Tramite on tb_Tipo_Tramite.tb_Unidad_Tramite_id = tb_Unidad_Tramite.tb_Unidad_Tramite_id";
            sql = sql + " inner join tb_Unidad_Organizativa on tb_Unidad_Tramite.tb_Unidad_Organizativa_id = tb_Unidad_Organizativa.tb_Unidad_Organizativa_id";
            sql = sql + " where";
            sql = sql + " tb_Unidad_Tramite.tb_Unidad_Tramite_id =" + id.toString();
            sql = sql + ";";

            TipoTramite objeto = null;

            PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = ps.executeQuery();

            rs.beforeFirst();

            if (rs.next()) {
                objeto = new TipoTramite();

                objeto.setId(rs.getInt("tb_Tipo_Tramite_id"));
                objeto.setNombre(rs.getString("tb_Tipo_Tramite_nombre"));
                objeto.setTiempoEstimado(rs.getInt("tb_Tipo_Tramite_tiempoestimado"));
                objeto.setEstado(rs.getString("tb_Tipo_Tramite_estado"));
                objeto.setUnidadTramite(new UnidadTramite(rs.getInt("tb_Unidad_Tramite_id"), rs.getString("tb_Unidad_Tramite_nombre"), rs.getString("tb_Unidad_Tramite_abreviatura"), rs.getString("tb_Unidad_Tramite_responsable"), rs.getString("tb_Unidad_Tramite_estado"), new UnidadOrganizativa(rs.getInt("tb_Unidad_Organizativa_id"), rs.getString("tb_Unidad_Organizativa_nombre"), rs.getString("tb_Unidad_Organizativa_abreviatura"), rs.getString("tb_Unidad_Organizativa_estado"))));

            }
            return objeto;
        } catch (Exception e) {
            throw e;
        }
    }

    public void Insertar(TipoTramite objeto) throws Exception {
        try {
            String dml = "";

            dml = dml + " insert into tb_Tipo_Tramite(";
            dml = dml + " tb_Tipo_Tramite_nombre";
            dml = dml + ",tb_Tipo_Tramite_tiempoestimado";
            dml = dml + ",tb_Tipo_Tramite_estado";
            dml = dml + ",tb_Unidad_Tramite_id";
            dml = dml + ") values (";
            dml = dml + " '" + objeto.getNombre() + "'";
            dml = dml + ",'" + objeto.getTiempoEstimado() + "'";
            dml = dml + ",'" + objeto.getEstado() + "'";
            dml = dml + ",'" + objeto.getUnidadTramite().getId().toString() + "'";
            dml = dml + " )";
            dml = dml + ";";

            PreparedStatement ps = connection.prepareStatement(dml);

            ps.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
    }

    public void Actualizar(TipoTramite objeto) throws Exception {
        try {
            String dml = "";

            dml = dml + " update tb_Tipo_Tramite set";
            dml = dml + " tb_Tipo_Tramite_nombre = '" + objeto.getNombre() + "'";
            dml = dml + ",tb_Tipo_Tramite_tiempoestimado = '" + objeto.getTiempoEstimado() + "'";
            dml = dml + ",tb_Tipo_Tramite_estado = '" + objeto.getEstado() + "'";
            dml = dml + ",tb_Unidad_Tramite_id = '" + objeto.getUnidadTramite().getId().toString() + "'";
            dml = dml + " where tb_Tipo_Tramite_id = " + objeto.getId().toString();
            dml = dml + ";";

            PreparedStatement ps = connection.prepareStatement(dml);

            ps.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
    }

    public void Eliminar(TipoTramite objeto) throws Exception {
        try {
            String dml = "";

            dml = dml + " delete from tb_Tipo_Tramite";
            dml = dml + " where tb_Tipo_Tramite_id = " + objeto.getId().toString();
            dml = dml + ";";

            PreparedStatement ps = connection.prepareStatement(dml);

            ps.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
    }
}
