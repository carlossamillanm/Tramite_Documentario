package AccesoDatos;

import Entidades.DocumentoEmitido;
import Entidades.TipoDocumento;
import Entidades.Tramite;
import Entidades.UnidadOrganizativa;
import Entidades.UnidadTramite;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TramiteAD {

    private Connection connection;

    public TramiteAD(Connection connection) {
        this.connection = connection;
    }

    public List<Tramite> ListarTodo() throws Exception {
        try {
            String sql = "";

            sql = sql + " select";
            sql = sql + " tb_Tramite.tb_Tramite_id";
            sql = sql + ",tb_Tramite.tb_Tramite_descripcion";
            sql = sql + ",tb_Tramite.tb_Tramite_tiptraslado";
            sql = sql + ",tb_Tramite.tb_Tramite_tipentidad";
            sql = sql + ",tb_Tramite.tb_Tramite_descripentidad";
            sql = sql + ",tb_Tramite.tb_Tramite_fecha_emision";
            sql = sql + ",tb_Ruta.tb_Ruta_id";
            sql = sql + ",tb_Ruta.tb_Ruta_fechhora";
            sql = sql + ",tb_Ruta.tb_Ruta_tipoexpediente";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_id";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_año";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_numero";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_fechemision";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_para";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_asunto";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_numexpediente";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_fechrecepcion";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_tipo";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_id";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_nombre";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_estado";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_id";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_nombre";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_abreviatura";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_responsable";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_estado";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_id";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_nombre";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_abreviatura";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_estado";
            sql = sql + " from";
            sql = sql + " tb_Tramite";
            sql = sql + " inner join tb_Ruta on tb_Tramite.tb_Ruta_id = tb_Ruta.tb_Ruta_id";
            sql = sql + " inner join tb_Documento_Emitido on tb_Tramite.tb_Documento_Emitido_id = tb_Documento_Emitido.tb_Documento_Emitido_id";
            sql = sql + " inner join tb_Unidad_Tramite on tb_Documento_Emitido.tb_Unidad_Tramite_id = tb_Unidad_Tramite.tb_Unidad_Tramite_id";
            sql = sql + " inner join tb_Tipo_Documento on tb_Documento_Emitido.tb_Tipo_Documento_id = tb_Tipo_Documento.tb_Tipo_Documento_id";
            sql = sql + " inner join tb_Unidad_Organizativa on tb_Unidad_Tramite.tb_Unidad_Organizativa_id = tb_Unidad_Organizativa.tb_Unidad_Organizativa_id";
            sql = sql + ";";

            List<Tramite> lista = new ArrayList<Tramite>();
            Tramite objeto;

            PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = ps.executeQuery();

            rs.beforeFirst();

            while (rs.next()) {
                objeto = new Tramite();

                objeto.setId(rs.getInt("tb_Tramite_id"));
                objeto.setDescripcion(rs.getString("tb_Tramite_descripcion"));
                objeto.setTipoTraslado(rs.getString("tb_Tramite_tiptraslado"));
                objeto.setTipoEntidad(rs.getString("tb_Tramite_tipentidad"));
                objeto.setDescripcionEntidad(rs.getString("tb_Tramite_descripentidad"));
                objeto.setFechaemision(rs.getDate("tb_Tramite_fecha_emision"));
                objeto.setDocumentoEmitido(new DocumentoEmitido(rs.getInt("tb_Documento_Emitido_id"), rs.getTimestamp("tb_Documento_Emitido_año"), rs.getString("tb_Documento_Emitido_numero"), rs.getTimestamp("tb_Documento_Emitido_fechemision"), rs.getString("tb_Documento_Emitido_para"), rs.getString("tb_Documento_Emitido_asunto"), rs.getString("tb_Documento_Emitido_numexpediente"), rs.getTimestamp("tb_Documento_Emitido_fechrecepcion"), rs.getString("tb_Documento_Emitido_tipo"), new TipoDocumento(rs.getInt("tb_Tipo_Documento_id"), rs.getString("tb_Tipo_Documento_nombre"), rs.getString("tb_Tipo_Documento_estado")), new UnidadTramite(rs.getInt("tb_Unidad_Tramite_id"), rs.getString("tb_Unidad_Tramite_nombre"), rs.getString("tb_Unidad_Tramite_abreviatura"), rs.getString("tb_Unidad_Tramite_responsable"), rs.getString("tb_Unidad_Tramite_estado"), new UnidadOrganizativa(rs.getInt("tb_Unidad_Organizativa_id"), rs.getString("tb_Unidad_Organizativa_nombre"), rs.getString("tb_Unidad_Organizativa_abreviatura"), rs.getString("tb_Unidad_Organizativa_estado ")))));

                lista.add(objeto);
            }

            return lista;
        } catch (Exception e) {
            throw e;
        }
    }

    public List<Tramite> ListarFecha(String inicio, String fin) throws Exception {
        try {
            String sql = "";

            sql = sql + " select";
            sql = sql + " tb_Tramite.tb_Tramite_id";
            sql = sql + ",tb_Tramite.tb_Tramite_descripcion";
            sql = sql + ",tb_Tramite.tb_Tramite_tiptraslado";
            sql = sql + ",tb_Tramite.tb_Tramite_tipentidad";
            sql = sql + ",tb_Tramite.tb_Tramite_descripentidad";
            sql = sql + ",tb_Tramite.tb_Tramite_fecha_emision";
            sql = sql + ",tb_Ruta.tb_Ruta_id";
            sql = sql + ",tb_Ruta.tb_Ruta_fechhora";
            sql = sql + ",tb_Ruta.tb_Ruta_tipoexpediente";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_id";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_año";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_numero";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_fechemision";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_para";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_asunto";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_numexpediente";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_fechrecepcion";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_tipo";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_id";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_nombre";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_estado";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_id";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_nombre";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_abreviatura";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_responsable";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_estado";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_id";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_nombre";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_abreviatura";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_estado";
            sql = sql + " from";
            sql = sql + " tb_Tramite";
            sql = sql + " inner join tb_Ruta on tb_Tramite.tb_Ruta_id = tb_Ruta.tb_Ruta_id";
            sql = sql + " inner join tb_Documento_Emitido on tb_Tramite.tb_Documento_Emitido_id = tb_Documento_Emitido.tb_Documento_Emitido_id";
            sql = sql + " inner join tb_Unidad_Tramite on tb_Documento_Emitido.tb_Unidad_Tramite_id = tb_Unidad_Tramite.tb_Unidad_Tramite_id";
            sql = sql + " inner join tb_Tipo_Documento on tb_Documento_Emitido.tb_Tipo_Documento_id = tb_Tipo_Documento.tb_Tipo_Documento_id";
            sql = sql + " inner join tb_Unidad_Organizativa on tb_Unidad_Tramite.tb_Unidad_Organizativa_id = tb_Unidad_Organizativa.tb_Unidad_Organizativa_id";
            sql = sql + " where";
            sql = sql + " cast(tb_Tramite.tb_Tramite_fecha_emision as date) between '" + inicio + "' and '" + fin + "'";
            sql = sql + ";";

            List<Tramite> lista = new ArrayList<Tramite>();
            Tramite objeto;

            PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = ps.executeQuery();

            rs.beforeFirst();

            while (rs.next()) {
                objeto = new Tramite();

                objeto.setId(rs.getInt("tb_Tramite_id"));
                objeto.setDescripcion(rs.getString("tb_Tramite_descripcion"));
                objeto.setTipoTraslado(rs.getString("tb_Tramite_tiptraslado"));
                objeto.setTipoEntidad(rs.getString("tb_Tramite_tipentidad"));
                objeto.setDescripcionEntidad(rs.getString("tb_Tramite_descripentidad"));
                objeto.setFechaemision(rs.getDate("tb_Tramite_fecha_emision"));
                objeto.setDocumentoEmitido(new DocumentoEmitido(rs.getInt("tb_Documento_Emitido_id"), rs.getTimestamp("tb_Documento_Emitido_año"), rs.getString("tb_Documento_Emitido_numero"), rs.getTimestamp("tb_Documento_Emitido_fechemision"), rs.getString("tb_Documento_Emitido_para"), rs.getString("tb_Documento_Emitido_asunto"), rs.getString("tb_Documento_Emitido_numexpediente"), rs.getTimestamp("tb_Documento_Emitido_fechrecepcion"), rs.getString("tb_Documento_Emitido_tipo"), new TipoDocumento(rs.getInt("tb_Tipo_Documento_id"), rs.getString("tb_Tipo_Documento_nombre"), rs.getString("tb_Tipo_Documento_estado")), new UnidadTramite(rs.getInt("tb_Unidad_Tramite_id"), rs.getString("tb_Unidad_Tramite_nombre"), rs.getString("tb_Unidad_Tramite_abreviatura"), rs.getString("tb_Unidad_Tramite_responsable"), rs.getString("tb_Unidad_Tramite_estado"), new UnidadOrganizativa(rs.getInt("tb_Unidad_Organizativa_id"), rs.getString("tb_Unidad_Organizativa_nombre"), rs.getString("tb_Unidad_Organizativa_abreviatura"), rs.getString("tb_Unidad_Organizativa_estado")))));

                lista.add(objeto);
            }
            return lista;
        } catch (Exception e) {
            throw e;
        }
    }

    public List<Tramite> ListarFechaTipo(String inicio, String fin, String Tipo) throws Exception {
        try {
            String sql = "";

            sql = sql + " select";
            sql = sql + " tb_Tramite.tb_Tramite_id";
            sql = sql + ",tb_Tramite.tb_Tramite_descripcion";
            sql = sql + ",tb_Tramite.tb_Tramite_tiptraslado";
            sql = sql + ",tb_Tramite.tb_Tramite_tipentidad";
            sql = sql + ",tb_Tramite.tb_Tramite_descripentidad";
            sql = sql + ",tb_Tramite.tb_Tramite_fecha_emision";
            sql = sql + ",tb_Ruta.tb_Ruta_id";
            sql = sql + ",tb_Ruta.tb_Ruta_fechhora";
            sql = sql + ",tb_Ruta.tb_Ruta_tipoexpediente";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_id";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_año";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_numero";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_fechemision";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_para";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_asunto";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_numexpediente";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_fechrecepcion";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_tipo";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_id";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_nombre";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_estado";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_id";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_nombre";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_abreviatura";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_responsable";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_estado";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_id";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_nombre";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_abreviatura";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_estado";
            sql = sql + " from";
            sql = sql + " tb_Tramite";
            sql = sql + " inner join tb_Ruta on tb_Tramite.tb_Ruta_id = tb_Ruta.tb_Ruta_id";
            sql = sql + " inner join tb_Documento_Emitido on tb_Tramite.tb_Documento_Emitido_id = tb_Documento_Emitido.tb_Documento_Emitido_id";
            sql = sql + " inner join tb_Unidad_Tramite on tb_Documento_Emitido.tb_Unidad_Tramite_id = tb_Unidad_Tramite.tb_Unidad_Tramite_id";
            sql = sql + " inner join tb_Tipo_Documento on tb_Documento_Emitido.tb_Tipo_Documento_id = tb_Tipo_Documento.tb_Tipo_Documento_id";
            sql = sql + " inner join tb_Unidad_Organizativa on tb_Unidad_Tramite.tb_Unidad_Organizativa_id = tb_Unidad_Organizativa.tb_Unidad_Organizativa_id";
            sql = sql + " where";
            sql = sql + " tb_Documento_Emitido.tb_Documento_Emitido_tipo like '" + Tipo + "%'";
            sql = sql + " and cast(tb_Tramite.tb_Tramite_fecha_emision as date) between '" + inicio + "' and '" + fin + "'";
            sql = sql + ";";

            List<Tramite> lista = new ArrayList<Tramite>();
            Tramite objeto;

            PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = ps.executeQuery();

            rs.beforeFirst();

            while (rs.next()) {
                objeto = new Tramite();

                objeto.setId(rs.getInt("tb_Tramite_id"));
                objeto.setDescripcion(rs.getString("tb_Tramite_descripcion"));
                objeto.setTipoTraslado(rs.getString("tb_Tramite_tiptraslado"));
                objeto.setTipoEntidad(rs.getString("tb_Tramite_tipentidad"));
                objeto.setDescripcionEntidad(rs.getString("tb_Tramite_descripentidad"));
                objeto.setFechaemision(rs.getDate("tb_Tramite_fecha_emision"));
                objeto.setDocumentoEmitido(new DocumentoEmitido(rs.getInt("tb_Documento_Emitido_id"), rs.getTimestamp("tb_Documento_Emitido_año"), rs.getString("tb_Documento_Emitido_numero"), rs.getTimestamp("tb_Documento_Emitido_fechemision"), rs.getString("tb_Documento_Emitido_para"), rs.getString("tb_Documento_Emitido_asunto"), rs.getString("tb_Documento_Emitido_numexpediente"), rs.getTimestamp("tb_Documento_Emitido_fechrecepcion"), rs.getString("tb_Documento_Emitido_tipo"), new TipoDocumento(rs.getInt("tb_Tipo_Documento_id"), rs.getString("tb_Tipo_Documento_nombre"), rs.getString("tb_Tipo_Documento_estado")), new UnidadTramite(rs.getInt("tb_Unidad_Tramite_id"), rs.getString("tb_Unidad_Tramite_nombre"), rs.getString("tb_Unidad_Tramite_abreviatura"), rs.getString("tb_Unidad_Tramite_responsable"), rs.getString("tb_Unidad_Tramite_estado"), new UnidadOrganizativa(rs.getInt("tb_Unidad_Organizativa_id"), rs.getString("tb_Unidad_Organizativa_nombre"), rs.getString("tb_Unidad_Organizativa_abreviatura"), rs.getString("tb_Unidad_Organizativa_estado")))));

                lista.add(objeto);
            }
            return lista;
        } catch (Exception e) {
            throw e;
        }
    }

    public List<Tramite> ListarTipo(String inicio, String fin, String Tipo) throws Exception {
        try {
            String sql = "";

            sql = sql + " select";
            sql = sql + " tb_Tramite.tb_Tramite_id";
            sql = sql + ",tb_Tramite.tb_Tramite_descripcion";
            sql = sql + ",tb_Tramite.tb_Tramite_tiptraslado";
            sql = sql + ",tb_Tramite.tb_Tramite_tipentidad";
            sql = sql + ",tb_Tramite.tb_Tramite_descripentidad";
            sql = sql + ",tb_Tramite.tb_Tramite_fecha_emision";
            sql = sql + ",tb_Ruta.tb_Ruta_id";
            sql = sql + ",tb_Ruta.tb_Ruta_fechhora";
            sql = sql + ",tb_Ruta.tb_Ruta_tipoexpediente";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_id";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_año";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_numero";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_fechemision";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_para";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_asunto";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_numexpediente";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_fechrecepcion";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_tipo";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_id";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_nombre";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_estado";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_id";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_nombre";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_abreviatura";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_responsable";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_estado";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_id";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_nombre";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_abreviatura";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_estado";
            sql = sql + " from";
            sql = sql + " tb_Tramite";
            sql = sql + " inner join tb_Ruta on tb_Tramite.tb_Ruta_id = tb_Ruta.tb_Ruta_id";
            sql = sql + " inner join tb_Documento_Emitido on tb_Tramite.tb_Documento_Emitido_id = tb_Documento_Emitido.tb_Documento_Emitido_id";
            sql = sql + " inner join tb_Unidad_Tramite on tb_Documento_Emitido.tb_Unidad_Tramite_id = tb_Unidad_Tramite.tb_Unidad_Tramite_id";
            sql = sql + " inner join tb_Tipo_Documento on tb_Documento_Emitido.tb_Tipo_Documento_id = tb_Tipo_Documento.tb_Tipo_Documento_id";
            sql = sql + " inner join tb_Unidad_Organizativa on tb_Unidad_Tramite.tb_Unidad_Organizativa_id = tb_Unidad_Organizativa.tb_Unidad_Organizativa_id";
            sql = sql + " where";
            sql = sql + " tb_Tramite.tb_Tramite_descripentidad like '" + Tipo + "%'";
            sql = sql + " and cast(tb_Tramite.tb_Tramite_fecha_emision as date) between '" + inicio + "' and '" + fin + "'";
            sql = sql + ";";

            List<Tramite> lista = new ArrayList<Tramite>();
            Tramite objeto;

            PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = ps.executeQuery();

            rs.beforeFirst();

            while (rs.next()) {
                objeto = new Tramite();

                objeto.setId(rs.getInt("tb_Tramite_id"));
                objeto.setDescripcion(rs.getString("tb_Tramite_descripcion"));
                objeto.setTipoTraslado(rs.getString("tb_Tramite_tiptraslado"));
                objeto.setTipoEntidad(rs.getString("tb_Tramite_tipentidad"));
                objeto.setDescripcionEntidad(rs.getString("tb_Tramite_descripentidad"));
                objeto.setFechaemision(rs.getDate("tb_Tramite_fecha_emision"));
                objeto.setDocumentoEmitido(new DocumentoEmitido(rs.getInt("tb_Documento_Emitido_id"), rs.getTimestamp("tb_Documento_Emitido_año"), rs.getString("tb_Documento_Emitido_numero"), rs.getTimestamp("tb_Documento_Emitido_fechemision"), rs.getString("tb_Documento_Emitido_para"), rs.getString("tb_Documento_Emitido_asunto"), rs.getString("tb_Documento_Emitido_numexpediente"), rs.getTimestamp("tb_Documento_Emitido_fechrecepcion"), rs.getString("tb_Documento_Emitido_tipo"), new TipoDocumento(rs.getInt("tb_Tipo_Documento_id"), rs.getString("tb_Tipo_Documento_nombre"), rs.getString("tb_Tipo_Documento_estado")), new UnidadTramite(rs.getInt("tb_Unidad_Tramite_id"), rs.getString("tb_Unidad_Tramite_nombre"), rs.getString("tb_Unidad_Tramite_abreviatura"), rs.getString("tb_Unidad_Tramite_responsable"), rs.getString("tb_Unidad_Tramite_estado"), new UnidadOrganizativa(rs.getInt("tb_Unidad_Organizativa_id"), rs.getString("tb_Unidad_Organizativa_nombre"), rs.getString("tb_Unidad_Organizativa_abreviatura"), rs.getString("tb_Unidad_Organizativa_estado ")))));

                lista.add(objeto);
            }

            return lista;
        } catch (Exception e) {
            throw e;
        }
    }

    public List<Tramite> ListarTramite(String inicio, String fin, String id) throws Exception {
        try {
            String sql = "";

            sql = sql + " select";
            sql = sql + " tb_Tramite.tb_Tramite_id";
            sql = sql + ",tb_Tramite.tb_Tramite_descripcion";
            sql = sql + ",tb_Tramite.tb_Tramite_tiptraslado";
            sql = sql + ",tb_Tramite.tb_Tramite_tipentidad";
            sql = sql + ",tb_Tramite.tb_Tramite_descripentidad";
            sql = sql + ",tb_Tramite.tb_Tramite_fecha_emision";
            sql = sql + ",tb_Ruta.tb_Ruta_id";
            sql = sql + ",tb_Ruta.tb_Ruta_fechhora";
            sql = sql + ",tb_Ruta.tb_Ruta_tipoexpediente";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_id";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_año";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_numero";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_fechemision";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_para";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_asunto";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_numexpediente";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_fechrecepcion";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_tipo";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_id";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_nombre";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_estado";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_id";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_nombre";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_abreviatura";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_responsable";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_estado";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_id";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_nombre";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_abreviatura";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_estado";
            sql = sql + " from";
            sql = sql + " tb_Tramite";
            sql = sql + " inner join tb_Ruta on tb_Tramite.tb_Ruta_id = tb_Ruta.tb_Ruta_id";
            sql = sql + " inner join tb_Documento_Emitido on tb_Tramite.tb_Documento_Emitido_id = tb_Documento_Emitido.tb_Documento_Emitido_id";
            sql = sql + " inner join tb_Unidad_Tramite on tb_Documento_Emitido.tb_Unidad_Tramite_id = tb_Unidad_Tramite.tb_Unidad_Tramite_id";
            sql = sql + " inner join tb_Tipo_Documento on tb_Documento_Emitido.tb_Tipo_Documento_id = tb_Tipo_Documento.tb_Tipo_Documento_id";
            sql = sql + " inner join tb_Unidad_Organizativa on tb_Unidad_Tramite.tb_Unidad_Organizativa_id = tb_Unidad_Organizativa.tb_Unidad_Organizativa_id";
            sql = sql + " where";
            sql = sql + " tb_Unidad_Tramite.tb_Unidad_Tramite_id =" + id;
            sql = sql + " and cast(tb_Tramite.tb_Tramite_fecha_emision as date) between '" + inicio + "' and '" + fin + "'";
            sql = sql + ";";

            List<Tramite> lista = new ArrayList<Tramite>();
            Tramite objeto;

            PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = ps.executeQuery();

            rs.beforeFirst();

            while (rs.next()) {
                objeto = new Tramite();

                objeto.setId(rs.getInt("tb_Tramite_id"));
                objeto.setDescripcion(rs.getString("tb_Tramite_descripcion"));
                objeto.setTipoTraslado(rs.getString("tb_Tramite_tiptraslado"));
                objeto.setTipoEntidad(rs.getString("tb_Tramite_tipentidad"));
                objeto.setDescripcionEntidad(rs.getString("tb_Tramite_descripentidad"));
                objeto.setFechaemision(rs.getDate("tb_Tramite_fecha_emision"));
                objeto.setDocumentoEmitido(new DocumentoEmitido(rs.getInt("tb_Documento_Emitido_id"), rs.getTimestamp("tb_Documento_Emitido_año"), rs.getString("tb_Documento_Emitido_numero"), rs.getTimestamp("tb_Documento_Emitido_fechemision"), rs.getString("tb_Documento_Emitido_para"), rs.getString("tb_Documento_Emitido_asunto"), rs.getString("tb_Documento_Emitido_numexpediente"), rs.getTimestamp("tb_Documento_Emitido_fechrecepcion"), rs.getString("tb_Documento_Emitido_tipo"), new TipoDocumento(rs.getInt("tb_Tipo_Documento_id"), rs.getString("tb_Tipo_Documento_nombre"), rs.getString("tb_Tipo_Documento_estado")), new UnidadTramite(rs.getInt("tb_Unidad_Tramite_id"), rs.getString("tb_Unidad_Tramite_nombre"), rs.getString("tb_Unidad_Tramite_abreviatura"), rs.getString("tb_Unidad_Tramite_responsable"), rs.getString("tb_Unidad_Tramite_estado"), new UnidadOrganizativa(rs.getInt("tb_Unidad_Organizativa_id"), rs.getString("tb_Unidad_Organizativa_nombre"), rs.getString("tb_Unidad_Organizativa_abreviatura"), rs.getString("tb_Unidad_Organizativa_estado")))));

                lista.add(objeto);
            }

            return lista;
        } catch (Exception e) {
            throw e;
        }
    }

    public Tramite Ver(Integer id) throws Exception {
        try {
            String sql = "";

            sql = sql + " select";
            sql = sql + " tb_Tramite.tb_Tramite_id";
            sql = sql + ",tb_Tramite.tb_Tramite_descripcion";
            sql = sql + ",tb_Tramite.tb_Tramite_tiptraslado";
            sql = sql + ",tb_Tramite.tb_Tramite_tipentidad";
            sql = sql + ",tb_Tramite.tb_Tramite_descripentidad";
            sql = sql + ",tb_Tramite.tb_Tramite_fecha_emision";
            sql = sql + ",tb_Ruta.tb_Ruta_id";
            sql = sql + ",tb_Ruta.tb_Ruta_fechhora";
            sql = sql + ",tb_Ruta.tb_Ruta_tipoexpediente";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_id";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_año";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_numero";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_fechemision";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_para";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_asunto";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_numexpediente";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_fechrecepcion";
            sql = sql + ",tb_Documento_Emitido.tb_Documento_Emitido_tipo";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_id";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_nombre";
            sql = sql + ",tb_Tipo_Documento.tb_Tipo_Documento_estado";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_id";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_nombre";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_abreviatura";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_responsable";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_estado";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_estado";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_id";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_nombre";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_abreviatura";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_estado";
            sql = sql + " from";
            sql = sql + " tb_Tramite";
            sql = sql + " inner join tb_Ruta on tb_Tramite.tb_Ruta_id = tb_Ruta.tb_Ruta_id";
            sql = sql + " inner join tb_Documento_Emitido on tb_Tramite.tb_Documento_Emitido_id = tb_Documento_Emitido.tb_Documento_Emitido_id";
            sql = sql + " inner join tb_Unidad_Tramite on tb_Documento_Emitido.tb_Unidad_Tramite_id = tb_Unidad_Tramite.tb_Unidad_Tramite_id";
            sql = sql + " inner join tb_Tipo_Documento on tb_Documento_Emitido.tb_Tipo_Documento_id = tb_Tipo_Documento.tb_Tipo_Documento_id";
            sql = sql + " inner join tb_Unidad_Organizativa on tb_Unidad_Tramite.tb_Unidad_Organizativa_id = tb_Unidad_Organizativa.tb_Unidad_Organizativa_id";
            sql = sql + " where";
            sql = sql + " tb_Tramite.tb_Tramite_id =" + id.toString();
            sql = sql + ";";

            Tramite objeto = null;

            PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = ps.executeQuery();

            rs.beforeFirst();

            if (rs.next()) {
                objeto = new Tramite();

                objeto.setId(rs.getInt("tb_Tramite_id"));
                objeto.setDescripcion(rs.getString("tb_Tramite_descripcion"));
                objeto.setTipoTraslado(rs.getString("tb_Tramite_tiptraslado"));
                objeto.setTipoEntidad(rs.getString("tb_Tramite_tipentidad"));
                objeto.setDescripcionEntidad(rs.getString("tb_Tramite_descripentidad"));
                objeto.setFechaemision(rs.getDate("tb_Tramite_fecha_emision"));
                objeto.setDocumentoEmitido(new DocumentoEmitido(rs.getInt("tb_Documento_Emitido_id"), rs.getTimestamp("tb_Documento_Emitido_año"), rs.getString("tb_Documento_Emitido_numero"), rs.getTimestamp("tb_Documento_Emitido_fechemision"), rs.getString("tb_Documento_Emitido_para"), rs.getString("tb_Documento_Emitido_asunto"), rs.getString("tb_Documento_Emitido_numexpediente"), rs.getTimestamp("tb_Documento_Emitido_fechrecepcion"), rs.getString("tb_Documento_Emitido_tipo"), new TipoDocumento(rs.getInt("tb_Tipo_Documento_id"), rs.getString("tb_Tipo_Documento_nombre"), rs.getString("tb_Tipo_Documento_estado")), new UnidadTramite(rs.getInt("tb_Unidad_Tramite_id"), rs.getString("tb_Unidad_Tramite_nombre"), rs.getString("tb_Unidad_Tramite_abreviatura"), rs.getString("tb_Unidad_Tramite_responsable"), rs.getString("tb_Unidad_Tramite_estado"), new UnidadOrganizativa(rs.getInt("tb_Unidad_Organizativa_id"), rs.getString("tb_Unidad_Organizativa_nombre"), rs.getString("tb_Unidad_Organizativa_abreviatura"), rs.getString("tb_Unidad_Organizativa_estado ")))));

            }
            return objeto;
        } catch (Exception e) {
            throw e;
        }
    }

    public void Insertar(Tramite objeto) throws Exception {
        try {
            String dml = "";

            dml = dml + " insert into tb_Tramite(";
            dml = dml + " tb_Tramite_descripcion";
            dml = dml + ",tb_Tramite_tiptraslado";
            dml = dml + ",tb_Tramite_tipentidad";
            dml = dml + ",tb_Tramite_descripentidad";
            dml = dml + ",tb_Tramite_fecha_emision";
            dml = dml + ",tb_Documento_Emitido_id";
            dml = dml + ",tb_Ruta_id";
            dml = dml + ") values (";
            dml = dml + " '" + objeto.getDescripcion() + "'";
            dml = dml + ",'" + objeto.getTipoTraslado() + "'";
            dml = dml + ",'" + objeto.getTipoEntidad() + "'";
            dml = dml + ",'" + objeto.getDescripcionEntidad() + "'";
            dml = dml + ",'" + objeto.getFechaemision() + "'";
            dml = dml + ",'" + objeto.getDocumentoEmitido().getId().toString() + "'";
            dml = dml + ",'" + objeto.getRuta().getId().toString() + "'";
            dml = dml + " )";
            dml = dml + ";";

            PreparedStatement ps = connection.prepareStatement(dml);

            ps.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
    }

    public void Actualizar(Tramite objeto) throws Exception {
        try {
            String dml = "";

            dml = dml + " update tb_Tramite set";
            dml = dml + " tb_Tramite_descripcion = '" + objeto.getDescripcion() + "'";
            dml = dml + ",tb_Tramite_tiptraslado = '" + objeto.getTipoTraslado() + "'";
            dml = dml + ",tb_Tramite_tipentidad = '" + objeto.getTipoEntidad() + "'";
            dml = dml + ",tb_Tramite_descripentidad = '" + objeto.getDescripcionEntidad() + "'";
            dml = dml + ",tb_Tramite_fecha_emision = '" + objeto.getFechaemision() + "'";
            dml = dml + ",tb_Documento_Emitido_id = '" + objeto.getDocumentoEmitido().getId().toString() + "'";
            dml = dml + ",tb_Ruta_id='" + objeto.getRuta().getId().toString() + "'";
            dml = dml + " where tb_Tramite_id = " + objeto.getId().toString();
            dml = dml + ";";

            PreparedStatement ps = connection.prepareStatement(dml);

            ps.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
    }

    public void Eliminar(Tramite objeto) throws Exception {
        try {
            String dml = "";

            dml = dml + " delete from tb_Tramite";
            dml = dml + " where tb_Tramite_id = " + objeto.getId().toString();
            dml = dml + ";";

            PreparedStatement ps = connection.prepareStatement(dml);

            ps.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
    }
}
