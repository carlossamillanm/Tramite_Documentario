package AccesoDatos;

import Entidades.UnidadOrganizativa;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class UnidadOrganizativaAD {
    private Connection connection;

    public UnidadOrganizativaAD(Connection connection) {
        this.connection = connection;
    }

        public List<UnidadOrganizativa> ListaTotal() throws Exception {
        try {
            String sql = "";

            sql = sql + " select";
            sql = sql + " tb_Unidad_Organizativa_id";
            sql = sql + ",tb_Unidad_Organizativa_nombre";
            sql = sql + ",tb_Unidad_Organizativa_abreviatura";
            sql = sql + ",tb_Unidad_Organizativa_estado";
            sql = sql + " from tb_Unidad_Organizativa";
            sql = sql + ";";

            List<UnidadOrganizativa> lista = new ArrayList<UnidadOrganizativa>();
            UnidadOrganizativa objeto;

            PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = ps.executeQuery();

            rs.beforeFirst();

            while (rs.next()) {
                objeto = new UnidadOrganizativa();

                objeto.setId(rs.getInt("tb_Unidad_Organizativa_id"));
                objeto.setNombre(rs.getString("tb_Unidad_Organizativa_nombre"));
                objeto.setAbreviatura(rs.getString("tb_Unidad_Organizativa_abreviatura"));
                objeto.setEstado(rs.getString("tb_Unidad_Organizativa_estado"));

                lista.add(objeto);
            }

            return lista;
        } catch (Exception e) {
            throw e;
        }
    }
    
    public List<UnidadOrganizativa> ListarNombre(String nombre) throws Exception {
        try {
            String sql = "";

            sql = sql + " select";
            sql = sql + " tb_Unidad_Organizativa_id";
            sql = sql + ",tb_Unidad_Organizativa_nombre";
            sql = sql + ",tb_Unidad_Organizativa_abreviatura";
            sql = sql + ",tb_Unidad_Organizativa_estado";
            sql = sql + " from tb_Unidad_Organizativa";
            sql = sql + " where tb_Unidad_Organizativa_nombre like '%" + nombre + "%'";
            sql = sql + ";";

            List<UnidadOrganizativa> lista = new ArrayList<UnidadOrganizativa>();
            UnidadOrganizativa objeto;

            PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = ps.executeQuery();

            rs.beforeFirst();

            while (rs.next()) {
                objeto = new UnidadOrganizativa();

                objeto.setId(rs.getInt("tb_Unidad_Organizativa_id"));
                objeto.setNombre(rs.getString("tb_Unidad_Organizativa_nombre"));
                objeto.setAbreviatura(rs.getString("tb_Unidad_Organizativa_abreviatura"));
                objeto.setEstado(rs.getString("tb_Unidad_Organizativa_estado"));

                lista.add(objeto);
            }

            return lista;
        } catch (Exception e) {
            throw e;
        }
    }

    public UnidadOrganizativa Ver(Integer id) throws Exception {
        try {
            String sql = "";

            sql = sql + " select";
            sql = sql + " tb_Unidad_Organizativa_id";
            sql = sql + ",tb_Unidad_Organizativa_nombre";
            sql = sql + ",tb_Unidad_Organizativa_abreviatura";
            sql = sql + ",tb_Unidad_Organizativa_estado";
            sql = sql + " from tb_Unidad_Organizativa";
            sql = sql + " where tb_Unidad_Organizativa_id = " + id.toString();
            sql = sql + ";";

            UnidadOrganizativa objeto = null;

            PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = ps.executeQuery();

            rs.beforeFirst();

            if (rs.next()) {
                objeto = new UnidadOrganizativa();

                objeto.setId(rs.getInt("tb_Unidad_Organizativa_id"));
                objeto.setNombre(rs.getString("tb_Unidad_Organizativa_nombre"));
                objeto.setAbreviatura(rs.getString("tb_Unidad_Organizativa_abreviatura"));
                objeto.setEstado(rs.getString("tb_Unidad_Organizativa_estado"));
            }

            return objeto;
        } catch (Exception e) {
            throw e;
        }
    }

    public void Insertar(UnidadOrganizativa objeto) throws Exception {
        try {
            String dml = "";

            dml = dml + " insert into tb_Unidad_Organizativa(";
            dml = dml + " tb_Unidad_Organizativa_nombre";
            dml = dml + ",tb_Unidad_Organizativa_abreviatura";
            dml = dml + ",tb_Unidad_Organizativa_estado";
            dml = dml + ") values (";
            dml = dml + " '" + objeto.getNombre() + "'";
            dml = dml + ",'" + objeto.getAbreviatura()+ "'";
            dml = dml + ",'" + objeto.getEstado() + "'";
            dml = dml + " )";
            dml = dml + ";";

            PreparedStatement ps = connection.prepareStatement(dml);

            ps.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
    }

    public void Actualizar(UnidadOrganizativa objeto) throws Exception {
        try {
            String dml = "";

            dml = dml + " update tb_Unidad_Organizativa set";
            dml = dml + " tb_Unidad_Organizativa_nombre = '" + objeto.getNombre() + "'";
            dml = dml + ",tb_Unidad_Organizativa_abreviatura = '" + objeto.getAbreviatura() + "'";
            dml = dml + ",tb_Unidad_Organizativa_estado = '" + objeto.getEstado() + "'";
            dml = dml + " where tb_Unidad_Organizativa_id = " + objeto.getId().toString();
            dml = dml + ";";

            PreparedStatement ps = connection.prepareStatement(dml);

            ps.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
    }

    public void Eliminar(UnidadOrganizativa objeto) throws Exception {
        try {
            String dml = "";

            dml = dml + " delete from tb_Unidad_Organizativa";
            dml = dml + " where tb_Unidad_Organizativa_id = " + objeto.getId().toString();
            dml = dml + ";";

            PreparedStatement ps = connection.prepareStatement(dml);

            ps.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
    }
}
