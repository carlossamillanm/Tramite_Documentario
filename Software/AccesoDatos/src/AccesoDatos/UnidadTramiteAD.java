package AccesoDatos;

import Entidades.UnidadOrganizativa;
import Entidades.UnidadTramite;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class UnidadTramiteAD {

    private Connection connection;

    public UnidadTramiteAD(Connection connection) {
        this.connection = connection;
    }
    
        public List<UnidadTramite> ListarTodo() throws Exception {
        try {
            String sql = "";

            sql = sql + " select";
            sql = sql + " tb_Unidad_Tramite.tb_Unidad_Tramite_id";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_nombre";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_abreviatura";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_responsable";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_estado";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_id";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_nombre";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_abreviatura";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_estado";
            sql = sql + " from";
            sql = sql + " tb_Unidad_Tramite";
            sql = sql + " inner join tb_Unidad_Organizativa on tb_Unidad_Tramite.tb_Unidad_Organizativa_id = tb_Unidad_Organizativa.tb_Unidad_Organizativa_id";
            sql = sql + ";";

            List<UnidadTramite> lista = new ArrayList<UnidadTramite>();
            UnidadTramite objeto;

            PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = ps.executeQuery();

            rs.beforeFirst();

            while (rs.next()) {
                objeto = new UnidadTramite();

                objeto.setId(rs.getInt("tb_Unidad_Tramite_id"));
                objeto.setNombre(rs.getString("tb_Unidad_Tramite_nombre"));
                objeto.setAbreviatura(rs.getString("tb_Unidad_Tramite_abreviatura"));
                objeto.setResponsable(rs.getString("tb_Unidad_Tramite_responsable"));
                objeto.setEstado(rs.getString("tb_Unidad_Tramite_estado"));
                objeto.setUnidadorganizativa(new UnidadOrganizativa(rs.getInt("tb_Unidad_Organizativa_id"), rs.getString("tb_Unidad_Organizativa_nombre"), rs.getString("tb_Unidad_Organizativa_abreviatura"), rs.getString("tb_Unidad_Organizativa_estado")));

                lista.add(objeto);
            }

            return lista;
        } catch (Exception e) {
            throw e;
        }
    }

    public List<UnidadTramite> ListarNombre(String nombre) throws Exception {
        try {
            String sql = "";

            sql = sql + " select";
            sql = sql + " tb_Unidad_Tramite.tb_Unidad_Tramite_id";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_nombre";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_abreviatura";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_responsable";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_estado";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_id";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_nombre";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_abreviatura";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_estado";
            sql = sql + " from";
            sql = sql + " tb_Unidad_Tramite";
            sql = sql + " inner join tb_Unidad_Organizativa on tb_Unidad_Tramite.tb_Unidad_Organizativa_id = tb_Unidad_Organizativa.tb_Unidad_Organizativa_id";
            sql = sql + " where";
            sql = sql + " tb_Unidad_Tramite.tb_Unidad_Tramite_nombre like '" + nombre + "%'";
            sql = sql + ";";

            List<UnidadTramite> lista = new ArrayList<UnidadTramite>();
            UnidadTramite objeto;

            PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = ps.executeQuery();

            rs.beforeFirst();

            while (rs.next()) {
                objeto = new UnidadTramite();

                objeto.setId(rs.getInt("tb_Unidad_Tramite_id"));
                objeto.setNombre(rs.getString("tb_Unidad_Tramite_nombre"));
                objeto.setAbreviatura(rs.getString("tb_Unidad_Tramite_abreviatura"));
                objeto.setResponsable(rs.getString("tb_Unidad_Tramite_responsable"));
                objeto.setEstado(rs.getString("tb_Unidad_Tramite_estado"));
                objeto.setUnidadorganizativa(new UnidadOrganizativa(rs.getInt("tb_Unidad_Organizativa_id"), rs.getString("tb_Unidad_Organizativa_nombre"), rs.getString("tb_Unidad_Organizativa_abreviatura"), rs.getString("tb_Unidad_Organizativa_estado")));

                lista.add(objeto);
            }

            return lista;
        } catch (Exception e) {
            throw e;
        }
    }
    

    public UnidadTramite Ver(Integer id) throws Exception {
        try {
            String sql = "";

            sql = sql + " select";
            sql = sql + " tb_Unidad_Tramite.tb_Unidad_Tramite_id";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_nombre";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_abreviatura";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_responsable";
            sql = sql + ",tb_Unidad_Tramite.tb_Unidad_Tramite_estado";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_id";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_nombre";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_abreviatura";
            sql = sql + ",tb_Unidad_Organizativa.tb_Unidad_Organizativa_estado";
            sql = sql + " from";
            sql = sql + " tb_Unidad_Tramite";
            sql = sql + " inner join tb_Unidad_Organizativa on tb_Unidad_Tramite.tb_Unidad_Organizativa_id = tb_Unidad_Organizativa.tb_Unidad_Organizativa_id";
            sql = sql + " where";
            sql = sql + " tb_Unidad_Tramite.tb_Unidad_Tramite_id =" + id.toString();
            sql = sql + ";";

            UnidadTramite objeto = null;

            PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = ps.executeQuery();

            rs.beforeFirst();

            if (rs.next()) {
                objeto = new UnidadTramite();

                objeto.setId(rs.getInt("tb_Unidad_Tramite_id"));
                objeto.setNombre(rs.getString("tb_Unidad_Tramite_nombre"));
                objeto.setAbreviatura(rs.getString("tb_Unidad_Tramite_abreviatura"));
                objeto.setResponsable(rs.getString("tb_Unidad_Tramite_responsable"));
                objeto.setEstado(rs.getString("tb_Unidad_Tramite_estado"));
                objeto.setUnidadorganizativa(new UnidadOrganizativa(rs.getInt("tb_Unidad_Organizativa_id"), rs.getString("tb_Unidad_Organizativa_nombre"), rs.getString("tb_Unidad_Organizativa_abreviatura"), rs.getString("tb_Unidad_Organizativa_estado")));

            }
            return objeto;
        } catch (Exception e) {
            throw e;
        }
    }

    public void Insertar(UnidadTramite objeto) throws Exception {
        try {
            String dml = "";

            dml = dml + " insert into tb_Unidad_Tramite(";
            dml = dml + " tb_Unidad_Tramite_nombre";
            dml = dml + ",tb_Unidad_Tramite_abreviatura";
            dml = dml + ",tb_Unidad_Tramite_responsable";
            dml = dml + ",tb_Unidad_Tramite_estado";
            dml = dml + ",tb_Unidad_Organizativa_id";
            dml = dml + ") values (";
            dml = dml + " '" + objeto.getNombre() + "'";
            dml = dml + ",'" + objeto.getAbreviatura() + "'";
            dml = dml + ",'" + objeto.getResponsable() + "'";
            dml = dml + ",'" + objeto.getEstado() + "'";
            dml = dml + ",'" + objeto.getUnidadorganizativa().getId().toString() + "'";
            dml = dml + " )";
            dml = dml + ";";

            PreparedStatement ps = connection.prepareStatement(dml);

            ps.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
    }

    public void Actualizar(UnidadTramite objeto) throws Exception {
        try {
            String dml = "";

            dml = dml + " update tb_Unidad_Tramite set";
            dml = dml + " tb_Unidad_Tramite_nombre = '" + objeto.getNombre() + "'";
            dml = dml + ",tb_Unidad_Tramite_abreviatura = '" + objeto.getAbreviatura() + "'";
            dml = dml + ",tb_Unidad_Tramite_responsable = '" + objeto.getResponsable() + "'";
            dml = dml + ",tb_Unidad_Tramite_estado = '" + objeto.getEstado() + "'";
            dml = dml + ",tb_Unidad_Organizativa_id = '" + objeto.getUnidadorganizativa().getId().toString() + "'";
            dml = dml + " where tb_Unidad_Tramite_id = " + objeto.getId().toString();
            dml = dml + ";";

            PreparedStatement ps = connection.prepareStatement(dml);

            ps.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
    }

    public void Eliminar(UnidadTramite objeto) throws Exception {
        try {
            String dml = "";

            dml = dml + " delete from tb_Unidad_Tramite";
            dml = dml + " where tb_Unidad_Tramite_id = " + objeto.getId().toString();
            dml = dml + ";";

            PreparedStatement ps = connection.prepareStatement(dml);

            ps.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
    }
}
