package AccesoDatos;

import Entidades.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class UsuarioAD {

    private Connection connection;

    public UsuarioAD(Connection connection) {
        this.connection = connection;
    }

    public List<Usuario> ListarNombre(String nombre) throws Exception {
        try {
            String sql = "";

            sql = sql + " select";
            sql = sql + " tb_Usuario_id";
            sql = sql + ",tb_Usuario_apellipat";
            sql = sql + ",tb_Usuario_apellimat";
            sql = sql + ",tb_Usuario_nombre";
            sql = sql + ",tb_Usuario_correo";
            sql = sql + ",tb_Usuario_cuenta";
            sql = sql + ",tb_Usuario_clave";
            sql = sql + ",tb_Usuario_estado";
            sql = sql + ",tb_Usuario_tipo";
            sql = sql + " from tb_Usuario";
            sql = sql + " where tb_Usuario_nombre like '%" + nombre + "%'";
            sql = sql + ";";

            List<Usuario> lista = new ArrayList<Usuario>();
            Usuario objeto;

            PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = ps.executeQuery();

            rs.beforeFirst();

            while (rs.next()) {
                objeto = new Usuario();

                objeto.setId(rs.getInt("tb_Usuario_id"));
                objeto.setApelliPat(rs.getString("tb_Usuario_apellipat"));
                objeto.setApelliMat(rs.getString("tb_Usuario_apellimat"));
                objeto.setNombre(rs.getString("tb_Usuario_nombre"));
                objeto.setCorreo(rs.getString("tb_Usuario_correo"));
                objeto.setCuenta(rs.getString("tb_Usuario_cuenta"));
                objeto.setClave(rs.getString("tb_Usuario_clave"));
                objeto.setEstado(rs.getString("tb_Usuario_estado"));
                objeto.setTipo(rs.getString("tb_Usuario_tipo"));

                lista.add(objeto);
            }

            return lista;
        } catch (Exception e) {
            throw e;
        }
    }

    public List<Usuario> MostrarLista() throws Exception {
        try {
            String sql = "";

            sql = sql + " select";
            sql = sql + " tb_Usuario_id";
            sql = sql + ",tb_Usuario_apellipat";
            sql = sql + ",tb_Usuario_apellimat";
            sql = sql + ",tb_Usuario_nombre";
            sql = sql + ",tb_Usuario_correo";
            sql = sql + ",tb_Usuario_cuenta";
            sql = sql + ",tb_Usuario_clave";
            sql = sql + ",tb_Usuario_estado";
            sql = sql + ",tb_Usuario_tipo";
            sql = sql + " from tb_Usuario";
            sql = sql + ";";

            List<Usuario> lista = new ArrayList<Usuario>();
            Usuario objeto;

            PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = ps.executeQuery();

            rs.beforeFirst();

            while (rs.next()) {
                objeto = new Usuario();

                objeto.setId(rs.getInt("tb_Usuario_id"));
                objeto.setApelliPat(rs.getString("tb_Usuario_apellipat"));
                objeto.setApelliMat(rs.getString("tb_Usuario_apellimat"));
                objeto.setNombre(rs.getString("tb_Usuario_nombre"));
                objeto.setCorreo(rs.getString("tb_Usuario_correo"));
                objeto.setCuenta(rs.getString("tb_Usuario_cuenta"));
                objeto.setClave(rs.getString("tb_Usuario_clave"));
                objeto.setEstado(rs.getString("tb_Usuario_estado"));
                objeto.setTipo(rs.getString("tb_Usuario_tipo"));

                lista.add(objeto);
            }

            return lista;
        } catch (Exception e) {
            throw e;
        }
    }
    
    public Usuario Ver(Integer id) throws Exception {
        try {
            String sql = "";

            sql = sql + " select";
            sql = sql + " tb_Usuario_id";
            sql = sql + ",tb_Usuario_apellipat";
            sql = sql + ",tb_Usuario_apellimat";
            sql = sql + ",tb_Usuario_nombre";
            sql = sql + ",tb_Usuario_correo";
            sql = sql + ",tb_Usuario_cuenta";
            sql = sql + ",tb_Usuario_clave";
            sql = sql + ",tb_Usuario_estado";
            sql = sql + ",tb_Usuario_tipo";
            sql = sql + " from tb_Usuario";
            sql = sql + " where tb_Usuario_id = " + id.toString();
            sql = sql + ";";

            Usuario objeto = null;

            PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = ps.executeQuery();

            rs.beforeFirst();

            if (rs.next()) {
                objeto = new Usuario();

                objeto.setId(rs.getInt("tb_Usuario_id"));
                objeto.setApelliPat(rs.getString("tb_Usuario_apellipat"));
                objeto.setApelliMat(rs.getString("tb_Usuario_apellimat"));
                objeto.setNombre(rs.getString("tb_Usuario_nombre"));
                objeto.setCorreo(rs.getString("tb_Usuario_correo"));
                objeto.setCuenta(rs.getString("tb_Usuario_cuenta"));
                objeto.setClave(rs.getString("tb_Usuario_clave"));
                objeto.setEstado(rs.getString("tb_Usuario_estado"));
                objeto.setTipo(rs.getString("tb_Usuario_tipo"));
            }

            return objeto;
        } catch (Exception e) {
            throw e;
        }
    }
    
        public Usuario BuscarUsuario(String cuenta) throws Exception {
        try {
            String sql = "";

            sql = sql + " select";
            sql = sql + " tb_Usuario_id";
            sql = sql + ",tb_Usuario_apellipat";
            sql = sql + ",tb_Usuario_apellimat";
            sql = sql + ",tb_Usuario_nombre";
            sql = sql + ",tb_Usuario_correo";
            sql = sql + ",tb_Usuario_cuenta";
            sql = sql + ",tb_Usuario_clave";
            sql = sql + ",tb_Usuario_estado";
            sql = sql + ",tb_Usuario_tipo";
            sql = sql + " from tb_Usuario";
            sql = sql + " where tb_Usuario_cuenta like '" + cuenta +"%'";
            sql = sql + ";";

            Usuario objeto = null;

            PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = ps.executeQuery();

            rs.beforeFirst();

            if (rs.next()) {
                objeto = new Usuario();

                objeto.setId(rs.getInt("tb_Usuario_id"));
                objeto.setApelliPat(rs.getString("tb_Usuario_apellipat"));
                objeto.setApelliMat(rs.getString("tb_Usuario_apellimat"));
                objeto.setNombre(rs.getString("tb_Usuario_nombre"));
                objeto.setCorreo(rs.getString("tb_Usuario_correo"));
                objeto.setCuenta(rs.getString("tb_Usuario_cuenta"));
                objeto.setClave(rs.getString("tb_Usuario_clave"));
                objeto.setEstado(rs.getString("tb_Usuario_estado"));
                objeto.setTipo(rs.getString("tb_Usuario_tipo"));
            }

            return objeto;
        } catch (Exception e) {
            throw e;
        }
    }

    public void Insertar(Usuario objeto) throws Exception {
        try {
            String dml = "";

            dml = dml + " insert into tb_Usuario(";
            dml = dml + "tb_Usuario_apellipat";
            dml = dml + ",tb_Usuario_apellimat";
            dml = dml + ",tb_Usuario_nombre";
            dml = dml + ",tb_Usuario_correo";
            dml = dml + ",tb_Usuario_cuenta";
            dml = dml + ",tb_Usuario_clave";
            dml = dml + ",tb_Usuario_estado";
            dml = dml + ",tb_Usuario_tipo";
            dml = dml + ") values (";
            dml = dml + " '" + objeto.getApelliPat() + "'";
            dml = dml + ",'" + objeto.getApelliMat() + "'";
            dml = dml + ",'" + objeto.getNombre() + "'";
            dml = dml + ",'" + objeto.getCorreo() + "'";
            dml = dml + ",'" + objeto.getCuenta() + "'";
            dml = dml + ",'" + objeto.getClave() + "'";
            dml = dml + ",'" + objeto.getEstado() + "'";
            dml = dml + ",'" + objeto.getTipo() + "'";
            dml = dml + " )";
            dml = dml + ";";

            PreparedStatement ps = connection.prepareStatement(dml);

            ps.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
    }

    public void Actualizar(Usuario objeto) throws Exception {
        try {
            String dml = "";

            dml = dml + " update tb_Usuario set";
            dml = dml + " tb_Usuario_apellipat = '" + objeto.getApelliPat() + "'";
            dml = dml + ",tb_Usuario_apellimat = '" + objeto.getApelliMat() + "'";
            dml = dml + ",tb_Usuario_nombre = '" + objeto.getNombre() + "'";
            dml = dml + ",tb_Usuario_correo = '" + objeto.getCorreo() + "'";
            dml = dml + ",tb_Usuario_cuenta = '" + objeto.getCuenta() + "'";
            dml = dml + ",tb_Usuario_clave = '" + objeto.getClave() + "'";
            dml = dml + ",tb_Usuario_estado = '" + objeto.getEstado() + "'";
            dml = dml + ",tb_Usuario_tipo = '" + objeto.getTipo() + "'";
            dml = dml + "where tb_Usuario_id = " + objeto.getId().toString();
            dml = dml + ";";

            PreparedStatement ps = connection.prepareStatement(dml);

            ps.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
    }

    public void Eliminar(Usuario objeto) throws Exception {
        try {
            String dml = "";

            dml = dml + " delete from tb_Usuario";
            dml = dml + " where tb_Usuario_id = " + objeto.getId().toString();
            dml = dml + ";";

            PreparedStatement ps = connection.prepareStatement(dml);

            ps.executeUpdate();
        } catch (Exception e) {
            throw e;
        }
    }
}
