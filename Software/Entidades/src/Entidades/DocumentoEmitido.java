/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.sql.Timestamp;
import java.util.Date;

/**
 *
 * @author Carlos Anthony
 */
public class DocumentoEmitido {

    private Integer Id;
    private Date Año;
    private String Num;
    private Date FechaEmision;
    private String Para;
    private String Asunto;
    private String NumExpediente;
    private Date FechaRecepcion;
    private String Tipo;
    private TipoDocumento TipoDocumento;
    private UnidadTramite UnidadTramite;

    public DocumentoEmitido() {
    }

    public DocumentoEmitido(Integer Id) {
        this.Id = Id;
    }

    public DocumentoEmitido(Integer Id, Date Año, String Num, Date FechaEmision, String Para, String Asunto, String NumExpediente, Date FechaRecepcion, String Tipo, TipoDocumento TipoDocumento, UnidadTramite UnidadTramite) {
        this.Id = Id;
        this.Año = Año;
        this.Num = Num;
        this.FechaEmision = FechaEmision;
        this.Para = Para;
        this.Asunto = Asunto;
        this.NumExpediente = NumExpediente;
        this.FechaRecepcion = FechaRecepcion;
        this.Tipo = Tipo;
        this.TipoDocumento = TipoDocumento;
        this.UnidadTramite = UnidadTramite;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public Date getAño() {
        return Año;
    }

    public void setAño(Date Año) {
        this.Año = Año;
    }

    public String getNum() {
        return Num;
    }

    public void setNum(String Num) {
        this.Num = Num;
    }

    public Date getFechaEmision() {
        return FechaEmision;
    }

    public void setFechaEmision(Date FechaEmision) {
        this.FechaEmision = FechaEmision;
    }

    public String getPara() {
        return Para;
    }

    public void setPara(String Para) {
        this.Para = Para;
    }

    public String getAsunto() {
        return Asunto;
    }

    public void setAsunto(String Asunto) {
        this.Asunto = Asunto;
    }

    public String getNumExpediente() {
        return NumExpediente;
    }

    public void setNumExpediente(String NumExpediente) {
        this.NumExpediente = NumExpediente;
    }

    public Date getFechaRecepcion() {
        return FechaRecepcion;
    }

    public void setFechaRecepcion(Date FechaRecepcion) {
        this.FechaRecepcion = FechaRecepcion;
    }

    public String getTipo() {
        return Tipo;
    }

    public void setTipo(String Tipo) {
        this.Tipo = Tipo;
    }

    public TipoDocumento getTipoDocumento() {
        return TipoDocumento;
    }

    public void setTipoDocumento(TipoDocumento TipoDocumento) {
        this.TipoDocumento = TipoDocumento;
    }

    public UnidadTramite getUnidadTramite() {
        return UnidadTramite;
    }

    public void setUnidadTramite(UnidadTramite UnidadTramite) {
        this.UnidadTramite = UnidadTramite;
    }

   
    
     @Override
    public String toString() {
        return this.Asunto;
    }
}
