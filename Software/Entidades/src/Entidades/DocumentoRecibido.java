/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Entidades;

import java.sql.Timestamp;
import java.util.Date;

/**
 *
 * @author Carlos Anthony
 */
public class DocumentoRecibido {
    private Integer Id;
    private Date AñoExpediente;
    private String NumExpediente;
    private String Num;
    private String De;
    private String Asunto;
    private String Observacion;
    private String Tipo;
    private String Estado;
    private TipoDocumento TipoDocumento;

    public DocumentoRecibido() {
    }

    public DocumentoRecibido(Integer Id) {
        this.Id = Id;
    }

    public DocumentoRecibido(Integer Id, Date AñoExpediente, String NumExpediente, String Num, String De, String Asunto, String Observacion, String Tipo, String Estado, TipoDocumento TipoDocumento) {
        this.Id = Id;
        this.AñoExpediente = AñoExpediente;
        this.NumExpediente = NumExpediente;
        this.Num = Num;
        this.De = De;
        this.Asunto = Asunto;
        this.Observacion = Observacion;
        this.Tipo = Tipo;
        this.Estado = Estado;
        this.TipoDocumento = TipoDocumento;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public Date getAñoExpediente() {
        return AñoExpediente;
    }

    public void setAñoExpediente(Date AñoExpediente) {
        this.AñoExpediente = AñoExpediente;
    }

    public String getNumExpediente() {
        return NumExpediente;
    }

    public void setNumExpediente(String NumExpediente) {
        this.NumExpediente = NumExpediente;
    }

    public String getNum() {
        return Num;
    }

    public void setNum(String Num) {
        this.Num = Num;
    }

    public String getDe() {
        return De;
    }

    public void setDe(String De) {
        this.De = De;
    }

    public String getAsunto() {
        return Asunto;
    }

    public void setAsunto(String Asunto) {
        this.Asunto = Asunto;
    }

    public String getObservacion() {
        return Observacion;
    }

    public void setObservacion(String Observacion) {
        this.Observacion = Observacion;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String Estado) {
        this.Estado = Estado;
    }

    public TipoDocumento getTipoDocumento() {
        return TipoDocumento;
    }

    public void setTipoDocumento(TipoDocumento TipoDocumento) {
        this.TipoDocumento = TipoDocumento;
    }

    public String getTipo() {
        return Tipo;
    }

    public void setTipo(String Tipo) {
        this.Tipo = Tipo;
    }
 
    @Override
    public String toString() {
        return this.Asunto;
    }
}
