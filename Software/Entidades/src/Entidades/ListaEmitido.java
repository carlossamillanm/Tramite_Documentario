/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.util.Date;

/**
 *
 * @author Carlos Anthony
 */
public class ListaEmitido {

    private Date Fecha;
    private String Tipodoc;
    private String numdoc;
    private String Tipo;
    private String numexp;
    private String Estado;

    public ListaEmitido() {
    }

    public ListaEmitido(String numexp) {
        this.numexp = numexp;
    }

    public ListaEmitido(Date Fecha, String Tipodoc, String numdoc, String Tipo, String numexp, String Estado) {
        this.Fecha = Fecha;
        this.Tipodoc = Tipodoc;
        this.numdoc = numdoc;
        this.Tipo = Tipo;
        this.numexp = numexp;
        this.Estado = Estado;
    }

    public Date getFecha() {
        return Fecha;
    }

    public void setFecha(Date Fecha) {
        this.Fecha = Fecha;
    }

    public String getTipodoc() {
        return Tipodoc;
    }

    public void setTipodoc(String Tipodoc) {
        this.Tipodoc = Tipodoc;
    }

    public String getNumdoc() {
        return numdoc;
    }

    public void setNumdoc(String numdoc) {
        this.numdoc = numdoc;
    }

    public String getTipo() {
        return Tipo;
    }

    public void setTipo(String Tipo) {
        this.Tipo = Tipo;
    }

    public String getNumexp() {
        return numexp;
    }

    public void setNumexp(String numexp) {
        this.numexp = numexp;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String Estado) {
        this.Estado = Estado;
    }

    @Override
    public String toString() {
        return this.numexp;
    }

}
