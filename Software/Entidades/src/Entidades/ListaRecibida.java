/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Entidades;

import java.util.Date;

/**
 *
 * @author Carlos Anthony
 */
public class ListaRecibida {
 
    private String sn;
    private Date Fecha;
    private String De;
    private String TipoTramite;
    private String numexp;

    public ListaRecibida() {
    }

    public ListaRecibida(String numexp) {
        this.numexp = numexp;
    }

    public ListaRecibida(String sn, Date Fecha, String De, String TipoTramite, String numexp) {
        this.sn = sn;
        this.Fecha = Fecha;
        this.De = De;
        this.TipoTramite = TipoTramite;
        this.numexp = numexp;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public Date getFecha() {
        return Fecha;
    }

    public void setFecha(Date Fecha) {
        this.Fecha = Fecha;
    }

    public String getDe() {
        return De;
    }

    public void setDe(String De) {
        this.De = De;
    }

    public String getTipoTramite() {
        return TipoTramite;
    }

    public void setTipoTramite(String TipoTramite) {
        this.TipoTramite = TipoTramite;
    }

    public String getNumexp() {
        return numexp;
    }

    public void setNumexp(String numexp) {
        this.numexp = numexp;
    }

     @Override
    public String toString() {
        return this.numexp;
    }
    
}
