package Entidades;

import java.util.Date;

public class Permiso {
    private Integer Id;
    private Date Fechainicio;
    private Date Fechafin;
    private String Tipo;
    private Usuario Usuario;
    private UnidadOrganizativa UnidadOrganizativa;
    private UnidadTramite UnidadTramite;

    public Permiso() {
    }

    public Permiso(Integer Id) {
        this.Id = Id;
    }

    public Permiso(Integer Id, Date Fechainicio, Date Fechafin, String Tipo, Usuario Usuario, UnidadOrganizativa UnidadOrganizativa, UnidadTramite UnidadTramite) {
        this.Id = Id;
        this.Fechainicio = Fechainicio;
        this.Fechafin = Fechafin;
        this.Tipo = Tipo;
        this.Usuario = Usuario;
        this.UnidadOrganizativa = UnidadOrganizativa;
        this.UnidadTramite = UnidadTramite;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public Date getFechainicio() {
        return Fechainicio;
    }

    public void setFechainicio(Date Fechainicio) {
        this.Fechainicio = Fechainicio;
    }

    public Date getFechafin() {
        return Fechafin;
    }

    public void setFechafin(Date Fechafin) {
        this.Fechafin = Fechafin;
    }

    public String getTipo() {
        return Tipo;
    }

    public void setTipo(String Tipo) {
        this.Tipo = Tipo;
    }

    public Usuario getUsuario() {
        return Usuario;
    }

    public void setUsuario(Usuario Usuario) {
        this.Usuario = Usuario;
    }

    public UnidadOrganizativa getUnidadOrganizativa() {
        return UnidadOrganizativa;
    }

    public void setUnidadOrganizativa(UnidadOrganizativa UnidadOrganizativa) {
        this.UnidadOrganizativa = UnidadOrganizativa;
    }

    public UnidadTramite getUnidadTramite() {
        return UnidadTramite;
    }

    public void setUnidadTramite(UnidadTramite UnidadTramite) {
        this.UnidadTramite = UnidadTramite;
    }

    @Override
    public String toString() {
        return this.Tipo;
    }
    
    
    
}
