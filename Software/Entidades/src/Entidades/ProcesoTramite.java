/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Entidades;

/**
 *
 * @author Carlos Anthony
 */
public class ProcesoTramite {
    private Integer Id;
    private UnidadTramite UnidadTramite;
    private Tramite Tramite;

    public ProcesoTramite() {
    }

    public ProcesoTramite(Integer Id) {
        this.Id = Id;
    }

    public ProcesoTramite(Integer Id, UnidadTramite UnidadTramite, Tramite Tramite) {
        this.Id = Id;
        this.UnidadTramite = UnidadTramite;
        this.Tramite = Tramite;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public UnidadTramite getUnidadTramite() {
        return UnidadTramite;
    }

    public void setUnidadTramite(UnidadTramite UnidadTramite) {
        this.UnidadTramite = UnidadTramite;
    }

    public Tramite getTramite() {
        return Tramite;
    }

    public void setTramite(Tramite Tramite) {
        this.Tramite = Tramite;
    }
    
    
}
