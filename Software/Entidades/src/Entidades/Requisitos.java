/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Entidades;

/**
 *
 * @author Carlos Anthony
 */
public class Requisitos {
    private Integer Id;
    private String Descripcion;
    private String Estado;
    private TipoTramite TipoTramite;

    public Requisitos() {
    }

    public Requisitos(Integer Id) {
        this.Id = Id;
    }

    public Requisitos(Integer Id, String Descripcion, String Estado, TipoTramite TipoTramite) {
        this.Id = Id;
        this.Descripcion = Descripcion;
        this.Estado = Estado;
        this.TipoTramite = TipoTramite;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String Estado) {
        this.Estado = Estado;
    }

    public TipoTramite getTipoTramite() {
        return TipoTramite;
    }

    public void setTipoTramite(TipoTramite TipoTramite) {
        this.TipoTramite = TipoTramite;
    }
    
      @Override
    public String toString() {
        return this.Descripcion;
    }
}
