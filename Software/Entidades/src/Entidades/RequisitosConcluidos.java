/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

/**
 *
 * @author Carlos Anthony
 */
public class RequisitosConcluidos {

    private Integer Id;
    private Requisitos Requisitos;
    private DocumentoRecibido DocumentoRecibido;

    public RequisitosConcluidos() {
    }

    public RequisitosConcluidos(Integer Id) {
        this.Id = Id;
    }

    public RequisitosConcluidos(Integer Id, Requisitos Requisitos, DocumentoRecibido DocumentoRecibido) {
        this.Id = Id;
        this.Requisitos = Requisitos;
        this.DocumentoRecibido = DocumentoRecibido;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public Requisitos getRequisitos() {
        return Requisitos;
    }

    public void setRequisitos(Requisitos Requisitos) {
        this.Requisitos = Requisitos;
    }

    public DocumentoRecibido getDocumentoRecibido() {
        return DocumentoRecibido;
    }

    public void setDocumentoRecibido(DocumentoRecibido DocumentoRecibido) {
        this.DocumentoRecibido = DocumentoRecibido;
    }

    
}
