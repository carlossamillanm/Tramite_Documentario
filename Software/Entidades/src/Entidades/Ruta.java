/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Entidades;

import java.sql.Timestamp;

/**
 *
 * @author Carlos Anthony
 */
public class Ruta {
    private Integer Id;
    private Timestamp fechahora;
    private String TipoExpediente;
    private DocumentoRecibido DocumentoRecibido;
    private UnidadTramite UnidadTramite;

    public Ruta() {
    }

    public Ruta(Integer Id) {
        this.Id = Id;
    }

    public Ruta(Integer Id, Timestamp fechahora, String TipoExpediente, DocumentoRecibido DocumentoRecibido, UnidadTramite UnidadTramite) {
        this.Id = Id;
        this.fechahora = fechahora;
        this.TipoExpediente = TipoExpediente;
        this.DocumentoRecibido = DocumentoRecibido;
        this.UnidadTramite = UnidadTramite;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public Timestamp getFechahora() {
        return fechahora;
    }

    public void setFechahora(Timestamp fechahora) {
        this.fechahora = fechahora;
    }

    public String getTipoExpediente() {
        return TipoExpediente;
    }

    public void setTipoExpediente(String TipoExpediente) {
        this.TipoExpediente = TipoExpediente;
    }

    public DocumentoRecibido getDocumentoRecibido() {
        return DocumentoRecibido;
    }

    public void setDocumentoRecibido(DocumentoRecibido DocumentoRecibido) {
        this.DocumentoRecibido = DocumentoRecibido;
    }

    public UnidadTramite getUnidadTramite() {
        return UnidadTramite;
    }

    public void setUnidadTramite(UnidadTramite UnidadTramite) {
        this.UnidadTramite = UnidadTramite;
    }
    
      @Override
    public String toString() {
        return this.TipoExpediente;
    }
    
}
