/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

/**
 *
 * @author Carlos Anthony
 */
public class TipoDocumento {

    private Integer Id;
    private String Nombre;
    private String Estado;

    public TipoDocumento() {
    }

    public TipoDocumento(Integer Id) {
        this.Id = Id;
    }

    public TipoDocumento(Integer Id, String Nombre, String Estado) {
        this.Id = Id;
        this.Nombre = Nombre;
        this.Estado = Estado;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String Estado) {
        this.Estado = Estado;
    }

    @Override
    public String toString() {
        return this.Nombre;
    }

}
