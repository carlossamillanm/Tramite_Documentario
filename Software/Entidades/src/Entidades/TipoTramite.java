/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

/**
 *
 * @author Carlos Anthony
 */
public class TipoTramite {

    private Integer Id;
    private String nombre;
    private Integer TiempoEstimado;
    private String Estado;
    private UnidadTramite UnidadTramite;

    public TipoTramite() {
    }

    public TipoTramite(Integer Id) {
        this.Id = Id;
    }

    public TipoTramite(Integer Id, String nombre, Integer TiempoEstimado, String Estado, UnidadTramite UnidadTramite) {
        this.Id = Id;
        this.nombre = nombre;
        this.TiempoEstimado = TiempoEstimado;
        this.Estado = Estado;
        this.UnidadTramite = UnidadTramite;
    }



    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getTiempoEstimado() {
        return TiempoEstimado;
    }

    public void setTiempoEstimado(Integer TiempoEstimado) {
        this.TiempoEstimado = TiempoEstimado;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String Estado) {
        this.Estado = Estado;
    }

    public UnidadTramite getUnidadTramite() {
        return UnidadTramite;
    }

    public void setUnidadTramite(UnidadTramite UnidadTramite) {
        this.UnidadTramite = UnidadTramite;
    }

    @Override
    public String toString() {
        return this.nombre;
    }
}
