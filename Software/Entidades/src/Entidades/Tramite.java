/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Entidades;

import java.sql.Timestamp;
import java.util.Date;

/**
 *
 * @author Carlos Anthony
 */

public class Tramite {
    private Integer Id;
    private String Descripcion;
    private String TipoTraslado;
    private String TipoEntidad;
    private String DescripcionEntidad;
    private Date fechaemision;
    private DocumentoEmitido DocumentoEmitido;
    private Ruta Ruta;

    public Tramite() {
    }

    public Tramite(Integer Id) {
        this.Id = Id;
    }

    public Tramite(Integer Id, String Descripcion, String TipoTraslado, String TipoEntidad, String DescripcionEntidad, Date fechaemision, DocumentoEmitido DocumentoEmitido, Ruta Ruta) {
        this.Id = Id;
        this.Descripcion = Descripcion;
        this.TipoTraslado = TipoTraslado;
        this.TipoEntidad = TipoEntidad;
        this.DescripcionEntidad = DescripcionEntidad;
        this.fechaemision = fechaemision;
        this.DocumentoEmitido = DocumentoEmitido;
        this.Ruta = Ruta;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }

    public String getTipoTraslado() {
        return TipoTraslado;
    }

    public void setTipoTraslado(String TipoTraslado) {
        this.TipoTraslado = TipoTraslado;
    }

    public String getTipoEntidad() {
        return TipoEntidad;
    }

    public void setTipoEntidad(String TipoEntidad) {
        this.TipoEntidad = TipoEntidad;
    }

    public String getDescripcionEntidad() {
        return DescripcionEntidad;
    }

    public void setDescripcionEntidad(String DescripcionEntidad) {
        this.DescripcionEntidad = DescripcionEntidad;
    }

    public Date getFechaemision() {
        return fechaemision;
    }

    public void setFechaemision(Date fechaemision) {
        this.fechaemision = fechaemision;
    }

    public DocumentoEmitido getDocumentoEmitido() {
        return DocumentoEmitido;
    }

    public void setDocumentoEmitido(DocumentoEmitido DocumentoEmitido) {
        this.DocumentoEmitido = DocumentoEmitido;
    }

    public Ruta getRuta() {
        return Ruta;
    }

    public void setRuta(Ruta Ruta) {
        this.Ruta = Ruta;
    }

      @Override
    public String toString() {
        return this.Descripcion;
    }
    
}
