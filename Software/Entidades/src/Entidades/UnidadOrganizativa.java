package Entidades;

public class UnidadOrganizativa {
   private Integer Id;
   private String Nombre;
   private String Abreviatura;
   private String Estado;

    public UnidadOrganizativa() {
    }

    public UnidadOrganizativa(Integer Id) {
        this.Id = Id;
    }

    public UnidadOrganizativa(Integer Id, String Nombre, String Abreviatura, String Estado) {
        this.Id = Id;
        this.Nombre = Nombre;
        this.Abreviatura = Abreviatura;
        this.Estado = Estado;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getAbreviatura() {
        return Abreviatura;
    }

    public void setAbreviatura(String Abreviatura) {
        this.Abreviatura = Abreviatura;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String Estado) {
        this.Estado = Estado;
    }

    @Override
    public String toString() {
        return this.Nombre;
    }
   
   
}
