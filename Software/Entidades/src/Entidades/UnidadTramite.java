package Entidades;

public class UnidadTramite {
    private Integer Id;
    private String Nombre;
    private String Abreviatura;
    private String Responsable;
    private String Estado;
    private UnidadOrganizativa unidadorganizativa;

    public UnidadTramite() {
    }

    public UnidadTramite(Integer Id) {
        this.Id = Id;
    }

    public UnidadTramite(Integer Id, String Nombre, String Abreviatura, String Responsable, String Estado, UnidadOrganizativa unidadorganizativa) {
        this.Id = Id;
        this.Nombre = Nombre;
        this.Abreviatura = Abreviatura;
        this.Responsable = Responsable;
        this.Estado = Estado;
        this.unidadorganizativa = unidadorganizativa;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getAbreviatura() {
        return Abreviatura;
    }

    public void setAbreviatura(String Abreviatura) {
        this.Abreviatura = Abreviatura;
    }

    public String getResponsable() {
        return Responsable;
    }

    public void setResponsable(String Responsable) {
        this.Responsable = Responsable;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String Estado) {
        this.Estado = Estado;
    }

    public UnidadOrganizativa getUnidadorganizativa() {
        return unidadorganizativa;
    }

    public void setUnidadorganizativa(UnidadOrganizativa unidadorganizativa) {
        this.unidadorganizativa = unidadorganizativa;
    }

    @Override
    public String toString() {
        return this.Nombre;
    }
    
    
}
