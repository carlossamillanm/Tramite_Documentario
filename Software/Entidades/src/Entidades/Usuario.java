package Entidades;

public class Usuario {
    private Integer Id;
    private String ApelliPat;
    private String ApelliMat;
    private String Nombre;
    private String Correo;
    private String Cuenta;
    private String Clave;
    private String Estado;
    private String Tipo;

    public Usuario() {
    }

    public Usuario(Integer Id) {
        this.Id = Id;
    }

    public Usuario(Integer Id, String ApelliPat, String ApelliMat, String Nombre, String Correo, String Cuenta, String Clave, String Estado, String Tipo) {
        this.Id = Id;
        this.ApelliPat = ApelliPat;
        this.ApelliMat = ApelliMat;
        this.Nombre = Nombre;
        this.Correo = Correo;
        this.Cuenta = Cuenta;
        this.Clave = Clave;
        this.Estado = Estado;
        this.Tipo = Tipo;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public String getApelliPat() {
        return ApelliPat;
    }

    public void setApelliPat(String ApelliPat) {
        this.ApelliPat = ApelliPat;
    }

    public String getApelliMat() {
        return ApelliMat;
    }

    public void setApelliMat(String ApelliMat) {
        this.ApelliMat = ApelliMat;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getCorreo() {
        return Correo;
    }

    public void setCorreo(String Correo) {
        this.Correo = Correo;
    }

    public String getCuenta() {
        return Cuenta;
    }

    public void setCuenta(String Cuenta) {
        this.Cuenta = Cuenta;
    }

    public String getClave() {
        return Clave;
    }

    public void setClave(String Clave) {
        this.Clave = Clave;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String Estado) {
        this.Estado = Estado;
    }

    public String getTipo() {
        return Tipo;
    }

    public void setTipo(String Tipo) {
        this.Tipo = Tipo;
    }

    @Override
    public String toString() {
        return this.Nombre;
    }
 
}
