/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Logica;

import AccesoDatos.Conexion;
import AccesoDatos.TramiteAD;
import Entidades.Tramite;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Carlos Anthony
 */
public class Datos_Documentos_Emitidos {
    
    Conexion conn = new Conexion();
    
    public List<Entidades.Tramite> BuscarFechas(String Inicio,String Fin) throws Exception{
        try {
            TramiteAD t=new TramiteAD(conn.Abrir());
            List<Tramite> lista=new ArrayList<Tramite>();
            lista=t.ListarFecha(Inicio, Fin);
            return lista;
        } catch (Exception e) {
            throw e;
        }
    }
    
      public List<Entidades.Tramite> BuscarFechasTipo(String Inicio,String Fin,String Tipo) throws Exception{
        try {
            TramiteAD t=new TramiteAD(conn.Abrir());
            List<Tramite> lista=new ArrayList<Tramite>();
            lista=t.ListarFechaTipo(Inicio, Fin, Tipo);
            return lista;
        } catch (Exception e) {
            throw e;
        }
    }
    
    public List<Entidades.Tramite> BuscarTipos(String Inicio,String Fin,String Tipo) throws Exception{
        try {
            TramiteAD t=new TramiteAD(conn.Abrir());
            List<Tramite> lista=new ArrayList<Tramite>();
            t.ListarTipo(Inicio, Fin, Tipo);
            return lista;
        } catch (Exception e) {
            throw e;
        }
    }
}
