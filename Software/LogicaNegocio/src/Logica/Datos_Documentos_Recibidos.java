/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;

import AccesoDatos.Conexion;
import AccesoDatos.DocumentoRecibidoAD;
import Entidades.DocumentoRecibido;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Carlos Anthony
 */
public class Datos_Documentos_Recibidos {

    Conexion conn = new Conexion();

    public List<Entidades.DocumentoRecibido> BuscarFechas(String inicio, String fin) throws Exception {
        try {
            DocumentoRecibidoAD dr = new DocumentoRecibidoAD(conn.Abrir());
            List<DocumentoRecibido> lista = new ArrayList<DocumentoRecibido>();
            lista = dr.ListarFechas(inicio, fin);
            return lista;
        } catch (Exception e) {
            throw e;
        }
    }

    public List<Entidades.DocumentoRecibido> BuscarTodo(String inicio, String fin, String Tipo, String Estado) throws Exception {
        try {
            DocumentoRecibidoAD dr = new DocumentoRecibidoAD(conn.Abrir());
            List<DocumentoRecibido> lista = new ArrayList<DocumentoRecibido>();
            lista = dr.ListarTodo(inicio, fin, Tipo, Estado);
            return lista;
        } catch (Exception e) {
            throw e;
        }
    }

    public List<Entidades.DocumentoRecibido> BuscarTipos(String inicio, String fin, String Tipo) throws Exception {
        try {
            DocumentoRecibidoAD dr = new DocumentoRecibidoAD(conn.Abrir());
            List<DocumentoRecibido> lista = new ArrayList<DocumentoRecibido>();
            lista = dr.ListarTipos(inicio, fin, Tipo);
            return lista;
        } catch (Exception e) {
            throw e;
        }
    }

    public List<Entidades.DocumentoRecibido> BuscarEstado(String inicio, String fin, String Estado) throws Exception {
        try {
            DocumentoRecibidoAD dr = new DocumentoRecibidoAD(conn.Abrir());
            List<DocumentoRecibido> lista = new ArrayList<DocumentoRecibido>();
            lista = dr.ListarTipos(inicio, fin, Estado);
            return lista;
        } catch (Exception e) {
            throw e;
        }
    }
}
