/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Logica;

import AccesoDatos.Conexion;
import AccesoDatos.DocumentoRecibidoAD;
import AccesoDatos.RutaAD;
import Entidades.DocumentoRecibido;
import Entidades.Ruta;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Carlos Anthony
 */
public class Datos_Expediente_Curso {
    
     Conexion conn = new Conexion();
     
     public List<Ruta> BuscarRutas(String Id) throws Exception{
         try {
             RutaAD ru=new RutaAD(conn.Abrir());
             List<Ruta> lista=new ArrayList<Ruta>();
             lista=ru.BuscarRutas(Id);
             return lista;
         } catch (Exception e) {
             throw e;
         } 
     }
     
     public List<DocumentoRecibido> MostrarDocumentoRecibidostotales() throws Exception {
        try {
            DocumentoRecibidoAD dr = new DocumentoRecibidoAD(conn.Abrir());
            List<Entidades.DocumentoRecibido> lista = new ArrayList<Entidades.DocumentoRecibido>();
            lista = dr.MostrarLista();

            return lista;
        } catch (Exception e) {
            throw e;
        }
    }
    
     public boolean InsertarRuta(Ruta objeto) throws Exception {
        try {
            RutaAD r = new RutaAD(conn.Abrir());
            r.Insertar(objeto);
            return true;
        } catch (Exception e) {
            throw e;
        }
    }
     
     public boolean Actualizar(DocumentoRecibido objeto) throws Exception {
        try {
            DocumentoRecibidoAD dr = new DocumentoRecibidoAD(conn.Abrir());
            dr.Actualizar(objeto);
            return true;
        } catch (Exception e) {
            throw e;
        }
    }
     
}
