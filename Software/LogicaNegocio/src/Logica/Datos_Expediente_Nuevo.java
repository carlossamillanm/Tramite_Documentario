/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;

import AccesoDatos.Conexion;
import AccesoDatos.DocumentoRecibidoAD;
import AccesoDatos.RequisitoConcluidosAD;
import AccesoDatos.RequisitosAD;
import AccesoDatos.RutaAD;
import AccesoDatos.TipoDocumentoAD;
import AccesoDatos.TipoTramiteAD;
import Entidades.DocumentoRecibido;
import Entidades.Requisitos;
import Entidades.RequisitosConcluidos;
import Entidades.Ruta;
import Entidades.TipoDocumento;
import Entidades.TipoTramite;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Carlos Anthony
 */
public class Datos_Expediente_Nuevo {

    Conexion conn = new Conexion();

    public List<TipoTramite> LlenarComboTiposTramite(Integer id) throws Exception {
        try {
            TipoTramiteAD tt = new TipoTramiteAD(conn.Abrir());
            List<Entidades.TipoTramite> lista = new ArrayList<Entidades.TipoTramite>();
            lista = tt.TipoTramite(id);
            return lista;
        } catch (Exception e) {
            throw e;
        }

    }

    public List<TipoDocumento> MostrarListaDocumentos() throws Exception {
        try {
            TipoDocumentoAD tid = new TipoDocumentoAD(conn.Abrir());
            List<Entidades.TipoDocumento> lista = new ArrayList<Entidades.TipoDocumento>();
            lista = tid.ListaTotal();

            return lista;
        } catch (Exception e) {
            throw e;
        }
    }

    public List<DocumentoRecibido> MostrarDocumentoRecibidostotales() throws Exception {
        try {
            DocumentoRecibidoAD dr = new DocumentoRecibidoAD(conn.Abrir());
            List<Entidades.DocumentoRecibido> lista = new ArrayList<Entidades.DocumentoRecibido>();
            lista = dr.MostrarLista();

            return lista;
        } catch (Exception e) {
            throw e;
        }
    }

    public List<Requisitos> ListaTipo(Integer id) throws Exception {
        try {
            RequisitosAD tt = new RequisitosAD(conn.Abrir());
            List<Entidades.Requisitos> lista = new ArrayList<Entidades.Requisitos>();
            lista = tt.ListarTipo(id);
            return lista;
        } catch (Exception e) {
            throw e;
        }

    }
    
    public DocumentoRecibido Buscar(String id) throws Exception {
        try {
            DocumentoRecibidoAD dr = new DocumentoRecibidoAD(conn.Abrir());
            DocumentoRecibido objeto = new DocumentoRecibido();
            objeto = dr.Ver(id);
            return objeto;
        } catch (Exception e) {
            throw e;
        }

    }

    public boolean Insertar(DocumentoRecibido objeto) throws Exception {
        try {
            DocumentoRecibidoAD dr = new DocumentoRecibidoAD(conn.Abrir());
            dr.Insertar(objeto);
            return true;
        } catch (Exception e) {
            throw e;
        }
    }
    
    public boolean Actualizar(DocumentoRecibido objeto) throws Exception {
        try {
            DocumentoRecibidoAD dr = new DocumentoRecibidoAD(conn.Abrir());
            dr.Actualizar(objeto);
            return true;
        } catch (Exception e) {
            throw e;
        }
    }

    public boolean InsertarRuta(Ruta objeto) throws Exception {
        try {
            RutaAD r = new RutaAD(conn.Abrir());
            r.Insertar(objeto);
            return true;
        } catch (Exception e) {
            throw e;
        }
    }

    public boolean InsertarRequisitos(RequisitosConcluidos objeto) throws Exception {
        try {
            RequisitoConcluidosAD rc = new RequisitoConcluidosAD(conn.Abrir());
            rc.Insertar(objeto);
            return true;
        } catch (Exception e) {
            throw e;
        }
    }

}
