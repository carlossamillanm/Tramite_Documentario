/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;

import AccesoDatos.Conexion;
import AccesoDatos.DocumentoEmitidoAD;
import AccesoDatos.DocumentoRecibidoAD;
import AccesoDatos.TipoTramiteAD;
import AccesoDatos.TramiteAD;
import Entidades.DocumentoEmitido;
import Entidades.DocumentoRecibido;
import Entidades.TipoTramite;
import Entidades.Tramite;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Carlos Anthony
 */
public class Datos_Grafico_General {

    Conexion conn = new Conexion();

    public List<Entidades.DocumentoRecibido> BuscarFechasRecibidos(String inicio, String fin) throws Exception {
        try {
            DocumentoRecibidoAD dr = new DocumentoRecibidoAD(conn.Abrir());
            List<DocumentoRecibido> lista = new ArrayList<DocumentoRecibido>();
            lista = dr.ListarFechas(inicio, fin);
            return lista;
        } catch (Exception e) {
            throw e;
        }
    }
    
    public List<Entidades.DocumentoEmitido> BuscarFechasEmitidos(String inicio, String fin) throws Exception {
        try {
            DocumentoEmitidoAD dr = new DocumentoEmitidoAD(conn.Abrir());
            List<DocumentoEmitido> lista = new ArrayList<DocumentoEmitido>();
            lista = dr.ListarFechas(inicio, fin);
            return lista;
        } catch (Exception e) {
            throw e;
        }
    }

    public TipoTramite MostrarLista(Integer id) throws Exception {
        try {
            TipoTramiteAD tt = new TipoTramiteAD(conn.Abrir());
            TipoTramite objeto = new TipoTramite();
            objeto = tt.Vert(id);
            
            return objeto;
        } catch (Exception e) {
            throw e;
        }

    }
    
    public List<Entidades.Tramite> BuscarFechasEmitidosTr(String Inicio, String Fin, String id) throws Exception {
        try {
            TramiteAD t = new TramiteAD(conn.Abrir());
            List<Tramite> lista = new ArrayList<Tramite>();
            lista = t.ListarTramite(Inicio, Fin, id);
            return lista;
        } catch (Exception e) {
            throw e;
        }
    }

}
