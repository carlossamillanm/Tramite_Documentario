/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;

import AccesoDatos.Conexion;
import AccesoDatos.DocumentoEmitidoAD;
import AccesoDatos.DocumentoRecibidoAD;
import AccesoDatos.RutaAD;
import AccesoDatos.TipoDocumentoAD;
import AccesoDatos.TramiteAD;
import Entidades.DocumentoEmitido;
import Entidades.DocumentoRecibido;
import Entidades.Ruta;
import Entidades.TipoDocumento;
import Entidades.Tramite;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Carlos Anthony
 */
public class Datos_Inicio_Tramite {

    Conexion conn = new Conexion();

    public List<TipoDocumento> MostrarListaDocumentos() throws Exception {
        try {
            TipoDocumentoAD tid = new TipoDocumentoAD(conn.Abrir());
            List<Entidades.TipoDocumento> lista = new ArrayList<Entidades.TipoDocumento>();
            lista = tid.ListaTotal();

            return lista;
        } catch (Exception e) {
            throw e;
        }
    }

    public DocumentoEmitido BuscarDoc(String id) throws Exception {
        try {
            DocumentoEmitidoAD dea = new DocumentoEmitidoAD(conn.Abrir());
            DocumentoEmitido de = new DocumentoEmitido();
            de = dea.VerTramite(id);
            return de;
        } catch (Exception e) {
            throw e;
        }
    }

    public boolean Insertar(Tramite objeto) throws Exception {
        try {
            TramiteAD t = new TramiteAD(conn.Abrir());
            t.Insertar(objeto);
            return true;
        } catch (Exception e) {
            throw e;
        }
    }
    
     public boolean Actualizar(DocumentoRecibido objeto) throws Exception {
        try {
            DocumentoRecibidoAD dr = new DocumentoRecibidoAD(conn.Abrir());
            dr.Actualizar(objeto);
            return true;
        } catch (Exception e) {
            throw e;
        }
    }

    public boolean InsertarEmitido(DocumentoEmitido Objeto) throws Exception {
        try {
            DocumentoEmitidoAD de = new DocumentoEmitidoAD(conn.Abrir());
            de.Insertar(Objeto);
            return true;
        } catch (Exception e) {
            throw e;
        }
    }

    public boolean InsertarRuta(Ruta objeto) throws Exception {
        try {
            RutaAD r = new RutaAD(conn.Abrir());
            r.Insertar(objeto);
            return true;
        } catch (Exception e) {
            throw e;
        }
    }

    public DocumentoRecibido Buscar(String id) throws Exception {
        try {
            DocumentoRecibidoAD dr = new DocumentoRecibidoAD(conn.Abrir());
            DocumentoRecibido objeto = new DocumentoRecibido();
            objeto = dr.Ver(id);
            return objeto;
        } catch (Exception e) {
            throw e;
        }

    }

    public Ruta BuscarRuta(int id) throws Exception {
        try {
            RutaAD ru = new RutaAD(conn.Abrir());
            Ruta r = new Ruta();
            r = ru.Ver(id);
            return r;
        } catch (Exception e) {
            throw e;
        }
    }
}
