/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;

import AccesoDatos.Conexion;
import AccesoDatos.PermisoAD;
import AccesoDatos.UnidadTramiteAD;
import AccesoDatos.UsuarioAD;
import Entidades.Permiso;
import Entidades.UnidadTramite;
import Entidades.Usuario;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Carlos Anthony
 */
public class Datos_Registro_Usuario {

    Conexion conn = new Conexion();

    public List<Usuario> MostrarLista() throws Exception {
        try {
            UsuarioAD uni = new UsuarioAD(conn.Abrir());
            List<Entidades.Usuario> lista = new ArrayList<Entidades.Usuario>();
            lista = uni.MostrarLista();

            return lista;
        } catch (Exception e) {
            throw e;
        }

    }
    
     public List<Permiso> MostrarPermisos() throws Exception {
        try {
            PermisoAD per = new PermisoAD(conn.Abrir());
            List<Entidades.Permiso> lista = new ArrayList<Entidades.Permiso>();
            lista = per.MostrarLista();

            return lista;
        } catch (Exception e) {
            throw e;
        }

    }

    public List<UnidadTramite> LlenarCombo() throws Exception {
        try {
            UnidadTramiteAD uni = new UnidadTramiteAD(conn.Abrir());
            List<Entidades.UnidadTramite> lista = new ArrayList<Entidades.UnidadTramite>();
            lista = uni.ListarTodo();

            return lista;
        } catch (Exception e) {
            throw e;
        }

    }

    public boolean InsertarPermiso(Permiso objeto) throws Exception {
        try {
            PermisoAD user = new PermisoAD(conn.Abrir());
            user.Insertar(objeto);
            return true;
        } catch (Exception e) {
            throw e;
        }
    }
    
    public boolean Insertar(Usuario objeto) throws Exception {
        try {
            UsuarioAD user = new UsuarioAD(conn.Abrir());
            user.Insertar(objeto);
            return true;
        } catch (Exception e) {
            throw e;
        }
    }

    public boolean Eliminar(Usuario objeto) throws Exception {
        try {
            UsuarioAD user = new UsuarioAD(conn.Abrir());
            user.Eliminar(objeto);
            return true;
        } catch (Exception e) {
            throw e;
        }
    }

    public boolean Actualizar(Usuario objeto) throws Exception {
        try {
            UsuarioAD user = new UsuarioAD(conn.Abrir());
            user.Actualizar(objeto);
            return true;
        } catch (Exception e) {
            throw e;
        }
    }

}
