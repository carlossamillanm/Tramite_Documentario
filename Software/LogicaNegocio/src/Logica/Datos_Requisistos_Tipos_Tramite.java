/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;

import AccesoDatos.Conexion;
import AccesoDatos.RequisitosAD;
import AccesoDatos.TipoTramiteAD;
import AccesoDatos.UnidadTramiteAD;
import Entidades.Requisitos;
import Entidades.TipoTramite;
import Entidades.UnidadTramite;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Carlos Anthony
 */
public class Datos_Requisistos_Tipos_Tramite {

    Conexion conn = new Conexion();

    public List<Requisitos> MostrarLista() throws Exception {
        try {
            RequisitosAD tt = new RequisitosAD(conn.Abrir());
            List<Entidades.Requisitos> lista = new ArrayList<Entidades.Requisitos>();
            lista = tt.ListarTodo();
            return lista;
        } catch (Exception e) {
            throw e;
        }

    }

    public boolean Insertar(Requisitos objeto) throws Exception {
        try {
            RequisitosAD tt = new RequisitosAD(conn.Abrir());
            tt.Insertar(objeto);
            return true;
        } catch (Exception e) {
            throw e;
        }
    }

    public boolean Actualizar(Requisitos objeto) throws Exception {
        try {
            RequisitosAD tt = new RequisitosAD(conn.Abrir());
            tt.Actualizar(objeto);
            return true;
        } catch (Exception e) {
            throw e;
        }
    }

    public boolean Eliminar(Requisitos objeto) throws Exception {
        try {
            RequisitosAD tt = new RequisitosAD(conn.Abrir());
            tt.Eliminar(objeto);
            return true;
        } catch (Exception e) {
            throw e;
        }
    }

    public List<Requisitos> ListaTipo(Integer id) throws Exception {
        try {
            RequisitosAD tt = new RequisitosAD(conn.Abrir());
            List<Entidades.Requisitos> lista = new ArrayList<Entidades.Requisitos>();
            lista = tt.ListarTipo(id);
            return lista;
        } catch (Exception e) {
            throw e;
        }

    }

    public List<UnidadTramite> LlenarCombotramite() throws Exception {
        try {
            UnidadTramiteAD uni = new UnidadTramiteAD(conn.Abrir());
            List<Entidades.UnidadTramite> lista = new ArrayList<Entidades.UnidadTramite>();
            lista = uni.ListarTodo();
            return lista;
        } catch (Exception e) {
            throw e;
        }

    }

    public List<TipoTramite> LlenarComboTiposTramite(Integer id) throws Exception {
        try {
            TipoTramiteAD tt = new TipoTramiteAD(conn.Abrir());
            List<Entidades.TipoTramite> lista = new ArrayList<Entidades.TipoTramite>();
            lista = tt.TipoTramite(id);
            return lista;
        } catch (Exception e) {
            throw e;
        }

    }
}
