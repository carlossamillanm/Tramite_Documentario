/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Logica;

import AccesoDatos.Conexion;
import AccesoDatos.TipoDocumentoAD;
import Entidades.TipoDocumento;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Carlos Anthony
 */
public class Datos_Tipo_Documento {
    
    Conexion conn = new Conexion();
    
    public List<TipoDocumento> MostrarLista() throws Exception{
        try {
            TipoDocumentoAD tid = new TipoDocumentoAD(conn.Abrir());
            List<Entidades.TipoDocumento> lista = new ArrayList<Entidades.TipoDocumento>();
            lista = tid.ListaTotal();

            return lista;
        } catch (Exception e) {
            throw e;
        }
    }
    
    public boolean Insertar(TipoDocumento objeto) throws Exception{
        try {
            TipoDocumentoAD td=new TipoDocumentoAD(conn.Abrir());
            td.Insertar(objeto);
            return true;
        } catch (Exception e) {
            throw e;
        }
    }
    
    public boolean Actualizar(TipoDocumento objeto) throws Exception{
        try {
            TipoDocumentoAD td=new TipoDocumentoAD(conn.Abrir());
            td.Actualizar(objeto);
            return true;
        } catch (Exception e) {
            throw e;
        }
    }
    
        public boolean Eliminar(TipoDocumento objeto) throws Exception{
        try {
            TipoDocumentoAD td=new TipoDocumentoAD(conn.Abrir());
            td.Eliminar(objeto);
            return true;
        } catch (Exception e) {
            throw e;
        }
    }
}
