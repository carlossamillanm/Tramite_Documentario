/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Logica;

import AccesoDatos.Conexion;
import AccesoDatos.TipoTramiteAD;
import AccesoDatos.UnidadOrganizativaAD;
import AccesoDatos.UnidadTramiteAD;
import Entidades.TipoTramite;
import Entidades.UnidadOrganizativa;
import Entidades.UnidadTramite;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Carlos Anthony
 */
public class Datos_Tipos_Tramite {
    
    Conexion conn = new Conexion();
    
    public List<TipoTramite> MostrarLista() throws Exception {
        try {
            TipoTramiteAD tt = new TipoTramiteAD(conn.Abrir());
            List<Entidades.TipoTramite> lista = new ArrayList<Entidades.TipoTramite>();
            lista = tt.ListarTodo();

            return lista;
        } catch (Exception e) {
            throw e;
        }

    }
        
    public List<UnidadTramite> LlenarCombotramite() throws Exception {
        try {
            UnidadTramiteAD uni = new UnidadTramiteAD(conn.Abrir());
            List<Entidades.UnidadTramite> lista = new ArrayList<Entidades.UnidadTramite>();
            lista = uni.ListarTodo();

            return lista;
        } catch (Exception e) {
            throw e;
        }

    }
    
    public List<UnidadOrganizativa> LlenarComboOrganizacion() throws Exception {
        try {
            UnidadOrganizativaAD uni = new UnidadOrganizativaAD(conn.Abrir());
            List<Entidades.UnidadOrganizativa> lista = new ArrayList<Entidades.UnidadOrganizativa>();
            lista = uni.ListaTotal();
            return lista;
        } catch (Exception e) {
            throw e;
        }

    }
    
    public boolean Insertar(TipoTramite objeto) throws Exception {
        try {
            TipoTramiteAD user = new TipoTramiteAD(conn.Abrir());
            user.Insertar(objeto);
            return true;
        } catch (Exception e) {
            throw e;
        }
    }
    
    public boolean Actualizar(TipoTramite objeto) throws Exception {
        try {
            TipoTramiteAD user = new TipoTramiteAD(conn.Abrir());
            user.Actualizar(objeto);
            return true;
        } catch (Exception e) {
            throw e;
        }
    }
    
    public boolean Eliminar(TipoTramite objeto) throws Exception {
        try {
            TipoTramiteAD user = new TipoTramiteAD(conn.Abrir());
            user.Eliminar(objeto);
            return true;
        } catch (Exception e) {
            throw e;
        }
    }
}
