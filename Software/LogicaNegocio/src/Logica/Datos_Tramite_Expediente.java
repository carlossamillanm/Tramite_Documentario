/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;

import AccesoDatos.Conexion;
import AccesoDatos.DocumentoEmitidoAD;
import AccesoDatos.DocumentoRecibidoAD;
import AccesoDatos.RequisitoConcluidosAD;
import AccesoDatos.RequisitosAD;
import AccesoDatos.RutaAD;
import AccesoDatos.TipoDocumentoAD;
import AccesoDatos.TramiteAD;
import AccesoDatos.UnidadTramiteAD;
import Entidades.DocumentoEmitido;
import Entidades.DocumentoRecibido;
import Entidades.Requisitos;
import Entidades.RequisitosConcluidos;
import Entidades.Ruta;
import Entidades.TipoDocumento;
import Entidades.Tramite;
import Entidades.UnidadTramite;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Carlos Anthony
 */
public class Datos_Tramite_Expediente {

    Conexion conn = new Conexion();

    public DocumentoRecibido Buscar(String id) throws Exception {
        try {
            DocumentoRecibidoAD dr = new DocumentoRecibidoAD(conn.Abrir());
            DocumentoRecibido objeto = new DocumentoRecibido();
            objeto = dr.Ver(id);
            return objeto;
        } catch (Exception e) {
            throw e;
        }

    }
    
    public DocumentoEmitido BuscarEmitido(String id) throws Exception {
        try {
            DocumentoEmitidoAD dr = new DocumentoEmitidoAD(conn.Abrir());
            DocumentoEmitido objeto = new DocumentoEmitido();
            objeto = dr.Ver(id);
            return objeto;
        } catch (Exception e) {
            throw e;
        }

    }
    
     public List<UnidadTramite> LlenarCombo() throws Exception {
        try {
            UnidadTramiteAD uni = new UnidadTramiteAD(conn.Abrir());
            List<Entidades.UnidadTramite> lista = new ArrayList<Entidades.UnidadTramite>();
            lista = uni.ListarTodo();
            return lista;
        } catch (Exception e) {
            throw e;
        }

    }
    
    
    public List<TipoDocumento> MostrarListaDocumentos() throws Exception {
        try {
            TipoDocumentoAD tid = new TipoDocumentoAD(conn.Abrir());
            List<Entidades.TipoDocumento> lista = new ArrayList<Entidades.TipoDocumento>();
            lista = tid.ListaTotal();

            return lista;
        } catch (Exception e) {
            throw e;
        }
    }

    public RequisitosConcluidos Requisitos(int id) throws Exception {
        try {
            RequisitoConcluidosAD rc = new RequisitoConcluidosAD(conn.Abrir());
            RequisitosConcluidos objeto = new RequisitosConcluidos();
            objeto = rc.Ver(id);
            return objeto;
        } catch (Exception e) {
            throw e;
        }
    }

    public List<RequisitosConcluidos> ListaRequisitos(int id) throws Exception {
        try {
            RequisitoConcluidosAD rc = new RequisitoConcluidosAD(conn.Abrir());
            List<Entidades.RequisitosConcluidos> lista = new ArrayList<Entidades.RequisitosConcluidos>();
            lista = rc.Listar(id);
            return lista;
        } catch (Exception e) {
            throw e;
        }

    }

    public List<Requisitos> ListaTipo(Integer id) throws Exception {
        try {
            RequisitosAD tt = new RequisitosAD(conn.Abrir());
            List<Entidades.Requisitos> lista = new ArrayList<Entidades.Requisitos>();
            lista = tt.ListarTipo(id);
            return lista;
        } catch (Exception e) {
            throw e;
        }

    }

    public boolean Insertar(Tramite objeto) throws Exception {
        try {
            TramiteAD t = new TramiteAD(conn.Abrir());
            t.Insertar(objeto);
            return true;
        } catch (Exception e) {
            throw e;
        }
    }

    public boolean InsertarEmitido(DocumentoEmitido Objeto) throws Exception {
        try {
            DocumentoEmitidoAD de = new DocumentoEmitidoAD(conn.Abrir());
            de.Insertar(Objeto);
            return true;
        } catch (Exception e) {
            throw e;
        }
    }

    public boolean InsertarRuta(Ruta objeto) throws Exception {
        try {
            RutaAD r = new RutaAD(conn.Abrir());
            r.Insertar(objeto);
            return true;
        } catch (Exception e) {
            throw e;
        }
    }
    
    public DocumentoEmitido BuscarDoc(String id) throws Exception{
        try {
            DocumentoEmitidoAD dea=new DocumentoEmitidoAD(conn.Abrir());
            DocumentoEmitido de=new DocumentoEmitido();
            de=dea.Ver(id);
            return de;
        } catch (Exception e) {
            throw e;
        }
    }
    
    public Ruta BuscarRuta(int id) throws Exception{
        try {
            RutaAD ru=new RutaAD(conn.Abrir());
            Ruta r=new Ruta();
            r=ru.Ver(id);
            return r;
        } catch (Exception e) {
            throw e;
        }
    }
    
     public boolean Actualizar(DocumentoRecibido objeto) throws Exception {
        try {
            DocumentoRecibidoAD dr = new DocumentoRecibidoAD(conn.Abrir());
            dr.Actualizar(objeto);
            return true;
        } catch (Exception e) {
            throw e;
        }
    }
}
