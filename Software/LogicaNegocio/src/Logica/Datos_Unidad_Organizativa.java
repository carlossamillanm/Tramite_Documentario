package Logica;

import AccesoDatos.Conexion;
import AccesoDatos.UnidadOrganizativaAD;
import Entidades.UnidadOrganizativa;
import java.util.ArrayList;
import java.util.List;

public class Datos_Unidad_Organizativa {

    Conexion conn = new Conexion();

    public List<UnidadOrganizativa> MostrarLista() throws Exception{
        
        try {
            UnidadOrganizativaAD uni = new UnidadOrganizativaAD(conn.Abrir());
            List<Entidades.UnidadOrganizativa> lista = new ArrayList<Entidades.UnidadOrganizativa>();
            lista= uni.ListaTotal();
            
            return lista;
        } catch (Exception e) {
            throw e;
        }
        
    }
    
    public boolean Insertar(UnidadOrganizativa objeto) throws Exception{
        try {
            UnidadOrganizativaAD uni = new UnidadOrganizativaAD(conn.Abrir());
            uni.Insertar(objeto);
            return true;
        } catch (Exception e) {
            throw e;
        }
    }
    
    public boolean Eliminar(UnidadOrganizativa objeto) throws Exception{
        try {
            UnidadOrganizativaAD uni = new UnidadOrganizativaAD(conn.Abrir());
            uni.Eliminar(objeto);
            return true;
        } catch (Exception e) {
            throw e;
        }
    }
    
    public boolean Actualizar(UnidadOrganizativa objeto) throws Exception{
        try {
            UnidadOrganizativaAD uni = new UnidadOrganizativaAD(conn.Abrir());
            uni.Actualizar(objeto);
            return true;
        } catch (Exception e) {
            throw e;
        }
    }

}
