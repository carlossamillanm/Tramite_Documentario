/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;

import AccesoDatos.Conexion;
import AccesoDatos.UnidadOrganizativaAD;
import AccesoDatos.UnidadTramiteAD;
import Entidades.UnidadOrganizativa;
import Entidades.UnidadTramite;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Carlos Anthony
 */
public class Datos_Unidad_Tramite {

    Conexion conn = new Conexion();

    public List<UnidadTramite> MostrarLista() throws Exception {

        try {
            UnidadTramiteAD uni = new UnidadTramiteAD(conn.Abrir());
            List<Entidades.UnidadTramite> lista = new ArrayList<Entidades.UnidadTramite>();
            lista = uni.ListarTodo();

            return lista;
        } catch (Exception e) {
            throw e;
        }

    }

    public List<UnidadOrganizativa> LlenarCombo() throws Exception {
        try {
            UnidadOrganizativaAD uni = new UnidadOrganizativaAD(conn.Abrir());
            List<Entidades.UnidadOrganizativa> lista = new ArrayList<Entidades.UnidadOrganizativa>();
            lista = uni.ListaTotal();
            return lista;
        } catch (Exception e) {
            throw e;
        }

    }

    public boolean Insertar(UnidadTramite objeto) throws Exception {
        try {
            UnidadTramiteAD uni = new UnidadTramiteAD(conn.Abrir());
            uni.Insertar(objeto);
            return true;
        } catch (Exception e) {
            throw e;
        }
    }

    public boolean Eliminar(UnidadTramite objeto) throws Exception {
        try {
            UnidadTramiteAD ut = new UnidadTramiteAD(conn.Abrir());
            ut.Eliminar(objeto);
            return true;
        } catch (Exception e) {
            throw e;
        }
    }

    public boolean Actualizar(UnidadTramite objeto) throws Exception {
        try {
            UnidadTramiteAD uni = new UnidadTramiteAD(conn.Abrir());
            uni.Actualizar(objeto);
            return true;
        } catch (Exception e) {
            throw e;
        }
    }
}
