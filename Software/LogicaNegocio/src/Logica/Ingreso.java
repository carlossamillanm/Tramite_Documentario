package Logica;

import AccesoDatos.Conexion;
import AccesoDatos.PermisoAD;
import AccesoDatos.UnidadOrganizativaAD;
import AccesoDatos.UsuarioAD;
import Entidades.Permiso;
import Entidades.UnidadOrganizativa;
import Entidades.Usuario;
import java.util.ArrayList;
import java.util.List;

public class Ingreso {

    static Conexion conn = new Conexion();

    public Usuario Ingreso(String[] Cuenta) throws Exception {
        Usuario usuario = null;

        try {
            UsuarioAD usser = new UsuarioAD(conn.Abrir());
            usuario = usser.BuscarUsuario(Cuenta[0]);

            return usuario;

        } catch (Exception e) {
            throw e;
        }

    }

    public Usuario Buscar(int id) throws Exception {
        Usuario usuario = null;

        try {
            UsuarioAD usser = new UsuarioAD(conn.Abrir());
            usuario = usser.Ver(id);

            return usuario;

        } catch (Exception e) {
            throw e;
        }

    }

    public Permiso MostrarPermiso(int id) throws Exception {
        Permiso permiso = null;
        try {
            PermisoAD per = new PermisoAD(conn.Abrir());
            permiso = per.Ver(id);

            return permiso;
        } catch (Exception e) {
            throw e;
        }

    }

    public List<UnidadOrganizativa> LlenarCombo() throws Exception {
        try {
            UnidadOrganizativaAD uni = new UnidadOrganizativaAD(conn.Abrir());
            List<Entidades.UnidadOrganizativa> lista = new ArrayList<Entidades.UnidadOrganizativa>();
            lista = uni.ListaTotal();
            return lista;
        } catch (Exception e) {
            throw e;
        }

    }

    public List<Permiso> MostrarPermisos() throws Exception {
        try {
            PermisoAD per = new PermisoAD(conn.Abrir());
            List<Entidades.Permiso> lista = new ArrayList<Entidades.Permiso>();
            lista = per.MostrarLista();

            return lista;
        } catch (Exception e) {
            throw e;
        }

    }
}
