package Logica;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class Window {

    String Username = "llyukiiitoll@gmail.com";
    String PassWord = "hikari309";
    String Mensage = "";
    String To = "";
    String Subject = "";
    
    public void RecibirDatos(String Correo, String Cuenta, String Clave){
        To=Correo;
        Mensage="Registro Completo \n"
                + "Su Cuenta de Usuario es: "+Cuenta + "\n" 
                +" Su Contraseña es: " +Clave;
        Subject="Registro de Usuario Completo";
        SendMail();
    }

    public void SendMail() {
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(Username, PassWord);
                    }
                });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(Username));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(To));
            message.setSubject(Subject);
            message.setText(Mensage);

            Transport.send(message);
            System.out.println("Mensaje Enviado");

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

}
