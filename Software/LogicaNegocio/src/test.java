
import AccesoDatos.Conexion;
import AccesoDatos.PermisoAD;
import AccesoDatos.UnidadOrganizativaAD;
import AccesoDatos.UnidadTramiteAD;
import AccesoDatos.UsuarioAD;
import Entidades.Permiso;
import Entidades.UnidadOrganizativa;
import Entidades.UnidadTramite;
import Entidades.Usuario;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Carlos Anthony
 */
public class test {

    public static void main(String[] args) {
        Conexion conn = new Conexion();

        try {
            /*Agregando un usuario
             UsuarioAD usuario = new UsuarioAD(conn.Abrir());
             usuario.Insertar(new Usuario(null,"Estela","Espinoza", "Jacky","jacki_amix@hotmail.com", "jackycita","123456", "A", "G"));
             */

            /* Para una unidad organizativa
             UnidadOrganizativaAD usuario = new UnidadOrganizativaAD(conn.Abrir());
             usuario.Insertar(new UnidadOrganizativa(null,"Tramite", "Tra","A"));
             */
            /*Para una unidad Tramite
             UnidadTramiteAD usuario = new UnidadTramiteAD(conn.Abrir());

             usuario.Insertar(new UnidadTramite(null,"Transaccion", "Trs","Brenda", "A",new UnidadOrganizativa(1)));
             List<UnidadTramite> lista =usuario.ListarNombre("Transaccion");
            
             for (int i = 0; i < lista.size(); i++) {
             System.out.println(lista.get(i).getId());
             System.out.println(lista.get(i).getNombre());
             System.out.println(lista.get(i).getResponsable());
             System.out.println(lista.get(i).getEstado());
             System.out.println(lista.get(i).getUnidadorganizativa().getId());
             }
             
             UnidadTramite lista = usuario.Ver(1);
             System.out.println(lista.getId());
             System.out.println(lista.getNombre());
             System.out.println(lista.getResponsable());
             System.out.println(lista.getEstado());
             System.out.println(lista.getUnidadorganizativa().getId());
             System.out.println(lista.getUnidadorganizativa().getNombre());
            
            
             usuario.Actualizar(new UnidadTramite(1,"Servicio","Serv","Aneli","A",new UnidadOrganizativa(1)));
             */
            /*
             Para Permisos
             
             PermisoAD usuario = new PermisoAD(conn.Abrir());
            
             usuario.Insertar(new Permiso(null, null, null,"T",new Usuario(1),new UnidadOrganizativa(1), null));
             
             List<Permiso> lista=usuario.ListarTipo("T");
             for (int i = 0; i < lista.size(); i++) {
                
             System.out.println(lista.get(i).getId());
             System.out.println(lista.get(i).getFechainicio());
             System.out.println(lista.get(i).getFechafin());
             System.out.println(lista.get(i).getUsuario().getNombre());
             System.out.println(lista.get(i).getUnidadTramite().getNombre());
             System.out.println(lista.get(i).getUnidadOrganizativa().getNombre());  
             }
            
             Permiso lista = usuario.Ver(1);
            
             System.out.println(lista.getId());
             System.out.println(lista.getFechainicio());
             System.out.println(lista.getFechafin());
             System.out.println(lista.getUsuario().getNombre());
             System.out.println(lista.getUnidadTramite().getNombre());
             System.out.println(lista.getUnidadOrganizativa().getNombre());  
            
             usuario.Eliminar(new Permiso(2));
             */
            System.out.println(" salio :D");
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
