/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfases;

import Entidades.DocumentoRecibido;
import Entidades.RequisitosConcluidos;
import Entidades.RequisitosRecibidos;
import Entidades.Ruta;
import Global.Global;
import Logica.Datos_Expediente_Curso;
import Logica.Datos_Tramite_Expediente;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;
import mdl.mdlExpedienteCurso;
import mdl.mdlTramiteExpediente;

/**
 *
 * @author Carlos Anthony
 */
public class Documento_Recibidos_Detalles extends javax.swing.JFrame {

    String fecha;
    String Traslado = "";
    int Id;
    int tipdoc;
    int a=0;

    /**
     * Creates new form Documento_Recibidos_Detalles
     */
    public Documento_Recibidos_Detalles() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDesktopPane1 = new javax.swing.JDesktopPane();
        jLabel1 = new javax.swing.JLabel();
        jtUnidadTramite = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        lbNumExp = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        lbTipoDoc = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        lbNumDoc = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        lbDe = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        lbAsunto = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        lbFechResp = new javax.swing.JLabel();
        lbTiemEst = new javax.swing.JLabel();
        lbDescripcion = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jPanel4 = new javax.swing.JPanel();
        lbObservacion = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        lbDescri = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        lbTraslado = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        lbDoc = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jLabel7 = new javax.swing.JLabel();
        jbCerrar = new javax.swing.JButton();
        lbEstado = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jDesktopPane1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Unidad de Tramite: ");
        jDesktopPane1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 10, -1, -1));

        jtUnidadTramite.setText("jTextField1");
        jDesktopPane1.add(jtUnidadTramite, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 10, 550, -1));

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Documento"));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel3.setText("N° de Expediente: ");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, -1, -1));

        lbNumExp.setText("jLabel4");
        jPanel1.add(lbNumExp, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 20, 190, -1));

        jLabel4.setText("TIpo de Documento: ");
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 40, -1, -1));

        lbTipoDoc.setText("jLabel5");
        jPanel1.add(lbTipoDoc, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 40, -1, -1));

        jLabel5.setText("Numero de Documento: ");
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 60, -1, -1));

        lbNumDoc.setText("jLabel6");
        jPanel1.add(lbNumDoc, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 60, -1, -1));

        jLabel6.setText("De: ");
        jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 80, -1, -1));

        lbDe.setText("jLabel10");
        jPanel1.add(lbDe, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 80, -1, -1));

        jLabel10.setText("Asunto:");
        jPanel1.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 20, -1, -1));

        lbAsunto.setText("jLabel11");
        jPanel1.add(lbAsunto, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 40, 240, 50));

        jDesktopPane1.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 50, 680, 110));

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Tipo de Tramite"));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel11.setText("Descripcion: ");
        jPanel2.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, -1, -1));

        jLabel12.setText("Tiempo Estimado: ");
        jPanel2.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 50, -1, -1));

        jLabel13.setText("Fecha de Respuesta: ");
        jPanel2.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 80, -1, -1));

        lbFechResp.setText("jLabel14");
        jPanel2.add(lbFechResp, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 80, -1, -1));

        lbTiemEst.setText("jLabel14");
        jPanel2.add(lbTiemEst, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 50, -1, -1));

        lbDescripcion.setText("jLabel14");
        jPanel2.add(lbDescripcion, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 20, -1, -1));

        jDesktopPane1.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 170, 300, 110));

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Requisitos"));
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(jTable1);

        jPanel3.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 20, 320, 80));

        jDesktopPane1.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 170, 370, 110));

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("Observacion"));
        jPanel4.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbObservacion.setText("jLabel14");
        jPanel4.add(lbObservacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 30, -1, -1));

        jDesktopPane1.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 290, 680, 60));

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder("Ruta"));
        jPanel5.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel6.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 1, 1, java.awt.SystemColor.textHighlight));
        jPanel6.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel8.setText("Tramite de Expediente");
        jPanel6.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(7, 1, -1, -1));

        jLabel9.setText("Descripcion: ");
        jPanel6.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, -1, -1));

        lbDescri.setText("jLabel14");
        jPanel6.add(lbDescri, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 20, 480, -1));

        jLabel14.setText("Traslado: ");
        jPanel6.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 40, -1, -1));

        lbTraslado.setText("jLabel15");
        jPanel6.add(lbTraslado, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 40, 480, -1));

        jLabel15.setText("Documento: ");
        jPanel6.add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 60, -1, -1));

        lbDoc.setText("jLabel16");
        jPanel6.add(lbDoc, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 60, 480, -1));

        jPanel5.add(jPanel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 110, 580, 90));

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jTable2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable2MouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(jTable2);

        jPanel5.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, 570, 90));

        jDesktopPane1.add(jPanel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 360, 680, 210));

        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Estado: ");
        jDesktopPane1.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 580, 50, -1));

        jbCerrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/stop-stop-desactivar-la-salida-icono-5982-96.png"))); // NOI18N
        jbCerrar.setText("Cerrar");
        jbCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbCerrarActionPerformed(evt);
            }
        });
        jDesktopPane1.add(jbCerrar, new org.netbeans.lib.awtextra.AbsoluteConstraints(559, 590, 120, 35));

        lbEstado.setForeground(new java.awt.Color(255, 255, 255));
        lbEstado.setText("jLabel16");
        jDesktopPane1.add(lbEstado, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 580, 150, -1));

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/franjas-azules,-circulos,-fondo-azul-199896.jpg"))); // NOI18N
        jDesktopPane1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(-10, 0, 720, 660));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jDesktopPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 700, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jDesktopPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 639, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jbCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbCerrarActionPerformed
        if (JOptionPane.showConfirmDialog(null, "¿Esta seguro que desea cerrar?") == 0) {
            this.dispose();
        }
    }//GEN-LAST:event_jbCerrarActionPerformed

    private void jTable2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable2MouseClicked
        try {
            Datos_Expediente_Curso dec = new Datos_Expediente_Curso();
            List<Ruta> lista = new ArrayList<Ruta>();
            lista = dec.BuscarRutas(this.lbNumExp.getText());
            if (this.jTable2.getRowCount() > 0) {
                if (this.jTable2.getSelectedRow() != -1) {
                    this.lbDescri.setText(lista.get(this.jTable2.getSelectedRow()).getDocumentoRecibido().getObservacion());
                    this.lbTraslado.setText(Global.permiso.get(0).getUnidadOrganizativa().getNombre() + " - " + Global.permiso.get(0).getUnidadTramite().getNombre());
                    this.lbDoc.setText(lista.get(this.jTable2.getSelectedRow()).getDocumentoRecibido().getTipoDocumento().getNombre() + "/" + lista.get(this.jTable2.getSelectedRow()).getDocumentoRecibido().getNum() + "/" + lista.get(this.jTable2.getSelectedRow()).getDocumentoRecibido().getAñoExpediente());
                } else {
                    this.lbDescri.setText(lista.get(0).getDocumentoRecibido().getObservacion());
                    this.lbTraslado.setText(Global.permiso.get(0).getUnidadOrganizativa().getNombre() + " - " + Global.permiso.get(0).getUnidadTramite().getNombre());
                    this.lbDoc.setText(lista.get(0).getDocumentoRecibido().getTipoDocumento().getNombre() + "/" + lista.get(0).getDocumentoRecibido().getNum() + "/" + lista.get(0).getDocumentoRecibido().getAñoExpediente());
                }
            } else {
                this.lbDescri.setText("");
                this.lbTraslado.setText("");
                this.lbDoc.setText("");
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }//GEN-LAST:event_jTable2MouseClicked

    public void Busqueda(String Busqueda) {
        try {
            Limpiar();
            Buscar(Busqueda);
            this.lbEstado.setText("");
            Datos_Expediente_Curso dec = new Datos_Expediente_Curso();
            List<Ruta> lista = new ArrayList<Ruta>();
            lista = dec.BuscarRutas(this.lbNumExp.getText());
            this.jTable2.setModel(new mdlExpedienteCurso(lista));
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    public void Limpiar() {
        this.lbDescri.setText("");
        this.lbTraslado.setText("");
        this.lbDoc.setText("");
        this.lbDescripcion.setText("");
        this.lbTiemEst.setText("");
        this.lbFechResp.setText("");
        
    }

    public void respuesta(int adelanto) {
        int dia = new Date().getDate();
        int mes = new Date().getMonth() + 1;
        int año = 1900 + new Date().getYear();
        int calendario[][] = new int[12][3];
        for (int i = 0; i < 10; i++) {
            calendario[i][0] = i + 1;
        }
        calendario[0][1] = 31;
        calendario[1][1] = 28;
        calendario[2][1] = 31;
        calendario[3][1] = 30;
        calendario[4][1] = 31;
        calendario[5][1] = 30;
        calendario[6][1] = 31;
        calendario[7][1] = 31;
        calendario[8][1] = 30;
        calendario[9][1] = 31;
        calendario[10][1] = 30;
        calendario[11][1] = 31;

        if (mes == 2) {
            if (año % 4 == 0) {
                if ((dia + adelanto) > (calendario[mes - 1][1] + 1)) {
                    dia = dia + adelanto - calendario[mes - 1][1] + 1;
                    mes = mes + 1;
                    this.lbFechResp.setText(dia + "/" + mes + "/" + año);
                } else {
                    dia = dia + adelanto;
                    this.lbFechResp.setText(dia + "/" + mes + "/" + año);
                }
            }
        } else {
            if ((dia + adelanto) > calendario[mes - 1][1]) {
                if (mes + 1 > 12) {
                    dia = dia + adelanto - calendario[mes - 1][1];
                    mes = 1;
                    año = año + 1;
                    this.lbFechResp.setText(dia + "/" + mes + "/" + año);
                } else {
                    dia = dia + adelanto - calendario[mes - 1][1];
                    mes = mes + 1;
                    this.lbFechResp.setText(dia + "/" + mes + "/" + año);
                }
            } else {
                dia = dia + adelanto;
                this.lbFechResp.setText(dia + "/" + mes + "/" + año);
            }
        }

    }

    public void Buscar(String Num) {
       Datos_Tramite_Expediente dte = new Datos_Tramite_Expediente();
        DocumentoRecibido dr = new DocumentoRecibido();
        RequisitosConcluidos rc = new RequisitosConcluidos();
        List<Entidades.Requisitos> lista = new ArrayList<Entidades.Requisitos>();
        List<Entidades.RequisitosConcluidos> listaRequisitos = new ArrayList<Entidades.RequisitosConcluidos>();
        List<Entidades.RequisitosRecibidos> listaRecibido = new ArrayList<Entidades.RequisitosRecibidos>();
        try {
            dr = dte.Buscar(Num);
            Id = dr.getId();
            rc = dte.Requisitos(dr.getId());
            if (rc != null) {
                a=1;
                lista = dte.ListaTipo(rc.getRequisitos().getTipoTramite().getId());
                listaRequisitos = dte.ListaRequisitos(dr.getId());
            }
            this.lbNumExp.setText(dr.getNumExpediente());
            this.lbTipoDoc.setText(dr.getTipoDocumento().getNombre());
            tipdoc = dr.getTipoDocumento().getId();
            this.lbNumDoc.setText(dr.getNum());
            this.lbDe.setText(dr.getDe());
            this.lbAsunto.setText(dr.getAsunto());
            this.lbObservacion.setText(dr.getObservacion());
            if (rc != null) {
                a=1;
                this.lbDescripcion.setText(rc.getRequisitos().getTipoTramite().getNombre());
                this.lbTiemEst.setText(rc.getRequisitos().getTipoTramite().getTiempoEstimado().toString());
                respuesta(rc.getRequisitos().getTipoTramite().getTiempoEstimado());
                boolean a;
                for (int i = 0; i < lista.size(); i++) {
                    a = false;
                    for (int j = 0; j < listaRequisitos.size() && a == false; j++) {
                        if (lista.get(i).getDescripcion().equals(listaRequisitos.get(j).getRequisitos().getDescripcion())) {
                            RequisitosRecibidos rr = new RequisitosRecibidos(lista.get(i).getDescripcion(), "Si");
                            listaRecibido.add(rr);
                            a = true;
                            rr = null;
                        }
                    }
                    if (a == false) {
                        RequisitosRecibidos rr = new RequisitosRecibidos(lista.get(i).getDescripcion(), "No");
                        listaRecibido.add(rr);
                        rr = null;
                    }
                }
                this.jTable1.setModel(new mdlTramiteExpediente(listaRecibido));
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "No se a encontrado ningun resultado");
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Documento_Recibidos_Detalles.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Documento_Recibidos_Detalles.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Documento_Recibidos_Detalles.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Documento_Recibidos_Detalles.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Documento_Recibidos_Detalles().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JDesktopPane jDesktopPane1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTable2;
    private javax.swing.JButton jbCerrar;
    private javax.swing.JTextField jtUnidadTramite;
    private javax.swing.JLabel lbAsunto;
    private javax.swing.JLabel lbDe;
    private javax.swing.JLabel lbDescri;
    private javax.swing.JLabel lbDescripcion;
    private javax.swing.JLabel lbDoc;
    private javax.swing.JLabel lbEstado;
    private javax.swing.JLabel lbFechResp;
    private javax.swing.JLabel lbNumDoc;
    private javax.swing.JLabel lbNumExp;
    private javax.swing.JLabel lbObservacion;
    private javax.swing.JLabel lbTiemEst;
    private javax.swing.JLabel lbTipoDoc;
    private javax.swing.JLabel lbTraslado;
    // End of variables declaration//GEN-END:variables
}
