/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfases;

import Entidades.DocumentoEmitido;
import Entidades.DocumentoRecibido;
import Entidades.TipoTramite;
import Entidades.Tramite;
import Global.Global;
import Logica.Datos_Grafico_General;
import Logica.Datos_Requisistos_Tipos_Tramite;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

/**
 *
 * @author Carlos Anthony
 */
public class Grafico_UnidadTramite extends javax.swing.JFrame {

    String idtipo[][] = new String[1000][3];
    int Contador = 0;
    int dia;

    /**
     * Creates new form Grafico_UnidadTramite
     */
    public Grafico_UnidadTramite() {
        initComponents();
    }

    public void graficar2(TipoTramite objeto) {
        try {
            XYSeries series1 = new XYSeries("Documentos por dias");

            series1.add(0, 0);
            series1.add(1, objeto.getTiempoEstimado());

            XYSeriesCollection dataset = new XYSeriesCollection();
            dataset.addSeries(series1);

            JFreeChart chart = ChartFactory.createXYLineChart(
                    "Duracion de Tramite", // Título
                    "Tramite", // Etiqueta Coordenada X
                    "Dias", // Etiqueta Coordenada Y
                    dataset, // Datos
                    PlotOrientation.VERTICAL,
                    true, // Muestra la leyenda de los productos (Producto A)
                    false,
                    false
            );
            // Mostramos la grafica en pantalla
            ChartFrame frame = new ChartFrame("Ejemplo Grafica Lineal", chart);
            frame.pack();
            frame.setVisible(true);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }

    }

  public void graficar1(List<DocumentoEmitido> lista) {
        try {
            int cuenta = 0;
            boolean validar = true;
            int Grafico[][] = new int[31][2];
            int contar = 0;
            XYSeries series1 = new XYSeries("Documentos por dias");

            Grafico[0][0] = Integer.parseInt(lista.get(0).getFechaEmision().toString().substring(8, 10));
            for (int i = 0; i < lista.size(); i++) {
                if (i == 0) {
                    for (int j = 0; j < lista.size(); j++) {
                        if (Grafico[cuenta][0] == Integer.parseInt(lista.get(0).getFechaEmision().toString().substring(8, 10))) {
                            contar++;
                        }
                    }
                    Grafico[cuenta][1] = contar;
                    cuenta++;
                } else {
                    for (int j = 0; j < cuenta; j++) {
                        if (Grafico[j][0] == Integer.parseInt(lista.get(i).getFechaEmision().toString().substring(8, 10))) {
                            validar = false;
                        }
                    }
                    if (validar == true) {
                        Grafico[cuenta][0] = Integer.parseInt(lista.get(i).getFechaEmision().toString().substring(8, 10));
                        for (int j = 0; j < lista.size(); j++) {
                            if (Grafico[cuenta][0] == Integer.parseInt(lista.get(j).getFechaEmision().toString().substring(8, 10))) {
                                contar++;
                            }
                        }
                        Grafico[cuenta][1] = contar;
                        cuenta++;
                    }
                }
                validar = true;
                contar = 0;
            }
            series1.add(0, 0);
            for (int i = 0; i < cuenta; i++) {
                System.out.println(Grafico[i][0] + "//" + Grafico[i][1]);
            }
            for (int i = 0; i < cuenta; i++) {
                series1.add(Grafico[i][0], Grafico[i][1]);
            }

            XYSeriesCollection dataset = new XYSeriesCollection();
            dataset.addSeries(series1);

            JFreeChart chart = ChartFactory.createXYLineChart(
                    "Documentos Emitidos", // Título
                    "Dias", // Etiqueta Coordenada X
                    "N° de Documento", // Etiqueta Coordenada Y
                    dataset, // Datos
                    PlotOrientation.VERTICAL,
                    true, // Muestra la leyenda de los productos (Producto A)
                    false,
                    false
            );
            // Mostramos la grafica en pantalla
            ChartFrame frame = new ChartFrame("Ejemplo Grafica Lineal", chart);
            frame.pack();
            frame.setVisible(true);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }

    }

   public void graficar(List<DocumentoRecibido> lista) {
        try {
            int cuenta = 0;
            boolean validar = true;
            int Grafico[][] = new int[31][2];
            int contar = 0;
            XYSeries series1 = new XYSeries("Documentos por dias");

                        Grafico[0][0] = Integer.parseInt(lista.get(0).getAñoExpediente().toString().substring(8, 10));
            for (int i = 0; i < lista.size(); i++) {
                if (i == 0) {
                    for (int j = 0; j < lista.size(); j++) {
                        if (Grafico[cuenta][0] == Integer.parseInt(lista.get(0).getAñoExpediente().toString().substring(8, 10))) {
                            contar++;
                        }
                    }
                    Grafico[cuenta][1] = contar;
                    cuenta++;
                } else {
                    for (int j = 0; j < cuenta; j++) {
                        if (Grafico[j][0] == Integer.parseInt(lista.get(i).getAñoExpediente().toString().substring(8, 10))) {
                            validar = false;
                        }
                    }
                    if (validar == true) {
                        Grafico[cuenta][0] = Integer.parseInt(lista.get(i).getAñoExpediente().toString().substring(8, 10));
                        for (int j = 0; j < lista.size(); j++) {
                            if (Grafico[cuenta][0] == Integer.parseInt(lista.get(j).getAñoExpediente().toString().substring(8, 10))) {
                                contar++;
                            }
                        }
                        Grafico[cuenta][1] = contar;
                        cuenta++;
                    }
                }
                validar = true;
                contar = 0;
            }
            series1.add(0, 0);
            for (int i = 0; i < cuenta; i++) {
                series1.add(Grafico[i][0], Grafico[i][1]);
            }

            XYSeriesCollection dataset = new XYSeriesCollection();
            dataset.addSeries(series1);

            JFreeChart chart = ChartFactory.createXYLineChart(
                    "Documentos Recibidos", // Título
                    "Dias", // Etiqueta Coordenada X
                    "N° de Documento", // Etiqueta Coordenada Y
                    dataset, // Datos
                    PlotOrientation.VERTICAL,
                    true, // Muestra la leyenda de los productos (Producto A)
                    false,
                    false
            );
            // Mostramos la grafica en pantalla
            ChartFrame frame = new ChartFrame("Ejemplo Grafica Lineal", chart);
            frame.pack();
            frame.setVisible(true);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }

    }

    public void Limpiar() {
        this.jtAño.setText("");
        this.jtUnidadTramite.setText("");

        this.jtUnidadTramite.setText(Global.permiso.get(0).getUnidadOrganizativa().getNombre() + "-" + Global.permiso.get(0).getUnidadTramite().getNombre());
        this.jtUnidadTramite.setEditable(false);

        llenartipotramite(Global.permiso.get(0).getUnidadTramite().getId());

    }

    public void llenartipotramite(int id) {
        int Cuenta = 0;
        Datos_Requisistos_Tipos_Tramite drtt = new Datos_Requisistos_Tipos_Tramite();
        List<Entidades.TipoTramite> lista = new ArrayList<Entidades.TipoTramite>();
        try {
            lista = drtt.LlenarComboTiposTramite(id);
            for (int i = 0; i < lista.size(); i++) {
                this.jcbTipoTramite.addItem(lista.get(i).getNombre());
                idtipo[Cuenta][0] = Integer.toString(lista.get(i).getId());
                idtipo[Cuenta][1] = lista.get(i).getNombre();
                Cuenta++;
            }
        } catch (Exception e) {
            System.out.println(e + " c");
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDesktopPane1 = new javax.swing.JDesktopPane();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jtAño = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jComboBox2 = new javax.swing.JComboBox();
        jPanel1 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jbVerGrafico = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jbVerGrafica1 = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jbVerGrafico2 = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        jcbTipoTramite = new javax.swing.JComboBox();
        jtUnidadTramite = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jLabel11 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);

        jDesktopPane1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Unidad de Tramite: ");
        jDesktopPane1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 10, -1, -1));

        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Año: ");
        jDesktopPane1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 10, -1, -1));

        jtAño.setText("jTextField1");
        jDesktopPane1.add(jtAño, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 10, 80, -1));

        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Mes: ");
        jDesktopPane1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 10, -1, -1));

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" }));
        jDesktopPane1.add(jComboBox1, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 10, 110, -1));

        jLabel4.setText("Unidad de Tramite: ");
        jDesktopPane1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 10, -1, -1));

        jLabel5.setText("Año: ");
        jDesktopPane1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 10, -1, -1));

        jLabel6.setText("Mes: ");
        jDesktopPane1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 10, -1, -1));

        jComboBox2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" }));
        jDesktopPane1.add(jComboBox2, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 10, 110, -1));

        jLabel8.setText("Documento Recibidos");

        jbVerGrafico.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/ver.png"))); // NOI18N
        jbVerGrafico.setText("Ver Grafico");
        jbVerGrafico.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbVerGraficoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jbVerGrafico)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(9, 9, 9)
                        .addComponent(jLabel8)))
                .addContainerGap(22, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jbVerGrafico)
                .addGap(29, 29, 29))
        );

        jDesktopPane1.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 60, 170, 90));

        jLabel9.setText("Documento Emitidos");

        jbVerGrafica1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/ver.png"))); // NOI18N
        jbVerGrafica1.setText("Ver Grafico");
        jbVerGrafica1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbVerGrafica1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addComponent(jbVerGrafica1))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(36, 36, 36)
                        .addComponent(jLabel9)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jbVerGrafica1)
                .addGap(26, 26, 26))
        );

        jDesktopPane1.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 60, 160, 90));

        jLabel10.setText("Tipo de Tramite");

        jbVerGrafico2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/ver.png"))); // NOI18N
        jbVerGrafico2.setText("Ver Grafico");
        jbVerGrafico2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbVerGrafico2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addComponent(jbVerGrafico2)
                .addContainerGap(16, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel10)
                .addGap(40, 40, 40))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel10)
                .addGap(8, 8, 8)
                .addComponent(jbVerGrafico2)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jDesktopPane1.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 210, 160, -1));

        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Tipo de Tramite: ");
        jDesktopPane1.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 180, -1, -1));

        jDesktopPane1.add(jcbTipoTramite, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 170, 460, -1));

        jtUnidadTramite.setText("jTextField1");
        jDesktopPane1.add(jtUnidadTramite, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 10, 230, -1));

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/stop-stop-desactivar-la-salida-icono-5982-96.png"))); // NOI18N
        jButton1.setText("Cerrar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jDesktopPane1.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 290, 120, 35));

        jLabel11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/franjas-azules,-circulos,-fondo-azul-199896.jpg"))); // NOI18N
        jDesktopPane1.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 630, 360));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jDesktopPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 626, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jDesktopPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 336, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jbVerGrafico2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbVerGrafico2ActionPerformed
        try {
            Datos_Grafico_General dgg = new Datos_Grafico_General();
            TipoTramite objeto = new TipoTramite();
            objeto = dgg.MostrarLista(Integer.parseInt(idtipo[this.jcbTipoTramite.getSelectedIndex()][0]));
            graficar2(objeto);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }//GEN-LAST:event_jbVerGrafico2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        this.dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jbVerGraficoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbVerGraficoActionPerformed
        if (!this.jtAño.getText().equals("") && validar(this.jtAño.getText())==true) {
            if (this.jComboBox1.getSelectedIndex() + 1 == 1 || this.jComboBox1.getSelectedIndex() + 1 == 3 || this.jComboBox1.getSelectedIndex() + 1 == 5 || this.jComboBox1.getSelectedIndex() + 1 == 7 || this.jComboBox1.getSelectedIndex() + 1 == 8 || this.jComboBox1.getSelectedIndex() + 1 == 10 || this.jComboBox1.getSelectedIndex() + 1 == 12) {
                dia = 31;
            } else {
                if (this.jComboBox1.getSelectedIndex() + 1 == 4 || this.jComboBox1.getSelectedIndex() + 1 == 6 || this.jComboBox1.getSelectedIndex() + 1 == 9 || this.jComboBox1.getSelectedIndex() + 1 == 11) {
                    dia = 30;
                } else {
                    if (Integer.parseInt(this.jtAño.getText()) % 2 == 0) {
                        dia = 29;
                    } else {
                        dia = 28;
                    }
                }
            }
        }
        try {
            Datos_Grafico_General dgg = new Datos_Grafico_General();
            List<DocumentoRecibido> listad = new ArrayList<DocumentoRecibido>();
            listad = dgg.BuscarFechasRecibidos(this.jtAño.getText() + "-" + (this.jComboBox1.getSelectedIndex() + 1) + "-1", this.jtAño.getText() + "-" + (this.jComboBox1.getSelectedIndex() + 1) + "-" + dia);
            graficar(listad);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }//GEN-LAST:event_jbVerGraficoActionPerformed

    private void jbVerGrafica1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbVerGrafica1ActionPerformed
                if (!this.jtAño.getText().equals("") && validar(this.jtAño.getText())) {
            if (this.jComboBox1.getSelectedIndex() + 1 == 1 || this.jComboBox1.getSelectedIndex() + 1 == 3 || this.jComboBox1.getSelectedIndex() + 1 == 5 || this.jComboBox1.getSelectedIndex() + 1 == 7 || this.jComboBox1.getSelectedIndex() + 1 == 8 || this.jComboBox1.getSelectedIndex() + 1 == 10 || this.jComboBox1.getSelectedIndex() + 1 == 12) {
                dia = 31;
            } else {
                if (this.jComboBox1.getSelectedIndex() + 1 == 4 || this.jComboBox1.getSelectedIndex() + 1 == 6 || this.jComboBox1.getSelectedIndex() + 1 == 9 || this.jComboBox1.getSelectedIndex() + 1 == 11) {
                    dia = 30;
                } else {
                    if (Integer.parseInt(this.jtAño.getText()) % 2 == 0) {
                        dia = 29;
                    } else {
                        dia = 28;
                    }
                }
            }
            try {
                Datos_Grafico_General dgg = new Datos_Grafico_General();
                List<DocumentoEmitido> listat = new ArrayList<DocumentoEmitido>();
                listat = dgg.BuscarFechasEmitidos(this.jtAño.getText() + "-" + (this.jComboBox1.getSelectedIndex() + 1) + "-1", this.jtAño.getText() + "-" + (this.jComboBox1.getSelectedIndex() + 1) + "-" + dia);
                graficar1(listat);
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Debe Ingresar el año");
        }
    }//GEN-LAST:event_jbVerGrafica1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Grafico_UnidadTramite.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Grafico_UnidadTramite.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Grafico_UnidadTramite.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Grafico_UnidadTramite.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Grafico_UnidadTramite().setVisible(true);
            }
        });
    }
    
    public boolean validar(String dato){
        try {
            int a=Integer.parseInt(dato);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JComboBox jComboBox2;
    private javax.swing.JDesktopPane jDesktopPane1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JButton jbVerGrafica1;
    private javax.swing.JButton jbVerGrafico;
    private javax.swing.JButton jbVerGrafico2;
    private javax.swing.JComboBox jcbTipoTramite;
    private javax.swing.JTextField jtAño;
    private javax.swing.JTextField jtUnidadTramite;
    // End of variables declaration//GEN-END:variables
}
