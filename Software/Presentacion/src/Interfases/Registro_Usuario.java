package Interfases;

import Entidades.Permiso;
import Entidades.UnidadOrganizativa;
import Entidades.UnidadTramite;
import Entidades.Usuario;
import Logica.Datos_Registro_Usuario;
import Logica.Window;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import mdl.mdlRegistroUsuarioLista;

public class Registro_Usuario extends javax.swing.JFrame {

    int nuevo;
    String idtramite[][] = new String[1000][3];
    int seleccion;
    String Claveverdadera;
    String CuentaUsuario;
    String CorreoUsuario;
    int contador = 0;

    /**
     * Creates new form Registro_Usuario
     */
    public Registro_Usuario() {
        initComponents();
    }

    Thread hilos = new Thread() {
        public void run() {
            try {
                Window ventana = new Window();
                ventana.RecibirDatos(CorreoUsuario, CuentaUsuario, Claveverdadera);
            } catch (Exception e) {
                System.out.println(e);
            }
        }
    };

    public void Buscar() {
        Datos_Registro_Usuario dru = new Datos_Registro_Usuario();
        List<Entidades.Usuario> lista = new ArrayList<Entidades.Usuario>();
        try {
            lista = dru.MostrarLista();
            jtUsuarios.setModel(new mdlRegistroUsuarioLista(lista));
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, e.getMessage(), "Mensaje del Sistema", JOptionPane.WARNING_MESSAGE);
        }
    }

    public void llenarcombo() {
        if (contador == 0) {
            Datos_Registro_Usuario dru = new Datos_Registro_Usuario();
            List<Entidades.UnidadTramite> lista = new ArrayList<Entidades.UnidadTramite>();
            try {
                lista = dru.LlenarCombo();
                for (int i = 0; i < lista.size(); i++) {
                    this.jcbUnidadTramite.addItem(lista.get(i).getNombre());
                    idtramite[i][0] = lista.get(i).getId().toString();
                    idtramite[i][1] = lista.get(i).getUnidadorganizativa().getId().toString();
                    idtramite[i][2] = lista.get(i).getNombre();
                }
            } catch (Exception e) {
                System.out.println("error");
            }
        }
    }

    public void limpiar() {
        this.jtApeMat.setText("");
        this.jtApePat.setText("");
        this.jtNom.setText("");
        this.jtCorr.setText("");
        this.jtId.setEditable(false);
        this.jdFechaInicio.setDate(null);
        this.jdFechaFin.setDate(null);
    }

    public void editar() {
        this.jtApeMat.setEditable(true);
        this.jtApePat.setEditable(true);
        this.jtNom.setEditable(true);
        this.jtCorr.setEditable(true);
        this.jcbTipo.setEnabled(true);
        this.jcbUnidadTramite.setEnabled(true);
        this.jdFechaInicio.getCalendarButton().setEnabled(true);
        this.jdFechaFin.getCalendarButton().setEnabled(true);
    }

    public void editarparcial() {
        this.jtApeMat.setEditable(true);
        this.jtApePat.setEditable(true);
        this.jtNom.setEditable(true);
        this.jtCorr.setEditable(true);
        this.jcbTipo.setEnabled(true);

    }

    public void noeditar() {
        this.jtApeMat.setEditable(false);
        this.jtApePat.setEditable(false);
        this.jtNom.setEditable(false);
        this.jtCorr.setEditable(false);
        this.jcbTipo.setEnabled(false);
        this.jcbUnidadTramite.setEnabled(false);
        this.jdFechaInicio.setEnabled(false);
        this.jdFechaFin.setEnabled(false);
    }

    public int PonerId(List<Usuario> lista) {
        int mayor = 0;
        mayor = lista.get(0).getId();
        for (int i = 0; i < lista.size(); i++) {
            for (int j = i + 1; j < lista.size(); j++) {
                if (mayor <= lista.get(j).getId()) {
                    mayor = lista.get(j).getId();
                }
            }
        }
        return mayor;
    }

    public void inicializar() {
        nuevo = 0;
        Datos_Registro_Usuario dru = new Datos_Registro_Usuario();
        List<Entidades.Usuario> lista = new ArrayList<Entidades.Usuario>();
        try {
            noeditar();
            limpiar();
            Buscar();
            llenarcombo();
            lista = dru.MostrarLista();
            this.jtId.setText(Integer.toString(PonerId(lista) + 1));
        } catch (Exception e) {
            this.jtId.setText("1");
        }
    }

    public String GenerarClave() {
        String Clave = this.jtApePat.getText().substring((this.jtApePat.getText().length()) / 2 - 1, (this.jtApePat.getText().length()) / 2 + 1) + this.jtNom.getText() + this.jtApeMat.getText().substring((this.jtApeMat.getText().length()) / 2 - 1, (this.jtApeMat.getText().length()) / 2 + 1) + this.jtId.getText();
        Claveverdadera = Clave;
        System.out.println(Claveverdadera);
        return Clave;
    }

    public String GenerarCuenta() {
        String Cuenta = this.jtNom.getText().substring(0, 1) + this.jtApePat.getText() + this.jtApeMat.getText().substring(0, 1);
        CuentaUsuario = Cuenta;
        System.out.println(CuentaUsuario);
        return Cuenta;
    }

    public String CodificarClave(String Clave) {
        String CodificarClave = "";
        char c1[] = Clave.toCharArray();
        int x = Clave.length();
        int Sumador = 0;
        for (int i = 0; i < x; i++) {
            Sumador = (int) c1[i] + 1;
            CodificarClave = CodificarClave + (char) Sumador;
        }
        return CodificarClave;
    }

    public List<Usuario> ObtenerLista() throws Exception {
        try {
            List<Entidades.Usuario> lista = new ArrayList<Entidades.Usuario>();
            Datos_Registro_Usuario dru = new Datos_Registro_Usuario();
            lista = dru.MostrarLista();
            return lista;
        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDesktopPane1 = new javax.swing.JDesktopPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jtApePat = new javax.swing.JTextField();
        jtApeMat = new javax.swing.JTextField();
        jtNom = new javax.swing.JTextField();
        jtCorr = new javax.swing.JTextField();
        jcbTipo = new javax.swing.JComboBox();
        jbGuardar = new javax.swing.JButton();
        jbCancelar = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        jtId = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jcbUnidadTramite = new javax.swing.JComboBox();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jdFechaInicio = new com.toedter.calendar.JDateChooser();
        jdFechaFin = new com.toedter.calendar.JDateChooser();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jtUsuarios = new javax.swing.JTable();
        jbNuevo = new javax.swing.JButton();
        jbModificar = new javax.swing.JButton();
        jbAnular = new javax.swing.JButton();
        jbCerrar = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Usuario"));
        jPanel1.setLayout(null);

        jLabel2.setText("Apellido Paterno:");
        jPanel1.add(jLabel2);
        jLabel2.setBounds(20, 70, 100, 20);

        jLabel1.setText("Apellido Materno:");
        jPanel1.add(jLabel1);
        jLabel1.setBounds(20, 110, 100, 14);

        jLabel3.setText("Nombre:");
        jPanel1.add(jLabel3);
        jLabel3.setBounds(20, 150, 70, 14);

        jLabel4.setText("Correo:");
        jPanel1.add(jLabel4);
        jLabel4.setBounds(20, 190, 60, 14);

        jLabel5.setText("Tipo:");
        jPanel1.add(jLabel5);
        jLabel5.setBounds(20, 230, 60, 14);

        jtApePat.setText("jTextField1");
        jPanel1.add(jtApePat);
        jtApePat.setBounds(130, 60, 250, 20);

        jtApeMat.setText("jTextField2");
        jPanel1.add(jtApeMat);
        jtApeMat.setBounds(130, 100, 250, 20);

        jtNom.setText("jTextField3");
        jPanel1.add(jtNom);
        jtNom.setBounds(130, 140, 250, 20);

        jtCorr.setText("jTextField4");
        jPanel1.add(jtCorr);
        jtCorr.setBounds(130, 180, 250, 20);

        jcbTipo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Operador de Unidad de Tramite", "Jefe de Unidad de Tramite", "Jefe de Unidad Organizativa" }));
        jPanel1.add(jcbTipo);
        jcbTipo.setBounds(130, 220, 250, 20);

        jbGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/1437030748_document-save.png"))); // NOI18N
        jbGuardar.setText("Guardar");
        jbGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbGuardarActionPerformed(evt);
            }
        });
        jPanel1.add(jbGuardar);
        jbGuardar.setBounds(130, 410, 120, 35);

        jbCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/1438741222_delete_16.jpg"))); // NOI18N
        jbCancelar.setText("Cancelar");
        jbCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbCancelarActionPerformed(evt);
            }
        });
        jPanel1.add(jbCancelar);
        jbCancelar.setBounds(270, 410, 120, 35);

        jLabel6.setText("Id:");
        jPanel1.add(jLabel6);
        jLabel6.setBounds(20, 30, 14, 14);

        jtId.setText("jTextField1");
        jPanel1.add(jtId);
        jtId.setBounds(130, 20, 90, 20);

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Permisos"));
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel7.setText("Unidad de Tramite:");
        jPanel3.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 30, -1, -1));

        jPanel3.add(jcbUnidadTramite, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 50, 320, -1));

        jLabel8.setText("Fecha de Inicio:");
        jPanel3.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 80, -1, -1));

        jLabel9.setText("Fecha de Fin:");
        jPanel3.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 80, -1, -1));
        jPanel3.add(jdFechaInicio, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 100, 160, -1));
        jPanel3.add(jdFechaFin, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 100, 130, -1));

        jPanel1.add(jPanel3);
        jPanel3.setBounds(20, 260, 370, 150);

        jDesktopPane1.add(jPanel1);
        jPanel1.setBounds(10, 10, 410, 460);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Lista"));
        jPanel2.setLayout(null);

        jScrollPane1.setBorder(javax.swing.BorderFactory.createTitledBorder("Usuario"));

        jtUsuarios.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jtUsuarios);

        jPanel2.add(jScrollPane1);
        jScrollPane1.setBounds(10, 30, 490, 350);

        jbNuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/editar-icono-9796-128.png"))); // NOI18N
        jbNuevo.setText("Nuevo");
        jbNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbNuevoActionPerformed(evt);
            }
        });
        jPanel2.add(jbNuevo);
        jbNuevo.setBounds(20, 410, 110, 35);

        jbModificar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/save_as-32.png"))); // NOI18N
        jbModificar.setText("Modificar");
        jbModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbModificarActionPerformed(evt);
            }
        });
        jPanel2.add(jbModificar);
        jbModificar.setBounds(140, 410, 120, 35);

        jbAnular.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/descarga.jpg"))); // NOI18N
        jbAnular.setText("Anular");
        jbAnular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbAnularActionPerformed(evt);
            }
        });
        jPanel2.add(jbAnular);
        jbAnular.setBounds(270, 410, 110, 35);

        jbCerrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/stop-stop-desactivar-la-salida-icono-5982-96.png"))); // NOI18N
        jbCerrar.setText("Cerrar");
        jbCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbCerrarActionPerformed(evt);
            }
        });
        jPanel2.add(jbCerrar);
        jbCerrar.setBounds(390, 410, 110, 35);

        jDesktopPane1.add(jPanel2);
        jPanel2.setBounds(430, 10, 510, 460);

        jLabel10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/franjas-azules,-circulos,-fondo-azul-199896.jpg"))); // NOI18N
        jDesktopPane1.add(jLabel10);
        jLabel10.setBounds(0, 0, 950, 500);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jDesktopPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 951, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jDesktopPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 492, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jbNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbNuevoActionPerformed
        nuevo = 1;
        editar();
    }//GEN-LAST:event_jbNuevoActionPerformed

    private void jbCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbCerrarActionPerformed
        if (JOptionPane.showConfirmDialog(null, "¿Esta seguro que desea cerrar?") == 0) {
            this.dispose();
        }
    }//GEN-LAST:event_jbCerrarActionPerformed

    private void jbModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbModificarActionPerformed
        try {
            List<Usuario> lista = ObtenerLista();
            if (this.jtUsuarios.getRowCount() > 0) {
                if (this.jtUsuarios.getSelectedRow() != -1) {
                    this.jtId.setText(Integer.toString(lista.get(this.jtUsuarios.getSelectedRow()).getId()));
                    this.jtApePat.setText(lista.get(this.jtUsuarios.getSelectedRow()).getApelliPat());
                    this.jtApeMat.setText(lista.get(this.jtUsuarios.getSelectedRow()).getApelliMat());
                    this.jtNom.setText(lista.get(this.jtUsuarios.getSelectedRow()).getNombre());
                    this.jtCorr.setText(lista.get(this.jtUsuarios.getSelectedRow()).getCorreo());
                    if (lista.get(this.jtUsuarios.getSelectedRow()).getTipo().equals("O")) {
                        this.jcbTipo.setSelectedIndex(0);
                    }
                    if (lista.get(this.jtUsuarios.getSelectedRow()).getTipo().equals("T")) {
                        this.jcbTipo.setSelectedIndex(1);
                    }
                    if (lista.get(this.jtUsuarios.getSelectedRow()).getTipo().equals("G")) {
                        this.jcbTipo.setSelectedIndex(2);
                    }
                    nuevo = 2;
                    editarparcial();
                    seleccion = this.jtUsuarios.getSelectedRow();
                } else {
                    JOptionPane.showMessageDialog(null, "Debe Seleccionar la fila a modificar");
                }
            } else {
                JOptionPane.showMessageDialog(null, "No ahy filas que modificar");
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, e.getMessage(), "Mensaje del Sistema", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_jbModificarActionPerformed

    private void jbCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbCancelarActionPerformed
        inicializar();
    }//GEN-LAST:event_jbCancelarActionPerformed

    private void jbGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbGuardarActionPerformed
        if (JOptionPane.showConfirmDialog(rootPane, "¿Esta seguro que desea guardar la informacion?") == 0) {
            String Datos[]=new String[4];
            Datos[0]=this.jtApeMat.getText();
            Datos[1]=this.jtApePat.getText();
            Datos[2]=this.jtNom.getText();
            Datos[3]=this.jtCorr.getText();
            
            if (nuevo == 1) {
                Datos_Registro_Usuario dru = new Datos_Registro_Usuario();
                List<Entidades.Usuario> lista = new ArrayList<Entidades.Usuario>();
                if (!this.jtApeMat.getText().equals("") && !this.jtApePat.getText().equals("") && !this.jtNom.getText().equals("") && !this.jtCorr.getText().equals("") && Validar(Datos, 4)==true) {
                    try {
                        String Tipo = "";
                        String TipoPermiso = "";
                        if (this.jcbTipo.getSelectedIndex() == 0) {
                            Tipo = "O";
                            TipoPermiso = "T";
                        }
                        if (this.jcbTipo.getSelectedIndex() == 1) {
                            Tipo = "T";
                            TipoPermiso = "T";
                        }
                        if (this.jcbTipo.getSelectedIndex() == 2) {
                            Tipo = "G";
                            TipoPermiso = "G";
                        }
                        for (int i = 0; i < idtramite.length; i++) {
                            if (this.jcbUnidadTramite.getSelectedItem().toString().equals(idtramite[i][2])) {
                                seleccion = i;
                                i = idtramite.length + 1;
                            }
                        }
                        boolean validarusuario = dru.Insertar(new Usuario(null, this.jtApePat.getText(), this.jtApeMat.getText(), this.jtNom.getText(), this.jtCorr.getText(), GenerarCuenta(), CodificarClave(GenerarClave()), "A", Tipo));
                        lista = dru.MostrarLista();
                        int id = 1;
                        for (int i = 0; i < lista.size(); i++) {
                            if (this.jtNom.getText().equals(lista.get(i).getNombre()) && this.jtApePat.getText().equals(lista.get(i).getApelliPat()) && this.jtApeMat.getText().equals(lista.get(i).getApelliMat())) {
                                id = lista.get(i).getId();
                            }
                        }
                        boolean validarpermiso = dru.InsertarPermiso(new Permiso(null, this.jdFechaInicio.getDate(), this.jdFechaFin.getDate(), TipoPermiso, new Usuario(id), new UnidadOrganizativa(Integer.parseInt(idtramite[seleccion][1])), new UnidadTramite(Integer.parseInt(idtramite[seleccion][0]))));
                        if (validarusuario == true && validarpermiso == true) {
                            CorreoUsuario = this.jtCorr.getText();
                            hilos.start();
                            JOptionPane.showMessageDialog(null, "Usuario Registrado");
                            contador++;
                            inicializar();
                            nuevo = 0;
                        } else {
                            JOptionPane.showMessageDialog(null, "Error de Ingreso");
                        }

                    } catch (Exception e) {
                        System.out.println(e);
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Debe Ingresar todos los campos");
                }
            } else {
                if (nuevo == 2) {
                    if (!this.jtApeMat.getText().equals("") && !this.jtApePat.getText().equals("") && !this.jtNom.getText().equals("") && !this.jtCorr.getText().equals("") && Validar(Datos, 4)==true) {
                        try {
                            Datos_Registro_Usuario dru = new Datos_Registro_Usuario();
                            List<Entidades.Usuario> lista = new ArrayList<Entidades.Usuario>();
                            lista = dru.MostrarLista();
                            String Tipo = "";
                            if (this.jcbTipo.getSelectedIndex() == 0) {
                                Tipo = "O";
                            }
                            if (this.jcbTipo.getSelectedIndex() == 1) {
                                Tipo = "T";
                            }
                            if (this.jcbTipo.getSelectedIndex() == 2) {
                                Tipo = "G";
                            }

                            boolean validar = dru.Actualizar(new Usuario(Integer.parseInt(this.jtId.getText()), this.jtApePat.getText(), this.jtApeMat.getText(), this.jtNom.getText(), this.jtCorr.getText(), lista.get(seleccion).getCuenta(), lista.get(seleccion).getClave(), "A", Tipo));
                            if (validar == true) {
                                JOptionPane.showMessageDialog(null, "Actualizacion Completa");
                                contador++;
                                inicializar();
                                nuevo = 0;
                            }
                        } catch (Exception e) {
                            System.out.println(e);
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "Debe Ingresar todos los campos");
                    }

                }
            }
        } else {
            limpiar();
        }
    }//GEN-LAST:event_jbGuardarActionPerformed

    private void jbAnularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbAnularActionPerformed
        if (JOptionPane.showConfirmDialog(null, "¿Desea eliminar este dato?") == 0) {
            try {
                List<Usuario> lista = ObtenerLista();
                if (this.jtUsuarios.getRowCount() > 0) {
                    if (this.jtUsuarios.getSelectedRow() != -1) {
                        Datos_Registro_Usuario duo = new Datos_Registro_Usuario();
                        boolean a = duo.Eliminar(new Usuario(lista.get(this.jtUsuarios.getSelectedRow()).getId()));
                        if (a == true) {
                            JOptionPane.showMessageDialog(null, "Dato Eliminado");
                            inicializar();
                        } else {
                            JOptionPane.showMessageDialog(null, "Error al eliminar");
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "Debe Seleccionar la fila a eliminar");
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "No ahy filas que eliminar");
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(this, e.getMessage(), "Mensaje del Sistema", JOptionPane.WARNING_MESSAGE);
            }
        }
    }//GEN-LAST:event_jbAnularActionPerformed

    public boolean Validar(String Datos[], int contador) {
        boolean validar = true;
        boolean a = true;
        for (int i = 0; i < contador && validar == true; i++) {
            for (int j = 0; j < Datos[i].length(); j++) {
                if (Datos[i].substring(j, j + 1).equals("'")) {
                    a = false;
                }
            }
            if (a == false) {
                validar = false;
            }
        }
        return validar;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Registro_Usuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Registro_Usuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Registro_Usuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Registro_Usuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Registro_Usuario().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JDesktopPane jDesktopPane1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton jbAnular;
    private javax.swing.JButton jbCancelar;
    private javax.swing.JButton jbCerrar;
    private javax.swing.JButton jbGuardar;
    private javax.swing.JButton jbModificar;
    private javax.swing.JButton jbNuevo;
    private javax.swing.JComboBox jcbTipo;
    private javax.swing.JComboBox jcbUnidadTramite;
    private com.toedter.calendar.JDateChooser jdFechaFin;
    private com.toedter.calendar.JDateChooser jdFechaInicio;
    private javax.swing.JTextField jtApeMat;
    private javax.swing.JTextField jtApePat;
    private javax.swing.JTextField jtCorr;
    private javax.swing.JTextField jtId;
    private javax.swing.JTextField jtNom;
    private javax.swing.JTable jtUsuarios;
    // End of variables declaration//GEN-END:variables
}
