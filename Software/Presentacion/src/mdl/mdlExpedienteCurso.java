/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mdl;

import Entidades.Ruta;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Carlos Anthony
 */
public class mdlExpedienteCurso extends AbstractTableModel  {

    private String[] columnas = {"  ", " Fecha de Recepcion","Recepcionado en "};
    private List<Ruta> lista=new ArrayList<Ruta>();
    
    public mdlExpedienteCurso(List<Ruta> lista) {
        this.lista = lista;
    }
    
    @Override
    public int getRowCount() {
        return lista.size();
    }

    @Override
    public int getColumnCount() {
        return columnas.length;
    }

    @Override
    public Object getValueAt(int f, int c) {
        Object resultado;
        
        if (c == 0) {
            resultado = lista.get(f).getId();
        }if(c==1){
            resultado =lista.get(f).getFechahora();
        }else {
            resultado = (lista.get(f).getUnidadTramite().getUnidadorganizativa().getNombre()+"-"+lista.get(f).getUnidadTramite().getNombre());
        }

        return resultado;
    }

    @Override
    public String getColumnName(int c) {
        return columnas[c];
    }
    
}
