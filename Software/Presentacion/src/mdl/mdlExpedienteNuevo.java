/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdl;

import Entidades.Requisitos;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Carlos Anthony
 */
public class mdlExpedienteNuevo extends AbstractTableModel {

    private String[] columnas = {"id", "Nombre"};
    private List<Requisitos> lista = new ArrayList<Requisitos>();
    private boolean[] editables={true,false};

    public mdlExpedienteNuevo(List<Requisitos> lista) {
        this.lista = lista;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return editables[columnIndex];
    }
    
    @Override
    public int getRowCount() {
        return lista.size();
    }

    @Override
    public int getColumnCount() {
        return columnas.length;
    }

    @Override
    public Object getValueAt(int f, int c) {
        Object resultado;
        
        if (c == 0) {
            resultado = lista.get(f).getId();
        } else {
            resultado = lista.get(f).getDescripcion();
        }

        return resultado;
    }

    @Override
    public String getColumnName(int c) {
        return columnas[c];
    }
}
