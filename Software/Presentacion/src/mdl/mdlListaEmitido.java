/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdl;

import Entidades.ListaEmitido;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Carlos Anthony
 */
public class mdlListaEmitido extends AbstractTableModel {

    private String[] columnas = {"Fecha de Emision", "Tipo de Documento", "N° de Documento", "Tipo", "N° de Expediente"};
    private List<ListaEmitido> lista = new ArrayList<ListaEmitido>();

    public mdlListaEmitido(List<ListaEmitido> lista) {
        this.lista = lista;
    }

    @Override
    public int getRowCount() {
        return lista.size();
    }

    @Override
    public int getColumnCount() {
        return columnas.length;
    }

    @Override
    public Object getValueAt(int f, int c) {
        Object resultado;

        if (c == 0) {
            resultado = lista.get(f).getFecha();
        } else {
            if (c == 1) {
                resultado = lista.get(f).getTipodoc();
            } else {
                if (c == 2) {
                    resultado = lista.get(f).getNumdoc();
                } else {
                    if (c == 3) {
                        resultado = lista.get(f).getTipo();
                    } else {
                        resultado = lista.get(f).getNumexp();
                    }
                }
            }
        }

        return resultado;
    }

    @Override
    public String getColumnName(int c) {
        return columnas[c];
    }
}
