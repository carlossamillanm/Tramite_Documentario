/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdl;

import Entidades.ListaRecibida;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Carlos Anthony
 */
public class mdlListaRecibidos extends AbstractTableModel {

    private String[] columnas = {"Fecha de Recepcion", "De", "Tipo Tramite", "N° de Expediente"};
    private List<ListaRecibida> lista = new ArrayList<ListaRecibida>();

    public mdlListaRecibidos(List<ListaRecibida> lista) {
        this.lista = lista;
    }

    @Override
    public int getRowCount() {
        return lista.size();
    }

    @Override
    public int getColumnCount() {
        return columnas.length;
    }

    @Override
    public Object getValueAt(int f, int c) {
        Object resultado;

        if (c == 0) {
            resultado = lista.get(f).getFecha().toString().substring(0, 10);
        } else {
            if (c == 1) {
                resultado = lista.get(f).getDe();
            } else {
                if (c == 2) {
                    resultado = lista.get(f).getTipoTramite();
                } else {
                    resultado = lista.get(f).getNumexp();
                }
            }
        }

        return resultado;
    }

    @Override
    public String getColumnName(int c) {
        return columnas[c];
    }

}
