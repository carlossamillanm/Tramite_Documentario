/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mdl;

import Entidades.Usuario;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Carlos Anthony
 */
public class mdlRegistroUsuarioLista extends AbstractTableModel{
    private String[] columnas = {"Id", "Apellido Paterno", "Apellido Materno", "Nombre"};
    private List<Usuario> lista = new ArrayList<Usuario>();

    public mdlRegistroUsuarioLista(List<Usuario> lista) {
        this.lista = lista;
    }
    
      @Override
    public int getRowCount() {
        return lista.size();
    }

    @Override
    public int getColumnCount() {
        return columnas.length;
    }

    @Override
    public Object getValueAt(int f, int c) {
        Object resultado;

        if (c == 0) {
            resultado = lista.get(f).getId();
        } else if (c == 1) {
            resultado = lista.get(f).getApelliPat();
        } else if (c == 2) {
            resultado = lista.get(f).getApelliMat();
        } else {
            resultado = lista.get(f).getNombre();
        }

        return resultado;
    }

    @Override
    public String getColumnName(int c) {
        return columnas[c];
    }
}
