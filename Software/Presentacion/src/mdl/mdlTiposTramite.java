/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdl;

import Entidades.TipoTramite;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Carlos Anthony
 */
public class mdlTiposTramite extends AbstractTableModel {

    private String[] columnas = {"Id", "Nombre", "Tiempo Estimado"};
    private List<TipoTramite> lista = new ArrayList<TipoTramite>();

    public mdlTiposTramite(List<TipoTramite> lista) {
        this.lista = lista;
    }

    @Override
    public int getRowCount() {
        return lista.size();
    }

    @Override
    public int getColumnCount() {
        return columnas.length;
    }

    @Override
    public Object getValueAt(int f, int c) {
        Object resultado;

        if (c == 0) {
            resultado = lista.get(f).getId();
        } else if (c == 1) {
            resultado = lista.get(f).getNombre();
        } else {
            resultado = lista.get(f).getTiempoEstimado();
        }

        return resultado;
    }

    @Override
    public String getColumnName(int c) {
        return columnas[c];
    }
}
