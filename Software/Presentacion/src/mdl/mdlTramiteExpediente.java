/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mdl;

import Entidades.RequisitosRecibidos;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Carlos Anthony
 */
public class mdlTramiteExpediente extends AbstractTableModel{

    private String[] columnas = {"Descripcion", " "};
    private List<Entidades.RequisitosRecibidos> lista=new ArrayList<Entidades.RequisitosRecibidos>();
    
    public mdlTramiteExpediente(List<RequisitosRecibidos> lista){
        this.lista=lista;
    }
    
    @Override
    public int getRowCount() {
        return lista.size();
    }

    @Override
    public int getColumnCount() {
        return columnas.length;
    }

    @Override
    public Object getValueAt(int f, int c) {
        Object resultado;

        if (c == 0) {
            resultado = lista.get(f).getDescripcion();
        } else {
            resultado = lista.get(f).getEstado();
        } 
        
        return resultado;
    }
    
    @Override
    public String getColumnName(int c) {
        return columnas[c];
    }
    
}
