/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdl;

import Entidades.UnidadOrganizativa;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Carlos Anthony
 */
public class mdlUnidadesOrganizativas extends AbstractTableModel {

    private String[] columnas = {"Id", "Nombre", "Abreviatura"};
    private List<UnidadOrganizativa> lista = new ArrayList<UnidadOrganizativa>();

    public mdlUnidadesOrganizativas(List<UnidadOrganizativa> lista) {
        this.lista = lista;
    }

    @Override
    public int getRowCount() {
        return lista.size();
    }

    @Override
    public int getColumnCount() {
        return columnas.length;
    }

    @Override
    public Object getValueAt(int f, int c) {
        Object resultado;
        if (c == 0) {
            resultado = lista.get(f).getId();
        } else if (c == 1) {
            resultado = lista.get(f).getNombre();
        } else if (c == 2) {
            resultado = lista.get(f).getAbreviatura();
        } else {
            resultado = lista.get(f).getEstado();
        }

        return resultado;
    }

    @Override
    public String getColumnName(int c) {
        return columnas[c];
    }

}
